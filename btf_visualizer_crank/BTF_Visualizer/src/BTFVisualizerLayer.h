#pragma once
#include <Crank.h>

#include "Demo.h"

class BTFVisualizerLayer : public Crank::Layer
{
public:
	BTFVisualizerLayer();

	void OnAttach() override;
	void OnUpdate(Crank::DeltaTime dt) override;
	void OnImGuiRender() override;
	void OnEvent(Crank::Event& e) override;

private:
	glm::vec2 m_ViewportPanelSize = { 0.0f, 0.0f };

	Crank::Ref<Crank::Framebuffer> m_Framebuffer = nullptr;
	Crank::Ref<Crank::OrbitalCameraController> m_CameraController = nullptr;

	std::vector<Crank::Ref<Demo>> m_Demos;
	unsigned int m_SelectedDemoIdx = 1; // Sets default demo
};
