#include "crpch.h"
#include <Crank.h>
#include <Crank/Core/EntryPoint.h>

#include "BTFVisualizerLayer.h"

class BTFVisualizer : public Crank::Application
{
public:
	BTFVisualizer()
	{
		PushLayer(new BTFVisualizerLayer());
	}

	~BTFVisualizer() {}

protected:
	virtual bool OnKeyPressed(Crank::KeyPressedEvent& e) override;
};

bool BTFVisualizer::OnKeyPressed(Crank::KeyPressedEvent& e)
{
	switch (e.GetKeyCode())
	{
	case Crank::KeyInput::ESCAPE:
	{
		Close();
		break;
	}
	}
	return false;
}

Crank::Application* Crank::CreateApplication()
{
	return new BTFVisualizer();
}