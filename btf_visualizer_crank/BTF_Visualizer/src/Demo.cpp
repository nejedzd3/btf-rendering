#include "crpch.h"
#include "Demo.h"
#include <Crank.h>


//--------------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------Billboard Demo------------------------------------------------------------------------------------
PlaneDemo::PlaneDemo(const Crank::Ref<Crank::OrbitalCameraController>& camController, const Crank::Ref<Crank::TextureAtlas>& normalAtlas,
	const Crank::Ref<Crank::TextureAtlas>& utiaAtlas, const Crank::Ref<Crank::TextureAtlas>& testAtlas, const Crank::Ref<Crank::Shader>& btfShader)
	: m_CamController(camController), m_Atlas(normalAtlas), m_UTIAAtlas(utiaAtlas), m_TestAtlas(testAtlas), m_BTFShader(btfShader)
{
}

void PlaneDemo::OnUpdate()
{
	m_BTFShader->Bind();
	m_BTFShader->PassUniform("u_Bilinear", useBilinear);
	m_BTFShader->PassUniform("u_SimpleViewVec", useSimpleViewVec);
	m_BTFShader->PassUniform("u_FilmicToneMapping", useFilmicToneMapping);
	m_BTFShader->PassUniform("u_CameraPosition", m_CamController->GetCamera()->GetTransform().GetPosition());

	Crank::Renderer::Submit(m_Plane);
}

void PlaneDemo::OnLoad()
{
	if (m_Loaded)
		return;

	Init();

	m_Loaded = true;
}

void PlaneDemo::OnReload()
{
	Init();

	m_Loaded = true;
}

void PlaneDemo::OnImGuiRender()
{
	Crank::ImGuiRenderer::Submit(m_Name);
	Crank::ImGuiRenderer::Separator();

	auto& yawPitch = m_CamController->GetYawPitch();
	ImGui::Text("Camera Info");
	ImGui::Text("Yaw: %.3f", glm::degrees(yawPitch.x));
	ImGui::Text("Pitch: %.3f", glm::degrees(yawPitch.y));
	auto& cameraPos = m_CamController->GetCamera()->GetTransform().GetPosition();
	ImGui::Text("Position: (%.3f, %.3f, %.3f)", cameraPos.x, cameraPos.y, cameraPos.z);
	auto cameraPosSph = Crank::Utility::CartesianToSphereCoords({ cameraPos.x, cameraPos.z, cameraPos.y });
	ImGui::Text("Spherical Coords:\n r = %.3f\n azi = %.3f\n elev = %.3f", cameraPosSph.x, cameraPosSph.y, cameraPosSph.z);

	Crank::ImGuiRenderer::Separator();

	Crank::ImGuiRenderer::Submit(&useBilinear, { "Bilinear Sampling" });
	Crank::ImGuiRenderer::Submit(&useSimpleViewVec, { "Simple View Vector" });
	Crank::ImGuiRenderer::Submit(&useFilmicToneMapping, { "Filmic Tone Mapping" });

	Crank::ImGuiRenderer::Separator();

	Crank::ImGuiRenderer::Submit("Switch Atlas");

	if (Crank::ImGuiRenderer::Button("Grass"))
		enableNormalAtlas();

	Crank::ImGuiRenderer::SameLine();

	if (Crank::ImGuiRenderer::Button("Fabric"))
		enableUTIAAtlas();

	Crank::ImGuiRenderer::SameLine();

	if (Crank::ImGuiRenderer::Button("Test"))
		enableTestAtlas();

	Crank::ImGuiRenderer::Separator();

	m_Plane->GetMaterial()->OnImGuiRender();
}

bool PlaneDemo::IsLoaded()
{
	return m_Loaded;
}

void PlaneDemo::enableTestAtlas()
{
	if (!m_Plane)
		return;

	auto btfMat = Crank::CreateRef<Crank::Material>(m_BTFShader);
	btfMat->SetData("tex_cnt", m_TestAtlas->GetSpec().TextureCount)
		.SetData("single_tex_res", m_TestAtlas->GetSpec().SingleTextureResolution)
		.SetData("sph_coord_offset", m_TestAtlas->GetSpec().FirstSphericalCoordinate)
		.SetData("atlas_res", glm::uvec2(m_TestAtlas->GetAtlasTexture()->GetWidth(), m_TestAtlas->GetAtlasTexture()->GetHeight()))
		.SetData("is_utia", m_Atlas->GetSpec().IsUTIA);
	btfMat->SetTexture("atlas", m_TestAtlas->GetAtlasTexture());

	m_Plane->SetMaterial(btfMat);
}

void PlaneDemo::enableNormalAtlas()
{
	if (!m_Plane)
		return;

	auto btfMat = Crank::CreateRef<Crank::Material>(m_BTFShader);
	btfMat->SetData("atlas_res", glm::uvec2(m_Atlas->GetAtlasTexture()->GetWidth(), m_Atlas->GetAtlasTexture()->GetHeight()))
		.SetData("tex_cnt", m_Atlas->GetSpec().TextureCount)
		.SetData("single_tex_res", m_Atlas->GetSpec().SingleTextureResolution)
		.SetData("sph_coord_offset", m_Atlas->GetSpec().FirstSphericalCoordinate)
		.SetData("is_utia", m_Atlas->GetSpec().IsUTIA);
	btfMat->SetTexture("atlas", m_Atlas->GetAtlasTexture());

	m_Plane->SetMaterial(btfMat);
}

void PlaneDemo::enableUTIAAtlas()
{
	if (!m_Plane)
		return;

	auto btfMat = Crank::CreateRef<Crank::Material>(m_BTFShader);
	btfMat->SetData("atlas_res", glm::uvec2(m_UTIAAtlas->GetAtlasTexture()->GetWidth(), m_UTIAAtlas->GetAtlasTexture()->GetHeight()))
		.SetData("tex_cnt", m_UTIAAtlas->GetSpec().TextureCount)
		.SetData("single_tex_res", m_UTIAAtlas->GetSpec().SingleTextureResolution)
		.SetData("sph_coord_offset", m_UTIAAtlas->GetSpec().FirstSphericalCoordinate)
		.SetData("is_utia", m_UTIAAtlas->GetSpec().IsUTIA);
	btfMat->SetTexture("atlas", m_UTIAAtlas->GetAtlasTexture());

	m_Plane->SetMaterial(btfMat);
}

void PlaneDemo::Init()
{
	m_Plane = Crank::MeshLoader::LoadMesh("./assets/models/plane.glb", m_BTFShader);
	m_Plane->GetTransform().SetPosition({ 0.0f, 0.0f, 0.0f });

	enableNormalAtlas();
}

//--------------------------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------Object Demo------------------------------------------------------------------------------------
ObjectDemo::ObjectDemo(const Crank::Ref<Crank::OrbitalCameraController>& camController, const Crank::Ref<Crank::TextureAtlas>& normalAtlas,
	const Crank::Ref<Crank::TextureAtlas>& utiaAtlas, const Crank::Ref<Crank::TextureAtlas>& testAtlas, const Crank::Ref<Crank::Shader>& btfShader)
	: m_CamController(camController), m_Atlas(normalAtlas), m_UTIAAtlas(utiaAtlas), m_TestAtlas(testAtlas), m_BTFShader(btfShader)
{
}

void ObjectDemo::OnUpdate()
{
	m_BTFShader->Bind();
	m_BTFShader->PassUniform("u_Bilinear", useBilinear);
	m_BTFShader->PassUniform("u_SimpleViewVec", useSimpleViewVec);
	m_BTFShader->PassUniform("u_FilmicToneMapping", useFilmicToneMapping);
	m_BTFShader->PassUniform("u_CameraPosition", m_CamController->GetCamera()->GetTransform().GetPosition());

	Crank::Renderer::Submit(m_Object);
}

void ObjectDemo::OnLoad()
{
	if (m_Loaded)
		return;

	Init();

	m_Loaded = true;
}

void ObjectDemo::OnReload()
{
	Init();

	m_Loaded = true;
}

void ObjectDemo::OnImGuiRender()
{
	Crank::ImGuiRenderer::Submit(m_Name);
	Crank::ImGuiRenderer::Separator();

	auto& yawPitch = m_CamController->GetYawPitch();
	ImGui::Text("Camera Info");
	ImGui::Text("Yaw: %.3f", glm::degrees(yawPitch.x));
	ImGui::Text("Pitch: %.3f", glm::degrees(yawPitch.y));
	auto& cameraPos = m_CamController->GetCamera()->GetTransform().GetPosition();
	ImGui::Text("Position: (%.3f, %.3f, %.3f)", cameraPos.x, cameraPos.y, cameraPos.z);
	auto cameraPosSph = Crank::Utility::CartesianToSphereCoords({ cameraPos.x, cameraPos.z, cameraPos.y });
	ImGui::Text("Spherical Coords:\n r = %.3f\n azi = %.3f\n elev = %.3f", cameraPosSph.x, cameraPosSph.y, cameraPosSph.z);

	Crank::ImGuiRenderer::Separator();

	Crank::ImGuiRenderer::Submit(&useBilinear, { "Bilinear Sampling" });
	Crank::ImGuiRenderer::Submit(&useSimpleViewVec, { "Simple View Vector" });
	Crank::ImGuiRenderer::Submit(&useFilmicToneMapping, { "Filmic Tone Mapping" });

	Crank::ImGuiRenderer::Separator();

	Crank::ImGuiRenderer::Submit("Switch Atlas");

	if (Crank::ImGuiRenderer::Button("Grass"))
		enableNormalAtlas();

	Crank::ImGuiRenderer::SameLine();

	if (Crank::ImGuiRenderer::Button("Fabric"))
		enableUTIAAtlas();

	Crank::ImGuiRenderer::SameLine();

	if (Crank::ImGuiRenderer::Button("Test"))
		enableTestAtlas();

	Crank::ImGuiRenderer::Separator();

	m_Object->GetMaterial()->OnImGuiRender();
}

bool ObjectDemo::IsLoaded()
{
	return m_Loaded;
}

void ObjectDemo::Init()
{
	m_Object = Crank::MeshLoader::LoadMesh("./assets/models/suzanne.glb", m_BTFShader);
	m_Object->GetTransform().SetPosition({ 0.0f, 0.0f, 0.0f });
	m_Object->GetTransform().SetScale(glm::vec3(0.3f));

	enableNormalAtlas();
}

void ObjectDemo::enableTestAtlas()
{
	if (!m_Object)
		return;

	auto btfMat = Crank::CreateRef<Crank::Material>(m_BTFShader);
	btfMat->SetData("tex_cnt", m_TestAtlas->GetSpec().TextureCount)
		.SetData("single_tex_res", m_TestAtlas->GetSpec().SingleTextureResolution)
		.SetData("sph_coord_offset", m_TestAtlas->GetSpec().FirstSphericalCoordinate)
		.SetData("atlas_res", glm::uvec2(m_TestAtlas->GetAtlasTexture()->GetWidth(), m_TestAtlas->GetAtlasTexture()->GetHeight()))
		.SetData("is_utia", m_Atlas->GetSpec().IsUTIA);
	btfMat->SetTexture("atlas", m_TestAtlas->GetAtlasTexture());

	m_Object->SetMaterial(btfMat);
}

void ObjectDemo::enableNormalAtlas()
{
	if (!m_Object)
		return;

	auto btfMat = Crank::CreateRef<Crank::Material>(m_BTFShader);
	btfMat->SetData("atlas_res", glm::uvec2(m_Atlas->GetAtlasTexture()->GetWidth(), m_Atlas->GetAtlasTexture()->GetHeight()))
		.SetData("tex_cnt", m_Atlas->GetSpec().TextureCount)
		.SetData("single_tex_res", m_Atlas->GetSpec().SingleTextureResolution)
		.SetData("sph_coord_offset", m_Atlas->GetSpec().FirstSphericalCoordinate)
		.SetData("is_utia", m_Atlas->GetSpec().IsUTIA);
	btfMat->SetTexture("atlas", m_Atlas->GetAtlasTexture());

	m_Object->SetMaterial(btfMat);
}

void ObjectDemo::enableUTIAAtlas()
{
	if (!m_Object)
		return;

	auto btfMat = Crank::CreateRef<Crank::Material>(m_BTFShader);
	btfMat->SetData("atlas_res", glm::uvec2(m_UTIAAtlas->GetAtlasTexture()->GetWidth(), m_UTIAAtlas->GetAtlasTexture()->GetHeight()))
		.SetData("tex_cnt", m_UTIAAtlas->GetSpec().TextureCount)
		.SetData("single_tex_res", m_UTIAAtlas->GetSpec().SingleTextureResolution)
		.SetData("sph_coord_offset", m_UTIAAtlas->GetSpec().FirstSphericalCoordinate)
		.SetData("is_utia", m_UTIAAtlas->GetSpec().IsUTIA);
	btfMat->SetTexture("atlas", m_UTIAAtlas->GetAtlasTexture());

	m_Object->SetMaterial(btfMat);
}

//--------------------------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------Custom Demo------------------------------------------------------------------------------------
CustomDemo::CustomDemo(const Crank::Ref<Crank::OrbitalCameraController>& camController, const Crank::Ref<Crank::TextureAtlas>& normalAtlas,
	const Crank::Ref<Crank::TextureAtlas>& utiaAtlas, const Crank::Ref<Crank::TextureAtlas>& testAtlas, const Crank::Ref<Crank::Shader>& btfShader)
	: m_CamController(camController), m_Atlas(normalAtlas), m_UTIAAtlas(utiaAtlas), m_TestAtlas(testAtlas), m_BTFShader(btfShader)
{
}

void CustomDemo::OnUpdate()
{
	m_BTFShader->Bind();
	m_BTFShader->PassUniform("u_Bilinear", useBilinear);
	m_BTFShader->PassUniform("u_SimpleViewVec", useSimpleViewVec);
	m_BTFShader->PassUniform("u_FilmicToneMapping", useFilmicToneMapping);
	m_BTFShader->PassUniform("u_CameraPosition", m_CamController->GetCamera()->GetTransform().GetPosition());

	Crank::Renderer::Submit(m_Object);
}

void CustomDemo::OnLoad()
{
	if (m_Loaded)
		return;

	Init();

	m_Loaded = true;
}

void CustomDemo::OnReload()
{
	Init();

	m_Loaded = true;
}

void CustomDemo::OnImGuiRender()
{
	Crank::ImGuiRenderer::Submit(m_Name);
	Crank::ImGuiRenderer::Separator();
	Crank::ImGuiRenderer::Submit("To view different model,");
	Crank::ImGuiRenderer::Submit("replace file \"custom.obj\"");
	Crank::ImGuiRenderer::Submit("in folder assets/models");
	Crank::ImGuiRenderer::Separator();

	auto& yawPitch = m_CamController->GetYawPitch();
	ImGui::Text("Camera Info");
	ImGui::Text("Yaw: %.3f", glm::degrees(yawPitch.x));
	ImGui::Text("Pitch: %.3f", glm::degrees(yawPitch.y));
	auto& cameraPos = m_CamController->GetCamera()->GetTransform().GetPosition();
	ImGui::Text("Position: (%.3f, %.3f, %.3f)", cameraPos.x, cameraPos.y, cameraPos.z);
	auto cameraPosSph = Crank::Utility::CartesianToSphereCoords({ cameraPos.x, cameraPos.z, cameraPos.y });
	ImGui::Text("Spherical Coords:\n r = %.3f\n azi = %.3f\n elev = %.3f", cameraPosSph.x, cameraPosSph.y, cameraPosSph.z);

	Crank::ImGuiRenderer::Separator();

	Crank::ImGuiRenderer::Submit(&useBilinear, { "Bilinear Sampling" });
	Crank::ImGuiRenderer::Submit(&useSimpleViewVec, { "Simple View Vector" });
	Crank::ImGuiRenderer::Submit(&useFilmicToneMapping, { "Filmic Tone Mapping" });

	Crank::ImGuiRenderer::Separator();

	Crank::ImGuiRenderer::Submit("Switch Atlas");

	if (Crank::ImGuiRenderer::Button("Grass"))
		enableNormalAtlas();

	Crank::ImGuiRenderer::SameLine();

	if (Crank::ImGuiRenderer::Button("Fabric"))
		enableUTIAAtlas();

	Crank::ImGuiRenderer::SameLine();

	if (Crank::ImGuiRenderer::Button("Test"))
		enableTestAtlas();

	Crank::ImGuiRenderer::Separator();

	m_Object->GetMaterial()->OnImGuiRender();
}

bool CustomDemo::IsLoaded()
{
	return m_Loaded;
}

void CustomDemo::Init()
{
	m_Object = Crank::MeshLoader::LoadMesh("./assets/models/custom.obj", m_BTFShader);
	m_Object->GetTransform().SetPosition({ 0.0f, 0.0f, 0.0f });
	m_Object->GetTransform().SetScale(glm::vec3(0.3f));

	enableNormalAtlas();
}

void CustomDemo::enableTestAtlas()
{
	if (!m_Object)
		return;

	auto btfMat = Crank::CreateRef<Crank::Material>(m_BTFShader);
	btfMat->SetData("tex_cnt", m_TestAtlas->GetSpec().TextureCount)
		.SetData("single_tex_res", m_TestAtlas->GetSpec().SingleTextureResolution)
		.SetData("sph_coord_offset", m_TestAtlas->GetSpec().FirstSphericalCoordinate)
		.SetData("atlas_res", glm::uvec2(m_TestAtlas->GetAtlasTexture()->GetWidth(), m_TestAtlas->GetAtlasTexture()->GetHeight()))
		.SetData("is_utia", m_Atlas->GetSpec().IsUTIA);
	btfMat->SetTexture("atlas", m_TestAtlas->GetAtlasTexture());

	m_Object->SetMaterial(btfMat);
}

void CustomDemo::enableNormalAtlas()
{
	if (!m_Object)
		return;

	auto btfMat = Crank::CreateRef<Crank::Material>(m_BTFShader);
	btfMat->SetData("atlas_res", glm::uvec2(m_Atlas->GetAtlasTexture()->GetWidth(), m_Atlas->GetAtlasTexture()->GetHeight()))
		.SetData("tex_cnt", m_Atlas->GetSpec().TextureCount)
		.SetData("single_tex_res", m_Atlas->GetSpec().SingleTextureResolution)
		.SetData("sph_coord_offset", m_Atlas->GetSpec().FirstSphericalCoordinate)
		.SetData("is_utia", m_Atlas->GetSpec().IsUTIA);
	btfMat->SetTexture("atlas", m_Atlas->GetAtlasTexture());

	m_Object->SetMaterial(btfMat);
}

void CustomDemo::enableUTIAAtlas()
{
	if (!m_Object)
		return;

	auto btfMat = Crank::CreateRef<Crank::Material>(m_BTFShader);
	btfMat->SetData("atlas_res", glm::uvec2(m_UTIAAtlas->GetAtlasTexture()->GetWidth(), m_UTIAAtlas->GetAtlasTexture()->GetHeight()))
		.SetData("tex_cnt", m_UTIAAtlas->GetSpec().TextureCount)
		.SetData("single_tex_res", m_UTIAAtlas->GetSpec().SingleTextureResolution)
		.SetData("sph_coord_offset", m_UTIAAtlas->GetSpec().FirstSphericalCoordinate)
		.SetData("is_utia", m_UTIAAtlas->GetSpec().IsUTIA);
	btfMat->SetTexture("atlas", m_UTIAAtlas->GetAtlasTexture());

	m_Object->SetMaterial(btfMat);
}

//--------------------------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------Default Demo------------------------------------------------------------------------------------
DefaultDemo::DefaultDemo(const Crank::Ref<Crank::OrbitalCameraController>& camController)
	: m_CamController(camController)
{
}

void DefaultDemo::OnUpdate()
{
	m_PBRShader->Bind();
	m_PBRShader->PassUniform("u_CameraPosition", m_CamController->GetCamera()->GetTransform().GetPosition());
	m_Light->PassData();

	Crank::Renderer::Submit(m_Object);
}

void DefaultDemo::OnLoad()
{
	if (m_Loaded)
		return;

	Init();

	m_Loaded = true;
}

void DefaultDemo::OnReload()
{
	Init();

	m_Loaded = true;
}

void DefaultDemo::OnImGuiRender()
{
	Crank::ImGuiRenderer::Submit(m_Name);

	ImGui::Separator();

	m_Object->GetMaterial()->OnImGuiRender();
	
	ImGui::Separator();

	m_Light->OnImGuiRender();
}

bool DefaultDemo::IsLoaded()
{
	return m_Loaded;
}

void DefaultDemo::Init()
{
	m_PBRShader = Crank::Shader::Create("./assets/shaders/PBRShader.glsl");

	m_Object= Crank::MeshLoader::LoadMesh("./assets/models/suzanne.glb", m_PBRShader);
	m_Object->GetTransform().SetPosition({ 0.0f, 0.0f, 0.0f });
	m_Object->GetTransform().SetScale(glm::vec3(0.3f));

	auto pbrMat = Crank::CreateRef<Crank::Material>(m_PBRShader);
	pbrMat->SetData("color", glm::vec3(1.0f), Crank::ImGuiDataDetail(true, 1.0f, 1.0f, 1.0f))
		.SetData("roughness", 0.5f, Crank::ImGuiDataDetail(false, 0.0f, 1.0f, 0.05f))
		.SetData("metallic", 0.0f, Crank::ImGuiDataDetail(false, 0.0f, 1.0f, 0.1f));
	m_Object->SetMaterial(pbrMat);

	m_Light = Crank::CreateRef<Crank::Light>(m_PBRShader);
	m_Light->SetData("origin", glm::vec4(0.0f, 0.0f, 2.0f, 1.0f), { false, -10.0f, 10.0f, 0.1f })
		.SetData("color", glm::vec3(1.0f), { true, 1.0f, 1.0f, 1.0f })
		.SetData("intensity", 1.0f, { false, 0.0f, 10.0f, 0.1f });
}
