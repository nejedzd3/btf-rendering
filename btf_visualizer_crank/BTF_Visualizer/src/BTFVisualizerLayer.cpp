#include "crpch.h"
#include "BTFVisualizerLayer.h"

#include <imgui.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>


BTFVisualizerLayer::BTFVisualizerLayer()
	: Layer("BTF_Visualize")
{

	m_CameraController = Crank::CreateRef<Crank::OrbitalCameraController>(glm::vec2(0.0f, -1.71f));
	m_CameraController->OnResize(Crank::WindowResizedEvent(Crank::Application::Get().GetWindow().GetWidth(), 
															Crank::Application::Get().GetWindow().GetHeight())
		);

	auto normalAtlas = Crank::CreateRef<Crank::TextureAtlas>("./assets/btf_materials/grass-L6.283-1.047.png");
	auto utiaAtlas = Crank::CreateRef<Crank::TextureAtlas>("./assets/btf_materials/fabric-L0.000-0.000.png");
	auto testAtlas = Crank::CreateRef<Crank::TextureAtlas>("./assets/btf_materials/TestAtlas.png");
	auto BTFShader = Crank::Shader::Create("./assets/shaders/BTFShader.glsl");

	// Add Demos
	m_Demos.push_back(Crank::CreateRef<DefaultDemo>(m_CameraController));
	m_Demos.push_back(Crank::CreateRef<PlaneDemo>(m_CameraController, normalAtlas, utiaAtlas, testAtlas, BTFShader));
	m_Demos.push_back(Crank::CreateRef<ObjectDemo>(m_CameraController, normalAtlas, utiaAtlas, testAtlas, BTFShader));
	m_Demos.push_back(Crank::CreateRef<CustomDemo>(m_CameraController, normalAtlas, utiaAtlas, testAtlas, BTFShader));

	m_Demos[m_SelectedDemoIdx]->OnLoad(); // load default demo

	// Set default font
	Crank::ImGuiRenderer::SetFontAsDefault("./assets/fonts/montserrat/Montserrat-Regular.ttf", 16.0f);
}

void BTFVisualizerLayer::OnAttach()
{
	auto& win = Crank::Application::Get().GetWindow();
	m_Framebuffer = Crank::Framebuffer::Create(
		Crank::Framebuffer::Specification(win.GetWidth(), win.GetHeight(), win.GetMSAASamples())
	);
	win.SetVSync(true);
}

void BTFVisualizerLayer::OnUpdate(Crank::DeltaTime dt)
{
	m_Framebuffer->Bind();
	Crank::Renderer::GetAPI()->SetClearColor({1.0f, 1.0f, 1.0f, 1.0f});
	Crank::Renderer::GetAPI()->Clear();

	m_CameraController->OnUpdate(dt);

	Crank::Renderer::BeginScene(m_CameraController->GetCamera());

	m_Demos[m_SelectedDemoIdx]->OnUpdate();
		
	Crank::Renderer::EndScene();
	m_Framebuffer->Unbind();
}

void BTFVisualizerLayer::OnImGuiRender()
{
    static bool showDockspace = true;
    static bool opt_debugInfo = true;
    static ImGuiDockNodeFlags dockspace_flags = ImGuiDockNodeFlags_None;

    ImGuiWindowFlags window_flags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;

	// Fullscreen variant
    const ImGuiViewport* viewport = ImGui::GetMainViewport();
    ImGui::SetNextWindowPos(viewport->WorkPos);
    ImGui::SetNextWindowSize(viewport->WorkSize);
    ImGui::SetNextWindowViewport(viewport->ID);
    ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
    ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
    window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
    window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;

    if (dockspace_flags & ImGuiDockNodeFlags_PassthruCentralNode)
        window_flags |= ImGuiWindowFlags_NoBackground;

    ImGui::Begin("Crank Dockspace", &showDockspace, window_flags);
    ImGui::PopStyleVar(2);

    // Submit the DockSpace
    ImGuiIO& io = ImGui::GetIO();
    if (io.ConfigFlags & ImGuiConfigFlags_DockingEnable)
    {
        ImGuiID dockspace_id = ImGui::GetID("DockSpace");
        ImGui::DockSpace(dockspace_id, ImVec2(0.0f, 0.0f), dockspace_flags);
    }
    else
    {
        CR_CORE_WARN("Sandbox::OnImGuiRender(): Docking is disabled.");
    }

    if (ImGui::BeginMenuBar())
    {
		if (ImGui::BeginMenu("File"))
		{
			ImGui::Separator();
			if (ImGui::MenuItem("Close", NULL, false, showDockspace))
				Crank::Application::Get().Close();

			ImGui::EndMenu();
		}

		if (ImGui::BeginMenu("Demos"))
		{
			if (ImGui::MenuItem("Default PBR", nullptr, m_SelectedDemoIdx == 0)) {
				m_SelectedDemoIdx = 0;
				m_Demos[m_SelectedDemoIdx]->OnReload();
			}
			else if (ImGui::MenuItem("Plane", nullptr, m_SelectedDemoIdx == 1)) {
				m_SelectedDemoIdx = 1;
				m_Demos[m_SelectedDemoIdx]->OnReload();
			}
			else if (ImGui::MenuItem("Object", nullptr, m_SelectedDemoIdx == 2)) {
				m_SelectedDemoIdx = 2;
				m_Demos[m_SelectedDemoIdx]->OnReload();
			}
			else if (ImGui::MenuItem("Custom", nullptr, m_SelectedDemoIdx == 3)) {
				m_SelectedDemoIdx = 3;
				m_Demos[m_SelectedDemoIdx]->OnReload();
			}
			ImGui::EndMenu();
		}

        if (ImGui::BeginMenu("View"))
        {
            ImGui::MenuItem("Debug Info", NULL, &opt_debugInfo);
			ImGui::Separator();

            ImGui::EndMenu();
        }

        ImGui::EndMenuBar();
    }

	if (opt_debugInfo)
	{
		ImGuiWindowFlags debuginfoFlags = ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus |
											ImGuiWindowFlags_NoCollapse;
		ImGui::Begin("Debug Info", &opt_debugInfo, debuginfoFlags);
		ImGui::Separator();

		ImGui::Text("Fps: %.3f", Crank::Application::Get().GetFps());
		ImGui::Text("Per Frame: %.3f ms", Crank::Application::Get().GetFrameTime() * 1000.0f);

		ImGui::Separator();

		m_Demos[m_SelectedDemoIdx]->OnImGuiRender();

		ImGui::End();
	}

	// viewport
	ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, { 0, 0 });
	ImGui::Begin("Viewport");

	ImVec2 imguiViewportPanelSize = ImGui::GetContentRegionAvail();
	if (imguiViewportPanelSize.x != m_ViewportPanelSize.x || imguiViewportPanelSize.y != m_ViewportPanelSize.y)
	{
		m_ViewportPanelSize = { imguiViewportPanelSize.x, imguiViewportPanelSize.y };
		m_Framebuffer->Resize((unsigned int)m_ViewportPanelSize.x, (unsigned int)m_ViewportPanelSize.y);
		m_CameraController->Resize((unsigned int)m_ViewportPanelSize.x, (unsigned int)m_ViewportPanelSize.y);
	}

	auto& spec = m_Framebuffer->GetSpecification();
	ImGui::Image((ImTextureID)m_Framebuffer->GetColorAttachment()->GetHandle(), {(float)spec.Width, (float)spec.Height}, {0, 1}, {1, 0});
	ImGui::End();
	ImGui::PopStyleVar();


	// DOCKSPACE END
    ImGui::End(); 
}

void BTFVisualizerLayer::OnEvent(Crank::Event& e)
{
	m_CameraController->OnEvent(e);
}
