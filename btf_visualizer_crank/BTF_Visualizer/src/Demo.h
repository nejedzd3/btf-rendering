#pragma once
#include <Crank.h>

class Demo
{
public:
	virtual ~Demo() {}
	virtual void OnUpdate() = 0;
	virtual void OnLoad() = 0;
	virtual void OnReload() = 0;
	virtual void OnImGuiRender() = 0;
	virtual bool IsLoaded() = 0;
};


class PlaneDemo : public Demo
{
public:
	PlaneDemo(const Crank::Ref<Crank::OrbitalCameraController>& camController, const Crank::Ref<Crank::TextureAtlas>& normalAtlas, 
		const Crank::Ref<Crank::TextureAtlas>& utiaAtlas, const Crank::Ref<Crank::TextureAtlas>& testAtlas, const Crank::Ref<Crank::Shader>& btfShader);
	virtual ~PlaneDemo() {}

	virtual void OnUpdate() override;
	virtual void OnLoad() override;
	virtual void OnReload() override;
	virtual void OnImGuiRender() override;

	virtual bool IsLoaded() override;

private:
	std::string m_Name = "Plane Demo";
	bool m_Loaded = false;

	Crank::Ref<Crank::OrbitalCameraController> m_CamController = nullptr;

	Crank::Ref<Crank::Shader> m_BTFShader = nullptr;
	Crank::Ref<Crank::Mesh> m_Plane = nullptr;

	Crank::Ref<Crank::TextureAtlas> m_Atlas = nullptr;
	Crank::Ref<Crank::TextureAtlas> m_UTIAAtlas = nullptr;
	Crank::Ref<Crank::TextureAtlas> m_TestAtlas = nullptr;

	bool useBilinear = false;
	bool useSimpleViewVec = false;
	bool useFilmicToneMapping = true;

private:
	void Init();
	void enableTestAtlas();
	void enableNormalAtlas();
	void enableUTIAAtlas();
};


class ObjectDemo : public Demo
{
public:
	ObjectDemo(const Crank::Ref<Crank::OrbitalCameraController>& camController, const Crank::Ref<Crank::TextureAtlas>& normalAtlas,
		const Crank::Ref<Crank::TextureAtlas>& utiaAtlas, const Crank::Ref<Crank::TextureAtlas>& testAtlas, const Crank::Ref<Crank::Shader>& btfShader);
	virtual ~ObjectDemo() {}

	virtual void OnUpdate() override;
	virtual void OnLoad() override;
	virtual void OnReload() override;
	virtual void OnImGuiRender() override;

	virtual bool IsLoaded() override;

private:
	std::string m_Name = "Object Demo";
	bool m_Loaded = false;

	Crank::Ref<Crank::OrbitalCameraController> m_CamController = nullptr;

	Crank::Ref<Crank::Shader> m_BTFShader = nullptr;
	Crank::Ref<Crank::Mesh> m_Object = nullptr;

	Crank::Ref<Crank::TextureAtlas> m_Atlas = nullptr;
	Crank::Ref<Crank::TextureAtlas> m_UTIAAtlas = nullptr;
	Crank::Ref<Crank::TextureAtlas> m_TestAtlas = nullptr;

	bool useBilinear = false;
	bool useSimpleViewVec = false;
	bool useFilmicToneMapping = true;

private:
	void Init();
	void enableTestAtlas();
	void enableNormalAtlas();
	void enableUTIAAtlas();
};

class CustomDemo : public Demo
{
public:
	CustomDemo(const Crank::Ref<Crank::OrbitalCameraController>& camController, const Crank::Ref<Crank::TextureAtlas>& normalAtlas,
		const Crank::Ref<Crank::TextureAtlas>& utiaAtlas, const Crank::Ref<Crank::TextureAtlas>& testAtlas, const Crank::Ref<Crank::Shader>& btfShader);
	virtual ~CustomDemo() {}

	virtual void OnUpdate() override;
	virtual void OnLoad() override;
	virtual void OnReload() override;
	virtual void OnImGuiRender() override;

	virtual bool IsLoaded() override;

private:
	std::string m_Name = "Custom Demo";
	bool m_Loaded = false;

	Crank::Ref<Crank::OrbitalCameraController> m_CamController = nullptr;

	Crank::Ref<Crank::Shader> m_BTFShader = nullptr;
	Crank::Ref<Crank::Mesh> m_Object = nullptr;

	Crank::Ref<Crank::TextureAtlas> m_Atlas = nullptr;
	Crank::Ref<Crank::TextureAtlas> m_UTIAAtlas = nullptr;
	Crank::Ref<Crank::TextureAtlas> m_TestAtlas = nullptr;

	bool useBilinear = false;
	bool useSimpleViewVec = false;
	bool useFilmicToneMapping = true;

private:
	void Init();
	void enableTestAtlas();
	void enableNormalAtlas();
	void enableUTIAAtlas();
};


class DefaultDemo : public Demo
{
public:
	DefaultDemo(const Crank::Ref<Crank::OrbitalCameraController>& camController);
	virtual ~DefaultDemo() {}

	virtual void OnUpdate() override;
	virtual void OnLoad() override;
	virtual void OnReload() override;
	virtual void OnImGuiRender() override;

	virtual bool IsLoaded() override;

private:
	std::string m_Name = "Default Demo";
	bool m_Loaded = false;

	Crank::Ref<Crank::OrbitalCameraController> m_CamController = nullptr;

	Crank::Ref<Crank::Shader> m_PBRShader = nullptr;
	Crank::Ref<Crank::Mesh> m_Object = nullptr;
	Crank::Ref<Crank::Light> m_Light = nullptr;

private:
	void Init();
};
