#type vertex

in vec3 a_Position;

uniform mat4 u_ProjectionView;

out vec3 v_CubeMapTexCoords;

void main()
{
	v_CubeMapTexCoords = a_Position;
	gl_Position = (u_ProjectionView * vec4(a_Position, 1.0)).xyww; // swap z for w to achieve depth of 1.0 (maximal)
}

#type fragment

in vec3 v_CubeMapTexCoords;

uniform samplerCube u_CubeMap;

out vec4 fragColor;

void main()
{
	fragColor = texture(u_CubeMap, v_CubeMapTexCoords);
}