#include ./assets/shaders/includables/ColorFunctions.glsl
#include ./assets/shaders/includables/BTFDataStructures.glsl
#include ./assets/shaders/includables/BTFFunctions.glsl

#type vertex

in vec3 a_Position;
in vec3 a_Normal;
in vec2 a_TexCoord;

uniform mat4 u_PVM;
uniform mat4 u_ModelMatrix;

out vec3 v_PositionWorld;
out vec3 v_NormalWorld;
out vec2 v_TexCoord;


void main()
{
	v_PositionWorld = (u_ModelMatrix * vec4(a_Position, 1.0)).xyz;
	v_NormalWorld = (u_ModelMatrix * vec4(a_Normal, 1.0)).xyz;
	v_TexCoord = a_TexCoord;
	gl_Position = u_PVM * vec4(a_Position, 1.0);
}

#type fragment

in vec3 v_PositionWorld;
in vec3 v_NormalWorld;
in vec2 v_TexCoord;

uniform vec3 u_CameraPosition;
uniform BTFMaterial um_Material;

uniform bool u_Bilinear;
uniform bool u_SimpleViewVec;
uniform bool u_FilmicToneMapping;

out vec4 fragColor;

void main()
{
	vec3 view; 
	if (u_SimpleViewVec)
		view = normalize(u_CameraPosition - v_PositionWorld * 0.00000001); // ignore fragment world position
	else
		view = normalize(u_CameraPosition - v_PositionWorld);

	vec3 n = normalize(v_NormalWorld);
    vec3 axis = cross(n, vec3(0.0, 1.0, 0.0));
    float cos_theta = dot(vec3(0.0, 1.0, 0.0), n);


    // Rodrigues' rotation formula
    vec3 view_rot = view * cos_theta + cross(axis, view) * sin(acos(cos_theta)) + axis * dot(axis, view) * (1.0 - cos_theta);

	// (azimuth, elevation)
	vec2 sph_coord = CartesianToSpherical(view_rot);
	
	vec3 color = SampleBTF(sph_coord, v_TexCoord, um_Material, u_Bilinear);

	if (u_FilmicToneMapping)
		color = FilmicToneMapping(color);

	fragColor = vec4(color, 1.0);
}