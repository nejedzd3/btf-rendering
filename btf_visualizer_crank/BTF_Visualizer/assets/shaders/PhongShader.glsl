#include ./assets/shaders/includables/Common.glsl
#include ./assets/shaders/includables/PhongFunctions.glsl
#include ./assets/shaders/includables/ColorFunctions.glsl

#type vertex

in vec3 a_Position;
in vec3 a_Normal;
in vec2 a_TexCoord;

uniform mat4 u_PVM;
uniform mat4 u_ModelMatrix;

smooth out vec3 v_PositionWS;
smooth out vec3 v_NormalWS;
smooth out vec2 v_TexCoord;


void main()
{
	v_PositionWS = (u_ModelMatrix * vec4(a_Position, 1.0)).xyz;
	v_NormalWS = (u_ModelMatrix * vec4(a_Normal, 0.0)).xyz;
	v_TexCoord = a_TexCoord;

	gl_Position = u_PVM * vec4(a_Position.x, a_Position.y, a_Position.z, 1.0);
}

#type fragment

smooth in vec3 v_PositionWS;
smooth in vec3 v_NormalWS;
smooth in vec2 v_TexCoord;

uniform Light u_Light;
uniform PhongMaterial u_Material;
uniform vec3 u_CameraPosition;
//uniform sampler2D u_Texture;

out vec4 fragColor;

void main()
{
	vec3 color = vec3(0.0);

	color += BlinnPhongPointLight(u_Light, u_Material, v_PositionWS, v_NormalWS, u_CameraPosition);

	color = FilmicToneMapping(color);

	//fragColor *= texture(u_Texture, v_TexCoord);

	fragColor = vec4(color, 1.0);
}