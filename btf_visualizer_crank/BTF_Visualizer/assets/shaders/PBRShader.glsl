#include ./assets/shaders/includables/Common.glsl
#include ./assets/shaders/includables/PBRFunctions.glsl
#include ./assets/shaders/includables/ColorFunctions.glsl

#type vertex

in vec3 a_Position;
in vec3 a_Normal;
in vec2 a_TexCoord;

uniform mat4 u_PVM;
uniform mat4 u_ModelMatrix;

out vec3 v_PositionWorld;
out vec3 v_Normal;
out vec2 v_TexCoord;


void main()
{
	v_PositionWorld = (u_ModelMatrix * vec4(a_Position, 1.0)).xyz;
	v_Normal = a_Normal;
	v_TexCoord = a_TexCoord;

	gl_Position = u_PVM * vec4(a_Position, 1.0);
}

#type fragment

in vec3 v_PositionWorld;
in vec3 v_Normal;
in vec2 v_TexCoord;

uniform vec3 u_CameraPosition;
uniform PBRMaterial um_Material;
uniform Light ul_Light;
//uniform sampler2D u_Texture;

out vec4 fragColor;

void main()
{
	vec3 color = vec3(0.0, 0.0, 0.0);

	color += PBRPointLight(ul_Light, um_Material, v_Normal, v_PositionWorld, u_CameraPosition);

	color = FilmicToneMapping(color);

	fragColor = vec4(color, 1.0);

	fragColor += v_TexCoord.xxxx * 0.0000001; // for TexCoord to not get optimised out by OpenGL
}