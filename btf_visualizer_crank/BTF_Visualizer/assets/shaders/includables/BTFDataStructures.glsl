#type fragment_data_structure
struct BTFMaterial 
{
  sampler2D atlas;
  uvec2		atlas_res;
  uvec2		tex_cnt;
  uvec2		single_tex_res;
  vec2		sph_coord_offset;
  bool		is_utia;
};
