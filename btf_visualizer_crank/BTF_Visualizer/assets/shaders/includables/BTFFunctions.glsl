#include ./assets/shaders/includables/Constants.glsl
#include ./assets/shaders/includables/BTFDataStructures.glsl

#type fragment_function
vec2 CartesianToSpherical(vec3 cart_coords)
{
	float elevation = acos(cart_coords.y);
	if (elevation == 0.0)
		return vec2(0.0, 0.0);
	float azimuth = PI + sign(cart_coords.z) * acos(cart_coords.x / sqrt(cart_coords.x * cart_coords.x + cart_coords.z * cart_coords.z));
	return vec2(azimuth, elevation);
}

#type fragment_function
vec3 SphericalToCartesian(vec2 sph_coords)
{
	float x = sin(sph_coords.y) * cos(sph_coords.x);
	float y = cos(sph_coords.y);
	float z = sin(sph_coords.y) * sin(sph_coords.x);
	return vec3(x, y, z);
}

#type fragment_function
uvec2 GetUtiaAtlasStartSampleIdx(vec2 sph_coord)
{
	int elev_idx = int(round(sph_coord.y / radians(15.0)));
	int azi_idx = int(round(sph_coord.x / radians(360.0 / UTIA_AZIMUTH_DIVISONS[elev_idx])));

	// loop along azimuth
	if (azi_idx == UTIA_AZIMUTH_DIVISONS[elev_idx])
		azi_idx = 0;

	int raveled_idx = 0;
	for (int i = 0; i < elev_idx; ++i)
	{
		raveled_idx += UTIA_AZIMUTH_DIVISONS[i];
	}
	raveled_idx += azi_idx;

	float raveled_ratio = raveled_idx / 9.0;

	uvec2 start_sample_idx = uvec2(0, 0);
	start_sample_idx.x = uint((raveled_ratio - float(floor(raveled_ratio))) * 9.0);
	start_sample_idx.y = uint(floor(raveled_ratio));

	return start_sample_idx;
}

#type fragment_function
float ComputeTriangleArea(vec3 pt1, vec3 pt2, vec3 pt3)
{
	vec3 x = pt1 - pt2;
	vec3 y = pt3 - pt2;
	return length(cross(x, y)) / 2.0;
}

#type fragment_function
vec3 SampleAtlas_UTIA_Simple(vec2 sph_coord, vec2 tex_coord, BTFMaterial material)
{
	float elev_offset = radians(15.0);

	float elev_ratio = sph_coord.y / elev_offset;
	int elev_idx = int(round(elev_ratio));

	// index out of range along elevation
	if (elev_idx >= 6)
		return vec3(0.0);

	uvec2 start_sample_idx = GetUtiaAtlasStartSampleIdx(sph_coord);
	
	vec2 uv_scaled = (tex_coord * material.single_tex_res + start_sample_idx * material.single_tex_res) / material.atlas_res;
	return texture(material.atlas, uv_scaled).xyz;
}

#type fragment_function
vec3 SampleAtlas_UTIA_Bilinear(vec2 sph_coord, vec2 tex_coord, BTFMaterial material)
{
	float elev_offset = radians(15.0);
	float elev_ratio = sph_coord.y / elev_offset;

	float nearest_elev1 = ceil(elev_ratio);
	float nearest_elev2 = floor(elev_ratio);

	// nearest elevation is out of bounds
	if (nearest_elev2 >= 6)
		return vec3(0.0);

	float azi1_offset = radians(360.0 / UTIA_AZIMUTH_DIVISONS[int(nearest_elev1)]);
	float azi1_ratio = sph_coord.x / azi1_offset;
	vec2 azi1_nearest = vec2(floor(azi1_ratio) * azi1_offset, ceil(azi1_ratio) * azi1_offset); // radians of two closest azimuths on elev1

	float azi2_offset = radians(360.0 / UTIA_AZIMUTH_DIVISONS[int(nearest_elev2)]);
	float azi2_ratio = sph_coord.x / azi2_offset;
	vec2 azi2_nearest = vec2(floor(azi2_ratio) * azi2_offset, ceil(azi2_ratio) * azi2_offset); // radians of two closest azimuths on elev2

	// ditch one furthest from sample
	float distances[4] = float[](abs(azi1_nearest[0] - sph_coord.x), abs(azi1_nearest[1] - sph_coord.x), 
								 abs(azi2_nearest[0] - sph_coord.x), abs(azi2_nearest[1] - sph_coord.x));
	int ditch_idx = 0;
	for (int i = 0; i < 4; i++)
	{
		if (distances[i] > distances[ditch_idx])
			ditch_idx = i;
	}

	// ditch one azimuthal value by index
	vec2 triangle_sph_pts[3];
	if (ditch_idx < 2)
	{
		triangle_sph_pts[0] = vec2(azi1_nearest[1 - ditch_idx], nearest_elev1 * elev_offset);
		triangle_sph_pts[1] = vec2(azi2_nearest[0], nearest_elev2 * elev_offset);
		triangle_sph_pts[2] = vec2(azi2_nearest[1], nearest_elev2 * elev_offset);
	}
	else
	{
		triangle_sph_pts[0] = vec2(azi1_nearest[0], nearest_elev1 * elev_offset);
		triangle_sph_pts[1] = vec2(azi1_nearest[1], nearest_elev1 * elev_offset);
		triangle_sph_pts[2] = vec2(azi2_nearest[3 - ditch_idx], nearest_elev2 * elev_offset);
	}

	vec3 sample_cart_pt = SphericalToCartesian(sph_coord);

	float weight[3];
	float weightScale = 0.0;
	uvec2 start_idx[3];
	for (int i = 0; i < 3; i++)
	{
		weight[i] = ComputeTriangleArea(sample_cart_pt, 
										SphericalToCartesian(triangle_sph_pts[(i + 1) % 3]), 
										SphericalToCartesian(triangle_sph_pts[(i + 2) % 3])
										);
		weightScale += weight[i];
		start_idx[i] = GetUtiaAtlasStartSampleIdx(triangle_sph_pts[i]);
	}

	// bilinear sampling
	vec3 sampled_color = vec3(0.0);
	for (uint i = 0; i < 3; ++i)
	{
		// index out of range along elevation
		if (triangle_sph_pts[i].y > radians(75.0))
			continue;
		
		vec2 scaled_uv = (tex_coord * material.single_tex_res + start_idx[i] * material.single_tex_res) / material.atlas_res;
		sampled_color += texture(material.atlas, scaled_uv).xyz * (weight[i] / weightScale);
	
	}

	return sampled_color;
}


#type fragment_function
vec3 SampleBTF_Simple(vec2 sph_coord, vec2 tex_coord, BTFMaterial material)
{
	vec2 sph_ratio = sph_coord / material.sph_coord_offset;

	// simple sampling
	uvec2 start_sample_idx = uvec2(round(sph_ratio.x), round(sph_ratio.y));

	// index out of range along elevation
	if (start_sample_idx.y >= material.tex_cnt.y)
		return vec3(0.0, 0.0, 0.0);

	// loop along azimuth
	if (start_sample_idx.x >= material.tex_cnt.x)
		start_sample_idx.x = 0;

	uvec2 start_sample_px = start_sample_idx * material.single_tex_res;
	
	vec2 uv_scaled = (tex_coord * material.single_tex_res + start_sample_px) / material.atlas_res;
	return texture(material.atlas, uv_scaled).xyz;
}


#type fragment_function
vec3 SampleBTF_Bilinear(vec2 sph_coord, vec2 tex_coord, BTFMaterial material)
{
	vec2 sph_ratio = sph_coord / material.sph_coord_offset;
	uvec2 sph_rat_floor = uvec2(floor(sph_ratio));
	uvec2 sph_rat_ceil = uvec2(ceil(sph_ratio));

	// bilinear sampling
	uvec2 start_idx[4];
	float weight[4];

	// left lower - [1, 1]
	start_idx[0] = sph_rat_floor;
	weight[0] = (sph_rat_ceil.x - sph_ratio.x) * (sph_rat_ceil.y - sph_ratio.y);

	// left upper - [1, 2]
	start_idx[1] = uvec2(sph_rat_floor.x, sph_rat_ceil.y);
	weight[1] = (sph_rat_ceil.x - sph_ratio.x) * (sph_ratio.y - sph_rat_floor.y);

	// right lower - [2, 1]
	start_idx[2] = uvec2(sph_rat_ceil.x, sph_rat_floor.y);
	weight[2] = (sph_ratio.x - sph_rat_floor.x) * (sph_rat_ceil.y - sph_ratio.y);

	// right upper - [2, 2]
	start_idx[3] = sph_rat_ceil;
	weight[3] = (sph_ratio.x - sph_rat_floor.x) * (sph_ratio.y - sph_rat_floor.y);

	vec3 sampled_color = vec3(0.0);


	for (uint i = 0; i < 4; ++i)
	{
		// index out of range along elevation
		if (start_idx[i].y >= material.tex_cnt.y)
			continue;

		// loop along azimuth
		if (start_idx[i].x >= material.tex_cnt.x)
			start_idx[i].x = 0;

		vec2 scaled_uv = (tex_coord * material.single_tex_res + start_idx[i] * material.single_tex_res) / material.atlas_res;
		sampled_color += texture(material.atlas, scaled_uv).xyz * weight[i];
	}

	return sampled_color;
}

#type fragment_function
vec3 SampleBTF(vec2 sph_coord, vec2 tex_coord, BTFMaterial material, bool use_bilinear)
{
	if (use_bilinear)
	{
		if (material.is_utia)
			return SampleAtlas_UTIA_Bilinear(sph_coord, tex_coord, material);
		else
			return SampleBTF_Bilinear(sph_coord, tex_coord, material);
	}
	else
	{
		if (material.is_utia)
			return SampleAtlas_UTIA_Simple(sph_coord, tex_coord, material);
		else
			return SampleBTF_Simple(sph_coord, tex_coord, material);
	}
}