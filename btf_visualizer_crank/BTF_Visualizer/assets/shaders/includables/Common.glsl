#type fragment_data_structure
struct Light 
{
  // w == 1, then it's a position | w == 0, then it's a direction
  vec4 origin; 
  vec3 color;
  float intensity;
};