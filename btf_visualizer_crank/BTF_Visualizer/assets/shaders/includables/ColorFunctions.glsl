#type fragment_function
vec3 FilmicToneMapping(vec3 color)
{
    color = max(vec3(0.0), color - 0.004);
    color = (color * (6.2 * color + 0.5)) / (color * (6.2 * color + 1.7) + 0.06);
    return color;
}

#type fragment_function
vec3 GammaCorrection(vec3 color)
{
    color = color / (color + vec3(1.0));
	color = pow(color, vec3(1.0/2.2));
    return color;
}