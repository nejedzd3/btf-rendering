#include ./assets/shaders/includables/PhongDataStructures.glsl
#include ./assets/shaders/includables/Common.glsl

#type fragment_function
vec3 BlinnPhongPointLight(Light light, PhongMaterial material, vec3 positionWS, vec3 normalWS, vec3 cameraPosition)
{
	vec3 lightDir = normalize(light.origin.xyz - positionWS);
	vec3 viewDir = normalize(cameraPosition - positionWS);
	vec3 halfwayDir = normalize(lightDir + viewDir);

	float dist = distance(positionWS, light.origin.xyz);
	float attenuate = 1.0 / (1.0 + dist*dist);
	vec3 radiance = light.color * light.intensity;

	vec3 outCol = vec3(0.0);

	outCol += material.color * material.ambient;
	outCol += material.color * material.diffuse * max(0.0, dot(normalWS, lightDir));
	outCol += material.color * material.specular * pow(max(dot(normalWS, halfwayDir), 0.0), material.shininess);

	outCol *= attenuate * radiance;

	return outCol;
}