#type fragment_data_structure
struct PBRMaterial 
{
  vec3 color;
  float roughness;
  float metallic;
};
