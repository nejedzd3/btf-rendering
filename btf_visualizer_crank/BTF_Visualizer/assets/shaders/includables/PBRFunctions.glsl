#include ./assets/shaders/includables/Constants.glsl
#include ./assets/shaders/includables/PBRDataStructures.glsl
#include ./assets/shaders/includables/Common.glsl

#type fragment_function
float GGXDistribution(float nDotH, float roughness)
{
	// empirically it's better to take power of 4 instead of 2
	float alpha2 = roughness * roughness * roughness * roughness;
	
	float denominator = nDotH * nDotH * (alpha2 - 1.0) + 1.0;
	return alpha2 / (PI * denominator * denominator);
}

#type fragment_function
float SmithGeometry(float dotProduct, float roughness)
{
	float k = ((roughness + 1.0) * (roughness + 1.0)) / 8.0;
	float denominator = dotProduct * (1.0 - k) + k;

	return dotProduct / denominator;
}

#type fragment_function
float SchlickGGXGeometry(float nDotV, float nDotL, float roughness)
{
	return SmithGeometry(nDotV, roughness) * SmithGeometry(nDotL, roughness);
}

#type fragment_function
vec3 SchlickFresnel(float cosTheta, vec3 fZero)
{
	return fZero + (vec3(1.0) - fZero) * pow(clamp(1.0 - cosTheta, 0.0, 1.0), 5);
}

#type fragment_function
vec3 PBRPointLight(Light light, PBRMaterial material, vec3 normal, vec3 positionWorld, vec3 cameraPosition)
{
	vec3 radiance = light.color * light.intensity;

	vec3 l = vec3(0.0);

	if (light.origin.w == 0.0)
	{
		l = -light.origin.xyz;
	}
	else
	{
		l = light.origin.xyz - positionWorld;
		float LightToPixelDistance = length(l);
		l = normalize(l);
		radiance = radiance / (LightToPixelDistance * LightToPixelDistance);
	}

	vec3 n = normalize(normal);
	vec3 v = normalize(cameraPosition - positionWorld);
	vec3 h = normalize(v + l);

	float nDotH = max(dot(n, h), 0.0);
	float nDotL = max(dot(n, l), 0.0);
	float nDotV = max(dot(n, v), 0.0);
	float vDotH = max(dot(v, h), 0.0);


	vec3 fZero = vec3(0.04);
	fZero = mix(fZero, material.color, material.metallic);
	vec3 F = SchlickFresnel(vDotH, fZero);

	vec3 kS = F;
	vec3 kD = vec3(1.0) - kS;
	kD *= 1.0 - material.metallic;

	float cappedRoughness = max(material.roughness, MIN_POINT_LIGHT_ROUGHNESS);

	vec3 specBRDF_nominator = GGXDistribution(nDotH, cappedRoughness) * F * SchlickGGXGeometry(nDotV, nDotL, cappedRoughness);
	vec3 specBRDF_denominator = vec3(4.0 * nDotV * nDotL + 0.0001);
	vec3 specBRDF = specBRDF_nominator / specBRDF_denominator;

	vec3 diffuseBRDF = kD * material.color / PI;

	vec3 lZero = (diffuseBRDF + specBRDF) * radiance * nDotL;
	
	return lZero;
}
