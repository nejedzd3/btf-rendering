#type fragment_constant
const float PI = 3.14159265359;

#type fragment_constant
const float MIN_POINT_LIGHT_ROUGHNESS = 0.03;

#type fragment_constant
// seventh value just for calculations - no actual data are on the seventh elevation!
const int UTIA_AZIMUTH_DIVISONS[7] = int[](1, 6, 12, 18, 20, 24, 30); 