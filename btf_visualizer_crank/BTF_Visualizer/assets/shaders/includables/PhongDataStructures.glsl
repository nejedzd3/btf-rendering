#type fragment_data_structure
struct PhongMaterial {
  vec3 color;
  float ambient;
  float diffuse;
  float specular;
  float shininess;
};