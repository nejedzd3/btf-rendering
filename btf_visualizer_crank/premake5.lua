workspace "Crank"
    architecture "x64"
    startproject "BTF_Visualizer"

    configurations
    {
        "Debug",
        "Release",
        "Dist"
    }

outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

IncludeDir = {}
IncludeDir["GLFW"] = "Crank/Dependencies/GLFW/include"
IncludeDir["GLM"] = "Crank/Dependencies/glm/include"
IncludeDir["SPDLOG"] = "Crank/Dependencies/spdlog/include"
IncludeDir["GLAD"] = "Crank/Dependencies/GLAD/include"
IncludeDir["IMGUI"] = "Crank/Dependencies/ImGui"
IncludeDir["stb_image"] = "Crank/Dependencies/stb_image"
IncludeDir["assimp"] = "Crank/Dependencies/assimp/include"


group "Dependencies"
    include "Crank/Dependencies/assimp"
    include "Crank/Dependencies/GLFW"
    include "Crank/Dependencies/GLAD"
    include "Crank/Dependencies/ImGui"

group ""

project "Crank"
    location "Crank"
    kind "StaticLib"
    staticruntime "On"
    language "C++"
    cppdialect "C++17"

    targetdir("bin/" .. outputdir .. "/%{prj.name}")
    objdir("bin-int/" .. outputdir .. "/%{prj.name}")

    pchheader "crpch.h"
    pchsource "%{prj.name}/src/crpch.cpp"

    defines
    {
        "CR_RENDERER_API_OPENGL",
        "_CRT_SECURE_NO_WARNINGS"
    }

    files
    {
        "%{prj.name}/Dependencies/stb_image/**.h",
        "%{prj.name}/Dependencies/stb_image/**.cpp",

        "%{prj.name}/Dependencies/glm/**.hpp",
        "%{prj.name}/Dependencies/glm/**.inl",

        "%{prj.name}/src/**.h",
        "%{prj.name}/src/**.cpp"
    }

    includedirs
    {
        "%{prj.name}/src",
        "%{IncludeDir.GLFW}",
        "%{IncludeDir.GLM}",
        "%{IncludeDir.IMGUI}",
        "%{IncludeDir.SPDLOG}",
        "%{IncludeDir.GLAD}",
        "%{IncludeDir.stb_image}",
        "%{IncludeDir.assimp}"
    }

    links
    {
        "GLFW",
        "GLAD",
        "ImGui",
        "opengl32.lib",
        "dwmapi.lib",
        "assimp"
    }

    filter "system:windows"
        systemversion "latest"

        defines
        {
            "CR_PLATFORM_WINDOWS",
            "GLFW_INCLUDE_NONE"
        }

        filter "configurations:Debug"
            defines "CR_DEBUG"
            runtime "Debug"
            symbols "On"

        filter "configurations:Release"
            defines "CR_RELEASE"
            runtime "Release"
            optimize "On"

        filter "configurations:Dist"
            defines "CR_DIST"
            runtime "Release"
            optimize "On"


project "BTF_Visualizer"
    location "BTF_Visualizer"
    kind "ConsoleApp"
    staticruntime "On"
    language "C++"

    targetdir("bin/" .. outputdir .. "/%{prj.name}")
    objdir("bin-int/" .. outputdir .. "/%{prj.name}")

    files
    {
        "%{prj.name}/src/**.h",
        "%{prj.name}/src/**.cpp",
        "%{prj.name}/assets/**.glsl"

    }

    includedirs
    {
        "Crank/src",
        "%{IncludeDir.GLFW}",
        "%{IncludeDir.GLM}",
        "%{IncludeDir.IMGUI}",
        "%{IncludeDir.SPDLOG}",
        "%{IncludeDir.GLAD}",
        "%{IncludeDir.stb_image}",
        "%{IncludeDir.assimp}"
    }

    filter "system:windows"
        cppdialect "C++17"
        systemversion "latest"

        defines
        {
            "CR_PLATFORM_WINDOWS"
        }

        links
        {
            "Crank"
        }

        filter "configurations:Debug"
            defines "CR_DEBUG"
            runtime "Debug"
            symbols "On"

        filter "configurations:Release"
            defines "CR_RELEASE"
            runtime "Release"
            optimize "On"

        filter "configurations:Dist"
            defines "CR_DIST"
            runtime "Release"
            optimize "On"