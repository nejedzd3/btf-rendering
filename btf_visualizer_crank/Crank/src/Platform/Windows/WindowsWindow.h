#pragma once
#include "Crank/Core/Window.h"
#include "Crank/Core/Core.h"
#include "Platform/OpenGL/OpenGLContext.h"

namespace Crank
{

	class WindowsWindow : public Window
	{
	public:
		WindowsWindow(const Specification& spec);
		virtual ~WindowsWindow();

		virtual void OnUpdate() override;
		virtual bool OnResize(WindowResizedEvent& e) override;

		inline virtual unsigned int GetWidth() const override { return m_Data.Width; }
		inline virtual unsigned int GetHeight() const override { return m_Data.Height; }
		inline virtual float GetAspectRatio() const override { return m_Data.Width / (float)m_Data.Height; }
		inline virtual unsigned int GetMSAASamples() const override { return m_Data.MSAASamples; }

		inline void SetEventCallback(const std::function<void(Event&)>& callback) override { m_Data.EventCallback = callback; }
		
		virtual void SetVSync(bool enabled) override;
		virtual bool IsVSync() const override;
		virtual bool IsMSAA() const override;

		inline virtual void* GetNativeWindow() override { return m_Window; }

	private:
		virtual void Init(const Specification& spec);
		virtual void Shutdown();

	private:
		struct Data
		{
		public:
			// from Window::Specification
			unsigned int	Width = 1280;
			unsigned int	Height = 720;
			const char* Title = "Default Window";
			unsigned int	MSAASamples = 0; // off by default
			bool			UseVSync = false;

			// unique
			std::function<void(Event&)> EventCallback;

		public:
			Data() {}
			Data(const Window::Specification& spec)
			{
				Width = spec.Width;
				Height = spec.Height;
				Title = spec.Title;
				MSAASamples = spec.MSAASamples;
				UseVSync = spec.UseVSync;
			}
		};
	private:
		GLFWwindow* m_Window;
		Ref<RenderingContext> m_Context;

		Data m_Data;
	};
}

