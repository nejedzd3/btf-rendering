#include "crpch.h"
#include "WindowsWindow.h"
#include "Crank/Core/Log.h"

#include <GLFW/glfw3.h>

#include "Crank/Events/ApplicationEvents.h"
#include "Crank/Events/KeyboardEvents.h"
#include "Crank/Events/MouseEvents.h"

namespace Crank
{
    static bool s_GLFWInitialized = false;

    static void GLFWErrorCallback(int error, const char* description) 
    {
        CR_CORE_ERROR("GLFW Error ({0})", description);
    }

    Window * Window::Create(const Window::Specification& spec)
    {
        return new WindowsWindow(spec);
    }

    WindowsWindow::WindowsWindow(const Window::Specification& spec)
    {
        Init(spec);
    }

    WindowsWindow::~WindowsWindow()
    {
        Shutdown();
    }

    void WindowsWindow::OnUpdate()
    {
        glfwPollEvents();
        m_Context->SwapBuffers();
    }

    bool WindowsWindow::OnResize(WindowResizedEvent& e)
    {
        m_Data.Width = e.GetNewWidth();
        m_Data.Height = e.GetNewHeight();

        return false;
    }

    void WindowsWindow::SetVSync(bool enabled)
    {
        enabled ? glfwSwapInterval(1) : glfwSwapInterval(0);
        m_Data.UseVSync = enabled;
    }

    bool WindowsWindow::IsVSync() const
    {
        return m_Data.UseVSync;
    }

    bool WindowsWindow::IsMSAA() const
    {
        return m_Data.MSAASamples > 0;
    }

    void WindowsWindow::Init(const Specification& spec)
    {
        m_Data = Data(spec);

        CR_CORE_INFO("Creating window {0} ({1}, {2})", spec.Title, spec.Width, spec.Height);

        if (!s_GLFWInitialized)
        {
            int success = glfwInit();
            CR_CORE_ASSERT(success, "Could not initialize GLFW!")

            glfwSetErrorCallback(GLFWErrorCallback);

            s_GLFWInitialized = true;
        }

        // Handle MSAA
        if (m_Data.MSAASamples > 0)
        {
            glfwWindowHint(GLFW_SAMPLES, m_Data.MSAASamples);
        }

        m_Window = glfwCreateWindow((int)spec.Width, (int)spec.Height, spec.Title, nullptr, nullptr);
        CR_CORE_ASSERT(m_Window, "Could not create window!");

        m_Context = CreateRef<OpenGLContext>(m_Window);
        m_Context->Init();

        glfwSetWindowUserPointer(m_Window, &m_Data);
        SetVSync(spec.UseVSync);

        // Set GLFW callbacks
        glfwSetWindowSizeCallback(m_Window, [](GLFWwindow* window, int width, int height) 
            {
            Data& data = *(Data*) glfwGetWindowUserPointer(window);

            data.Width = width;
            data.Height = height;

            WindowResizedEvent event(width, height);
            data.EventCallback(event);
        });
        glfwSetWindowCloseCallback(m_Window, [](GLFWwindow* window) 
            {
                Data& data = *(Data*)glfwGetWindowUserPointer(window);

            WindowClosedEvent event;
            data.EventCallback(event);
         });

        glfwSetKeyCallback(m_Window, [](GLFWwindow* window, int key, int scancode, int action, int mods) 
            {
                Data& data = *(Data*)glfwGetWindowUserPointer(window);

            switch (action) 
            {
                case GLFW_PRESS:
                {
                    KeyPressedEvent event(key);
                    data.EventCallback(event);
                    break;
                }
                case GLFW_RELEASE:
                {
                    KeyReleasedEvent event(key);
                    data.EventCallback(event);
                    break;
                }
                // TODO: Repeating button press event handling
                case GLFW_REPEAT:
                {
                    KeyPressedEvent event(key);
                    data.EventCallback(event);
                    break;
                }
            }
        });

        glfwSetCharCallback(m_Window, [](GLFWwindow* window, unsigned int character) 
            {
                Data& data = *(Data*)glfwGetWindowUserPointer(window);

                KeyTypedEvent event((KeyCode)character);
                data.EventCallback(event);
            });

        glfwSetMouseButtonCallback(m_Window, [](GLFWwindow* window, int button, int action, int mods) 
            {
                Data& data = *(Data*)glfwGetWindowUserPointer(window);

            switch (action) {
                case GLFW_PRESS:
                {
                    MouseClickedEvent event(button);
                    data.EventCallback(event);
                    break;
                }
                case GLFW_RELEASE:
                {
                    MouseReleasedEvent event(button);
                    data.EventCallback(event);
                    break;
                }

            }

        });

        glfwSetScrollCallback(m_Window, [](GLFWwindow* window, double xOffset, double yOffset)
            {
                Data& data = *(Data*)glfwGetWindowUserPointer(window);
            
            MouseScrolledEvent event((float)xOffset, (float)yOffset);
            data.EventCallback(event);
        });

        glfwSetCursorPosCallback(m_Window, [](GLFWwindow* window, double xPos, double yPos)
            {
                Data& data = *(Data*)glfwGetWindowUserPointer(window);

            MouseMovedEvent event((float)xPos, (float)yPos);
            data.EventCallback(event);
        });
    }

    void WindowsWindow::Shutdown()
    {
        glfwDestroyWindow(m_Window);
    }

}