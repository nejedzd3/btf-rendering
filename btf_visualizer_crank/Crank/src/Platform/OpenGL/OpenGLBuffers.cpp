#include "crpch.h"
#include "OpenGLBuffers.h"

#include <glad/glad.h>

namespace Crank
{
	// -------------------------------------------------------------------------------------------------
	// --------------------------------------------VBO--------------------------------------------------
	OpenGLVertexBuffer::OpenGLVertexBuffer(size_t size)
	{
		glGenBuffers(1, &m_Handle);
		Bind();

		glBufferData(GL_ARRAY_BUFFER, size, (const void*)0, GL_DYNAMIC_DRAW); // GL_DYNAMIC_DRAW -> data will be set afterwards
	}

	OpenGLVertexBuffer::OpenGLVertexBuffer(size_t size, float* data)
	{
		glGenBuffers(1, &m_Handle);
		Bind();

		glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
	}

	OpenGLVertexBuffer::~OpenGLVertexBuffer()
	{
		glDeleteBuffers(1, &m_Handle);
	}

	void OpenGLVertexBuffer::Bind() const
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_Handle);
	}

	void OpenGLVertexBuffer::Unbind() const
	{
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}


	void OpenGLVertexBuffer::SetData(size_t byteSize, float* data, size_t offset)
	{
		Bind();
		glBufferSubData(GL_ARRAY_BUFFER, offset, byteSize, data);
	}



	// -------------------------------------------------------------------------------------------------
	// --------------------------------------------EBO--------------------------------------------------


	OpenGLElementBuffer::OpenGLElementBuffer(unsigned int count)
	{
		glGenBuffers(1, &m_Handle);
		Bind();

		glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(unsigned int), (const void*)0, GL_DYNAMIC_DRAW);
		m_Count = count;
	}

	OpenGLElementBuffer::OpenGLElementBuffer(unsigned int count, unsigned int* data)
	{
		glGenBuffers(1, &m_Handle);
		Bind();

		glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(unsigned int), data, GL_STATIC_DRAW);
		m_Count = count;
	}

	OpenGLElementBuffer::~OpenGLElementBuffer()
	{
	}

	void OpenGLElementBuffer::Bind() const
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_Handle);
	}

	void OpenGLElementBuffer::Unbind() const
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}

	void OpenGLElementBuffer::SetData(unsigned int count, unsigned int* data, size_t offset)
	{
		if (count > m_Count)
		{
			CR_CORE_ERROR("OpenGLElementBuffer::SetData(): Not enough allocated memory in buffer. No data set.");
			return;
		}


		Bind();
		glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, offset, count * sizeof(unsigned int), data);
	}

	// -------------------------------------------------------------------------------------------------
	// -------------------------------------------Helper------------------------------------------------

	unsigned int BufferHelper::BufferDataTypeToOpenGLType(BufferDataType type)
	{
		switch (type)
		{
		case BufferDataType::Float1:
		case BufferDataType::Float2:
		case BufferDataType::Float3:
		case BufferDataType::Float4:
			return GL_FLOAT;

		case BufferDataType::None:
			return 0;

		default:
			CR_CORE_ERROR("BufferHelper::BufferDataTypeToOpenGLType(): Unknown buffer data type.");
			return 0;
		}
	}

}
