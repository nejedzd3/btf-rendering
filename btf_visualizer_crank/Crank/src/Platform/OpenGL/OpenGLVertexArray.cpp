#include "crpch.h"
#include "OpenGLVertexArray.h"

#include <glad/glad.h>

namespace Crank
{
    OpenGLVertexArray::OpenGLVertexArray(const Ref<Shader>& shader)
        : m_AssignedShader(shader)
    {
        glGenVertexArrays(1, &m_Handle);
        Bind();
    }

    OpenGLVertexArray::~OpenGLVertexArray()
    {
    }

    void OpenGLVertexArray::Bind() const
    {
        glBindVertexArray(m_Handle);
    }

    void OpenGLVertexArray::Unbind() const
    {
        glBindVertexArray(0);
    }

    void OpenGLVertexArray::AddVertexBuffer(const Ref<VertexBuffer>& vertexBuffer)
    {
        if (!vertexBuffer->GetLayout().GetElements().size())
        {
            CR_CORE_ERROR("OpenGLVertexArray::AddVertexBuffer(): Given vertex buffer has no layout.");
            return;
        }
        m_VertexBuffers.push_back(vertexBuffer);

        Bind();
        vertexBuffer->Bind();

        for (auto& element : vertexBuffer->GetLayout())
        {
            int elIndex = m_AssignedShader->GetLocation(element.Name);
            if (elIndex == -1)
                continue;

            glEnableVertexAttribArray(elIndex);
            glVertexAttribPointer(
                (GLuint) elIndex,
                (GLint) element.TypeCount,
                (GLenum) BufferHelper::BufferDataTypeToOpenGLType(element.Type),
                (GLboolean) element.Normalized ? GL_TRUE : GL_FALSE,
                (GLsizei) vertexBuffer->GetLayout().GetStride(),
                (const void*) element.Offset
            );
        }

    }

    void OpenGLVertexArray::AddElementBuffer(const Ref<ElementBuffer>& elementBuffer)
    {
        m_ElementBuffer = elementBuffer;
        
        Bind();
        m_ElementBuffer->Bind();
    }

    unsigned int OpenGLVertexArray::GetVertexCount() const
    {
        unsigned int vertexCount = 0;
        for (auto& vbo : m_VertexBuffers)
        {
            unsigned int tmpVertexCount = vbo->GetLayout().GetVertexCount();

            // number of vertices in multiple VBOs should be the same
            if (vertexCount != tmpVertexCount)
                CR_CORE_WARN("OpenGLVertexArray::GetVertexCount(): Vertex count differs in different VBOs");
            
            vertexCount = tmpVertexCount;
        }
        return vertexCount;
    }
}
