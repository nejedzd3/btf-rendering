#pragma once
#include "Crank/Renderer/Texture.h"

typedef unsigned int GLenum;

namespace Crank
{
	class OpenGLTextureHelper
	{
	public:
		static GLenum ParameterToOpenGLTextureParameter(Texture::Parameter p);
		static GLenum GetOpenGLDataFormat(const Texture::Specification& spec);
		static GLenum GetOpenGLInternalFormat(const Texture::Specification& spec);
	};

	class OpenGLTexture2D : public Texture2D
	{
	public:
		OpenGLTexture2D(const Specification& spec);
		OpenGLTexture2D(const std::string& path, bool useMipMaps = true);
		virtual ~OpenGLTexture2D();

		virtual Specification& GetSpec() override { return m_Specification; }
		virtual const Specification& GetSpec() const override { return m_Specification; }
		virtual unsigned int GetWidth() const override { return m_Specification.Width; }
		virtual unsigned int GetHeight() const override { return m_Specification.Height; }
		virtual unsigned int GetHandle() const override { return m_Handle; }

		virtual void SetParameter(Parameter parameter, int value) override;
		virtual void SetData(unsigned char* data) override;

		virtual void Bind(unsigned int slot = 0) const override;

	private:
		unsigned int m_Handle = 0;
		Specification m_Specification;
		std::string m_Path;

	};


	class OpenGLTexture2DMultiSampled : public Texture2DMultiSampled
	{
	public:
		OpenGLTexture2DMultiSampled(const Specification& spec);
		virtual ~OpenGLTexture2DMultiSampled();

		virtual Specification& GetSpec() override { return m_Specification; }
		virtual const Specification& GetSpec() const override { return m_Specification; }
		virtual unsigned int GetWidth() const override { return m_Specification.Width; }
		virtual unsigned int GetHeight() const override { return m_Specification.Height; }
		virtual unsigned int GetHandle() const override { return m_Handle; }

		virtual void SetParameter(Parameter parameter, int value) override;
		virtual void SetData(unsigned char* data) override;

		virtual void Bind(unsigned int slot = 0) const override;

	private:
		unsigned int m_Handle = 0;
		Specification m_Specification;
	};



	class OpenGLCubeMap : public CubeMap
	{
	public:
		OpenGLCubeMap(const std::vector<std::string>& paths);
		virtual ~OpenGLCubeMap();

		virtual unsigned int GetWidth() const override { return m_Resolution; }
		virtual unsigned int GetHeight() const override { return m_Resolution; }
		virtual unsigned int GetHandle() const override { return m_Handle; }

		virtual void SetParameter(Parameter parameter, int value) override;
		virtual void SetData(unsigned char* data) override;

		virtual void Bind(unsigned int slot = 0) const override;
	
	private:
		unsigned int m_Handle = 0;
		unsigned int m_Resolution = 0;
		std::vector<std::string> m_Paths;
	
	};
}

