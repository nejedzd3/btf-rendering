#include "crpch.h"
#include "OpenGLShader.h"

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace Crank
{
	OpenGLShader::OpenGLShader(const std::string& fileName)
	{
		if (!ParseShaderFile(fileName))
		{
			// TODO: Parsing failed case - set invalid shader
			CR_CORE_ERROR("OpenGLShader::OpenGLShader(): Shader \"{0}\" parsing failed.", fileName);
			return;
		}
		unsigned int vertexShader = CreateVertexShader(m_ShaderSources["vertex"].str());
		unsigned int fragmentShader = CreateFragmentShader(m_ShaderSources["fragment"].str());

		m_ShaderProgram = glCreateProgram();

		glAttachShader(m_ShaderProgram, vertexShader);
		glAttachShader(m_ShaderProgram, fragmentShader);

		glLinkProgram(m_ShaderProgram);

		int isLinked = 0;
		glGetProgramiv(m_ShaderProgram, GL_LINK_STATUS, &isLinked);

		if (isLinked == GL_FALSE)
		{
			GLint maxLength = 0;
			glGetProgramiv(m_ShaderProgram, GL_INFO_LOG_LENGTH, &maxLength);

			std::vector<GLchar> infoLog(maxLength);
			glGetProgramInfoLog(m_ShaderProgram, maxLength, &maxLength, &infoLog[0]);

			// TODO: Linking failed case - set as invalid shader
			CR_CORE_ERROR("OpenGLShader::Create(): Failed to link shader program - {0}", (const char*)&infoLog[0]);

			if (vertexShader)
				glDeleteShader(vertexShader);
			if (fragmentShader)
				glDeleteShader(fragmentShader);

			glDeleteProgram(m_ShaderProgram);
			m_ShaderProgram = 0;

			return;
		}

		CacheLocations();

		glDetachShader(m_ShaderProgram, vertexShader);
		glDetachShader(m_ShaderProgram, fragmentShader);

		glDeleteShader(vertexShader);
		glDeleteShader(fragmentShader);
	}

	OpenGLShader::~OpenGLShader()
	{
		if(m_ShaderProgram)
			glDeleteProgram(m_ShaderProgram);
	}

	void OpenGLShader::Bind() const
	{
		if (m_ShaderProgram == 0) // invalid shader program (either unitialized or failed to compile)
		{
			CR_CORE_WARN("OpenGLShader::Bind(): Binding unitialized or not compiled shader.");
			return;
		}
		glUseProgram(m_ShaderProgram);
	}

	void OpenGLShader::Unbind() const
	{
		glUseProgram(0);
	}

	int OpenGLShader::GetLocation(const std::string& name) const
	{
		auto foundUnif = m_Uniforms.find(name);
		if (foundUnif != m_Uniforms.end())
			return foundUnif->second.Location;

		auto foundAtr = m_Attributes.find(name);
		if (foundAtr != m_Attributes.end())
			return foundAtr->second.Location;
		
		auto foundTex = m_Textures.find(name);
		if (foundTex != m_Textures.end())
			return foundTex->second.Location;

		
		CR_CORE_ERROR("OpenGLShader::GetLocation(): Failed to pass uniform. Uniform location of \"{0}\" not found.", name);
		return -1;
	}

	void OpenGLShader::PassUniform(const std::string& name, const glm::mat4& value)
	{
		int loc = GetLocation(name);
		if (loc == -1)
			return;
		glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(value));
	}

	void OpenGLShader::PassUniform(const std::string& name, const glm::mat3& value)
	{
		int loc = GetLocation(name);
		if (loc == -1)
			return;
		glUniformMatrix3fv(loc, 1, GL_FALSE, glm::value_ptr(value));
	}

	void OpenGLShader::PassUniform(const std::string& name, const glm::uvec3& value)
	{
		int loc = GetLocation(name);
		if (loc == -1)
			return;
		glUniform3uiv(loc, 1, glm::value_ptr(value));
	}

	void OpenGLShader::PassUniform(const std::string& name, const glm::uvec2& value)
	{
		int loc = GetLocation(name);
		if (loc == -1)
			return;
		glUniform2uiv(loc, 1, glm::value_ptr(value));
	}

	void OpenGLShader::PassUniform(const std::string& name, const glm::vec4& value)
	{
		int loc = GetLocation(name);
		if (loc == -1)
			return;
		glUniform4fv(loc, 1, glm::value_ptr(value));
	}

	void OpenGLShader::PassUniform(const std::string& name, const glm::vec3& value)
	{
		int loc = GetLocation(name);
		if (loc == -1)
			return;
		glUniform3fv(loc, 1, glm::value_ptr(value));
	}

	void OpenGLShader::PassUniform(const std::string& name, const glm::vec2& value)
	{
		int loc = GetLocation(name);
		if (loc == -1)
			return;
		glUniform2fv(loc, 1, glm::value_ptr(value));
	}

	void OpenGLShader::PassUniform(const std::string& name, float value)
	{
		int loc = GetLocation(name);
		if (loc == -1)
			return;
		glUniform1f(loc, value);
	}

	void OpenGLShader::PassUniform(const std::string& name, int value)
	{
		int loc = GetLocation(name);
		if (loc == -1)
			return;
		glUniform1i(loc, value);
	}

	void OpenGLShader::PassUniform(const std::string& name, const UniformVariant& value)
	{
		std::visit([this, name](auto&& arg) {
			using T = std::decay_t<decltype(arg)>;
			if constexpr (std::is_same_v<T, int>) {
				PassUniform(name, arg);
			}
			else if constexpr (std::is_same_v<T, bool>) {
				PassUniform(name, arg);
			}
			else if constexpr (std::is_same_v<T, float>) {
				PassUniform(name, arg);
			}
			else if constexpr (std::is_same_v<T, glm::vec2>) {
				PassUniform(name, arg);
			}
			else if constexpr (std::is_same_v<T, glm::vec3>) {
				PassUniform(name, arg);
			}
			else if constexpr (std::is_same_v<T, glm::vec4>) {
				PassUniform(name, arg);
			}
			else if constexpr (std::is_same_v<T, glm::uvec2>) {
				PassUniform(name, arg);
			}
			else if constexpr (std::is_same_v<T, glm::uvec3>) {
				PassUniform(name, arg);
			}
			else if constexpr (std::is_same_v<T, glm::mat3>) {
				PassUniform(name, arg);
			}
			else if constexpr (std::is_same_v<T, glm::mat4>) {
				PassUniform(name, arg);
			}
			}, value);
	}

	unsigned int OpenGLShader::CreateVertexShader(const std::string& source)
	{
		unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
		const GLchar* VSSource = (GLchar*)source.c_str();

		glShaderSource(vertexShader, 1, &VSSource, nullptr);
		glCompileShader(vertexShader);

		GLint success;
		glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);

		if (success == GL_FALSE)
		{
			GLint maxLength = 0;
			glGetShaderiv(vertexShader, GL_INFO_LOG_LENGTH, &maxLength);

			std::vector<GLchar> errorLog(maxLength);
			glGetShaderInfoLog(vertexShader, maxLength, &maxLength, &errorLog[0]);

			CR_CORE_ERROR("OpenGLShader::CreateVertexShader(): Cannot compile vertex shader - {0}", (const char*)&errorLog[0]);
			glDeleteShader(vertexShader);
			return 0;
		}

		return vertexShader;
	}

	unsigned int OpenGLShader::CreateFragmentShader(const std::string& source)
	{
		unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

		const GLchar* FSSource = (GLchar*)source.c_str();

		glShaderSource(fragmentShader, 1, &FSSource, nullptr);
		glCompileShader(fragmentShader);

		GLint success;
		glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);

		if (success == GL_FALSE)
		{
			GLint maxLength = 0;
			glGetShaderiv(fragmentShader, GL_INFO_LOG_LENGTH, &maxLength);

			std::vector<GLchar> errorLog(maxLength);
			glGetShaderInfoLog(fragmentShader, maxLength, &maxLength, &errorLog[0]);

			CR_CORE_ERROR("OpenGLShader::CreateFragmentShader(): Cannot compile fragment shader - {0}", (const char*)&errorLog[0]);
			glDeleteShader(fragmentShader);
			return 0;
		}
		
		return fragmentShader;
	}

	void OpenGLShader::ResolveInclude(const std::string& path)
	{
		if (!m_Includes.emplace(path).second)
			return; // include already happened

		std::ifstream ifs(path, std::ios::in);
		CR_CORE_ASSERT(ifs.good() && ifs.is_open(), "OpenGLShader::ResolveInclude(): Include path \"" + path + "\" is invalid.");
		
		LoadingDestination ld = LoadingDestination::Invalid;

		std::string line;
		for (; std::getline(ifs, line) ;)
		{
			if (line.length() == 0)
				continue;

			if (Utility::StartsWith(line, "//"))
				continue;

			if (Utility::StartsWith(line, "#include"))
			{
				ResolveInclude(Utility::GetNthToken(line, 2));
				continue;
			}

			if (Utility::StartsWith(line, "#type"))
			{
				ld = GetLoadingDestination(Utility::GetNthToken(line, 2));
				continue;
			}

			GetShaderStream(ld) << line << "\n";
		}
	}

	std::stringstream& OpenGLShader::GetShaderStream(LoadingDestination ld)
	{
		switch (ld)
		{
		case LoadingDestination::VS:
			return m_ShaderSources["vertex"];

		case LoadingDestination::FS:
			return m_ShaderSources["fragment"];

		case LoadingDestination::Invalid:
			return m_ShaderSources["invalid"];
		}

		return m_ShaderSources["unknown"];
	}

	std::string OpenGLShader::GetShaderName(LoadingDestination ld)
	{
		switch (ld)
		{
		case LoadingDestination::VS:
			return "vertex";

		case LoadingDestination::FS:
			return "fragment";

		case LoadingDestination::Invalid:
			return "invalid";
		}

		return "unknown";
	}

	LoadingDestination OpenGLShader::GetLoadingDestination(const std::string& name)
	{
		if (name == "vertex")
			return LoadingDestination::VS;
		if (name == "fragment")
			return LoadingDestination::FS;

		if (name == "vertex_function")
			return LoadingDestination::VS;
		if (name == "fragment_function")
			return LoadingDestination::FS;

		if (name == "vertex_data_structure")
			return LoadingDestination::VS;
		if (name == "fragment_data_structure")
			return LoadingDestination::FS;

		if (name == "vertex_constant")
			return LoadingDestination::VS;
		if (name == "fragment_constant")
			return LoadingDestination::FS;
		
		return LoadingDestination::Invalid;
	}

	void OpenGLShader::CacheLocations()
	{
		// uniforms
		{
			int uniformCount = 0;
			glGetProgramiv(m_ShaderProgram, GL_ACTIVE_UNIFORMS, &uniformCount);

			if (!uniformCount)
				return;
		
			int	maxNameLength = 0, length = 0, count = 0;
			GLenum type = GL_NONE;
			glGetProgramiv(m_ShaderProgram, GL_ACTIVE_UNIFORM_MAX_LENGTH, &maxNameLength);

			auto name = std::make_unique<char[]>(maxNameLength);

			for (int i = 0; i < uniformCount; ++i)
			{
				glGetActiveUniform(m_ShaderProgram, i, maxNameLength, &length, &count, &type, name.get());

				if (type >= GL_SAMPLER_1D && type <= GL_SAMPLER_CUBE)
					m_Textures[std::string(name.get(), length)] = DataDescription((int)glGetUniformLocation(m_ShaderProgram, name.get()), OpenGLTypeToCrankDataType(type));
				else
					m_Uniforms[std::string(name.get(), length)] = DataDescription((int)glGetUniformLocation(m_ShaderProgram, name.get()), OpenGLTypeToCrankDataType(type));
			}
		}
		// attributes
		{
			int attributeCount = 0;
			glGetProgramiv(m_ShaderProgram, GL_ACTIVE_ATTRIBUTES, &attributeCount);

			if (!attributeCount)
				return;

			int	maxNameLength = 0, length = 0, count = 0;
			GLenum type = GL_NONE;
			glGetProgramiv(m_ShaderProgram, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &maxNameLength);

			auto name = std::make_unique<char[]>(maxNameLength);

			for (int i = 0; i < attributeCount; ++i)
			{
				glGetActiveAttrib(m_ShaderProgram, i, maxNameLength, &length, &count, &type, name.get());
				m_Attributes[std::string(name.get(), length)] = DataDescription((int)glGetAttribLocation(m_ShaderProgram, name.get()), OpenGLTypeToCrankDataType(type));
			}
		}

	}

	Shader::DataType OpenGLShader::OpenGLTypeToCrankDataType(GLenum type)
	{
		switch (type)
		{
		case GL_BOOL: return Shader::DataType::Bool;
		case GL_INT: return Shader::DataType::Int;
		case GL_FLOAT: return Shader::DataType::Float;
		case GL_FLOAT_VEC2: return Shader::DataType::Vec2;
		case GL_FLOAT_VEC3: return Shader::DataType::Vec3;
		case GL_FLOAT_VEC4: return Shader::DataType::Vec4;
		case GL_FLOAT_MAT2: return Shader::DataType::Mat2;
		case GL_FLOAT_MAT3: return Shader::DataType::Mat3;
		case GL_FLOAT_MAT4: return Shader::DataType::Mat4;
		case GL_SAMPLER_1D: return Shader::DataType::Sampler1D;
		case GL_SAMPLER_2D: return Shader::DataType::Sampler2D;
		case GL_SAMPLER_3D: return Shader::DataType::Sampler3D;
		case GL_SAMPLER_CUBE: return Shader::DataType::SamplerCube;
		default: return Shader::DataType::None;
		}
	}

	bool OpenGLShader::ParseShaderFile(const std::string& fileName)
	{
		std::ifstream ifs(fileName, std::ios::in);
		if (!(ifs.good() && ifs.is_open()))
		{
			CR_CORE_ERROR("OpenGLShader::ParseShaderFile(): Could not open file \"{0}\"", fileName);
			return false;
		}

		m_ShaderSources["vertex"];
		m_ShaderSources["fragment"];
		m_ShaderSources["invalid"];
		
		LoadingDestination ld = LoadingDestination::Invalid;

		m_ShaderSources["vertex"] << "// ------------ VERTEX SHADER ------------\n" << m_OpenGLVersion << "\n";
		m_ShaderSources["fragment"] << "// ----------- FRAGMENT SHADER -----------\n" << m_OpenGLVersion << "\n";

		for (std::string line; std::getline(ifs, line) ;)
		{
			if (line.length() == 0)
				continue;

			if (Utility::StartsWith(line, "//"))
				continue;

			if (Utility::StartsWith(line, "#include"))
			{
				ResolveInclude(Utility::GetNthToken(line, 2));
				continue;
			}

			if (Utility::StartsWith(line, "#type"))
			{
				ld = GetLoadingDestination(Utility::GetNthToken(line, 2));
				continue;
			}

			GetShaderStream(ld) << line << "\n";
		}

		// from https://stackoverflow.com/questions/8520560/get-a-file-name-from-a-path
		std::string base_filename = fileName.substr(fileName.find_last_of("/\\") + 1);
		std::string::size_type const p(base_filename.find_last_of('.'));
		std::string shaderName = base_filename.substr(0, p);

		std::ofstream ofs(std::string(".\\assets\\shaders\\") + shaderName + "_Source.txt", std::ios::out);
		ofs << m_ShaderSources["vertex"].str() << m_ShaderSources["fragment"].str();
		ofs.close();

		return true;
	}

}
