#pragma once
#include "Crank/Renderer/RenderingContext.h"

struct GLFWwindow;

namespace Crank
{
	class OpenGLContext : public RenderingContext
	{
	public:
		OpenGLContext(GLFWwindow* windowHandle);

		virtual void Init() override;
		virtual void SwapBuffers() override;

	private:
		GLFWwindow* m_WindowHandle;
	};
}
