#include "crpch.h"
#include "OpenGLTexture.h"

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <stb_image.h>

namespace Crank
{
	// -------------------------------------------------------------------------------------------------
	// -------------------------------------------Helper------------------------------------------------
	GLenum OpenGLTextureHelper::ParameterToOpenGLTextureParameter(Texture::Parameter p) 
	{
		switch (p)
		{
		case Texture::Parameter::MinificationFiltering: return GL_TEXTURE_MIN_FILTER;
		case Texture::Parameter::MagnificationFiltering: return GL_TEXTURE_MAG_FILTER;
		case Texture::Parameter::WrappingS: return GL_TEXTURE_WRAP_S;
		case Texture::Parameter::WrappingT: return GL_TEXTURE_WRAP_T;
		case Texture::Parameter::WrappingR: return GL_TEXTURE_WRAP_R;
		}
		return GL_FALSE;
	}

	GLenum OpenGLTextureHelper::GetOpenGLDataFormat(const Texture::Specification& spec)
	{
		switch (spec.Format)
		{
		case Texture::Format::RGB: return GL_RGB;
		case Texture::Format::RGBA: return GL_RGBA;
		}
		CR_CORE_ERROR("OpenGLTexture2D::GetOpenGLDataFormat(): Unknown format.");
		return GL_FALSE;
	}

	GLenum OpenGLTextureHelper::GetOpenGLInternalFormat(const Texture::Specification& spec)
	{
		if (spec.Format == Texture::Format::RGB)
		{
			switch (spec.BitsPerChannel)
			{
			case 4: return GL_RGB4;
			case 8: return GL_RGB8;
			case 10: return GL_RGB10;
			case 12: return GL_RGB12;
			}
		}
		else if (spec.Format == Texture::Format::RGBA)
		{
			switch (spec.BitsPerChannel)
			{
			case 4: return GL_RGBA4;
			case 8: return GL_RGBA8;
			case 12: return GL_RGBA12;
			case 16: return GL_RGBA16;
			}
		}
		else if (spec.Format == Texture::Format::DEPTH24_STENCIL8)
			return GL_DEPTH24_STENCIL8;

		CR_CORE_ERROR("OpenGLTexture2D::GetOpenGLInternalFormat(): Wrong format or number of bits per channel.");
		return GL_FALSE;
	}


	// -------------------------------------------------------------------------------------------------
	// -----------------------------------------Texture 2D----------------------------------------------
	OpenGLTexture2D::OpenGLTexture2D(const Specification& spec)
	{
		m_Specification = spec;

		glCreateTextures(GL_TEXTURE_2D, 1, &m_Handle);
		glTextureStorage2D(m_Handle, 1 + m_Specification.MipMapLevels, // "+1" for base level 
						   OpenGLTextureHelper::GetOpenGLInternalFormat(m_Specification), m_Specification.Width, m_Specification.Height);
	}

	OpenGLTexture2D::OpenGLTexture2D(const std::string& path, bool useMipMaps)
	{
		int width, height, channels;
		stbi_set_flip_vertically_on_load(1);

		unsigned char* data = stbi_load(path.c_str(), &width, &height, &channels, 0);
		if (!data)
		{
			CR_CORE_ERROR("OpenGLTexture2D::Constructor(): Failed to load image {0}.", path);
			return;
		}
		m_Specification.Width = width;
		m_Specification.Height = height;
		m_Specification.BitsPerChannel = 8;
		if (useMipMaps)
			m_Specification.SetMaxMipMapLevels();
		else
			m_Specification.MipMapLevels = 0;

		if (channels == 4)
		{
			m_Specification.Format = Format::RGBA;
		}
		else if (channels == 3)
		{
			m_Specification.Format = Format::RGB;
		}
		else
		{
			CR_CORE_ERROR("OpenGLTexture2D::Constructor(): Wrong number of channels ({0}) in image {1}.", channels, path);
			return;
		}

		glCreateTextures(GL_TEXTURE_2D, 1, &m_Handle);
		glTextureStorage2D(m_Handle, 1 + m_Specification.MipMapLevels, // "+1" for base level 
			OpenGLTextureHelper::GetOpenGLInternalFormat(m_Specification), m_Specification.Width, m_Specification.Height);

		glTextureSubImage2D(m_Handle, 0, 0, 0, m_Specification.Width, m_Specification.Height,
			OpenGLTextureHelper::GetOpenGLDataFormat(m_Specification), GL_UNSIGNED_BYTE, data);

		glGenerateTextureMipmap(m_Handle);

		glTextureParameteri(m_Handle, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTextureParameteri(m_Handle, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glTexParameteri(m_Handle, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(m_Handle, GL_TEXTURE_WRAP_T, GL_REPEAT);

		stbi_image_free(data);
	}

	OpenGLTexture2D::~OpenGLTexture2D()
	{
		glDeleteTextures(1, &m_Handle);
	}

	void OpenGLTexture2D::SetParameter(Parameter parameter, int value)
	{
		glTextureParameteri(m_Handle, OpenGLTextureHelper::ParameterToOpenGLTextureParameter(parameter), value);
	}

	void OpenGLTexture2D::SetData(unsigned char* data)
	{
		glTextureSubImage2D(m_Handle, 0, 0, 0, m_Specification.Width, m_Specification.Height,
			OpenGLTextureHelper::GetOpenGLDataFormat(m_Specification), GL_UNSIGNED_BYTE, data);
		
		if (m_Specification.MipMapLevels > 0)
			glGenerateTextureMipmap(m_Handle);
	}

	void OpenGLTexture2D::Bind(unsigned int slot) const
	{
		if (!m_Handle)
		{
			CR_CORE_ERROR("OpenGLTexture2D::Bind(): Trying to bind invalid texture.");
			return;
		}
		glBindTextureUnit(slot, m_Handle);
	}



	// -------------------------------------------------------------------------------------------------
	// ------------------------------------Texture 2D MultiSampled--------------------------------------
	OpenGLTexture2DMultiSampled::OpenGLTexture2DMultiSampled(const Specification& spec)
	{
		m_Specification = spec;
		int maxSamples;
		glGetIntegerv(GL_MAX_SAMPLES, &maxSamples);
		if (m_Specification.MSAASamples < 1 || m_Specification.MSAASamples > (unsigned int)maxSamples)
		{
			CR_CORE_ERROR("OpenGLTexture2DMultiSampled::Constructor(): Unacceptable number of samples.");
			return;
		}
		glCreateTextures(GL_TEXTURE_2D_MULTISAMPLE, 1, &m_Handle);

		glTextureStorage2DMultisample(
			m_Handle, m_Specification.MSAASamples, 
			OpenGLTextureHelper::GetOpenGLInternalFormat(m_Specification),
			m_Specification.Width, m_Specification.Height, GL_TRUE
		);

		if (glIsTexture(m_Handle) == GL_FALSE)
		{
			CR_CORE_ERROR("OpenGLTexture2DMultiSampled::Constructor(): Texture not constructed succesfully");
			return;
		}
	}

	OpenGLTexture2DMultiSampled::~OpenGLTexture2DMultiSampled()
	{
		glDeleteTextures(1, &m_Handle);
	}

	void OpenGLTexture2DMultiSampled::SetParameter(Parameter parameter, int value)
	{
		glTextureParameteri(m_Handle, OpenGLTextureHelper::ParameterToOpenGLTextureParameter(parameter), value);
	}

	void OpenGLTexture2DMultiSampled::SetData(unsigned char* data)
	{
		// FIXME: fix
		CR_CORE_ERROR("OpenGLTexture2DMultiSampled::SetData(): Cannot set data to multisampled texture.");
	}

	void OpenGLTexture2DMultiSampled::Bind(unsigned int slot) const
	{
		if (!m_Handle)
		{
			CR_CORE_ERROR("OpenGLTexture2DMultiSampled::Bind(): Trying to bind invalid texture.");
			return;
		}
		glBindTextureUnit(slot, m_Handle);
	}

	// -------------------------------------------------------------------------------------------------
	// -------------------------------------------Cube Map----------------------------------------------
	OpenGLCubeMap::OpenGLCubeMap(const std::vector<std::string>& paths)
	{
		glGenTextures(1, &m_Handle);
		glBindTexture(GL_TEXTURE_CUBE_MAP, m_Handle);

		int width, height, channels;

		for (unsigned int i = 0; i < paths.size(); ++i)
		{
			stbi_set_flip_vertically_on_load(0);
			unsigned char* data = stbi_load(paths[i].c_str(), &width, &height, &channels, 0);

			if (!data)
			{
				CR_CORE_ERROR("OpenGLCubeMap::Constructor(): Failed to load image {0}.", paths[i]);
				stbi_image_free(data);
				return;
			}

			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
			stbi_image_free(data);
		}

		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	}

	OpenGLCubeMap::~OpenGLCubeMap()
	{
		glDeleteTextures(1, &m_Handle);
	}

	void OpenGLCubeMap::SetParameter(Parameter parameter, int value)
	{
		glTextureParameteri(m_Handle, OpenGLTextureHelper::ParameterToOpenGLTextureParameter(parameter), value);
	}

	void OpenGLCubeMap::SetData(unsigned char* data)
	{
		CR_CORE_WARN("OpenGLCubeMap::SetData(): Cubemap does not yet support setting data.");
	}

	void OpenGLCubeMap::Bind(unsigned int slot) const
	{
		if (!m_Handle)
		{
			CR_CORE_ERROR("OpenGLCubeMap::Bind(): Trying to bind invalid texture.");
			return;
		}
		glBindTextureUnit(slot, m_Handle);
	}
}
