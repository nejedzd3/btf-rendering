#pragma once
#include "Crank/Renderer/VertexArray.h"
#include "Crank/Renderer/Shader.h"

namespace Crank
{
	class OpenGLVertexArray : public VertexArray
	{
	public:
		OpenGLVertexArray(const Ref<Shader>& shader);
		~OpenGLVertexArray();

		virtual void Bind() const;
		virtual void Unbind() const;

		virtual void AddVertexBuffer(const Ref<VertexBuffer>& vertexBuffer) override;
		virtual void AddElementBuffer(const Ref<ElementBuffer>& elementBuffer) override;

		virtual const std::vector<Ref<VertexBuffer>>& GetVertexBuffers() override { return m_VertexBuffers; }
		virtual const Ref<ElementBuffer>& GetElementBuffer() override { return m_ElementBuffer; }
		virtual unsigned int GetIndexCount() const override { return m_ElementBuffer->GetCount(); }
		virtual unsigned int GetVertexCount() const override;
		virtual const Ref<Shader>& GetShader() const override { return m_AssignedShader; }

	private:
		unsigned int m_Handle;

		Ref<Shader> m_AssignedShader;

		std::vector<Ref<VertexBuffer>> m_VertexBuffers;
		Ref<ElementBuffer> m_ElementBuffer;
	};
}
