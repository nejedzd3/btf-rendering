#pragma once
#include "Crank/Renderer/Framebuffer.h"
#include "Crank/Renderer/Texture.h"

namespace Crank
{
	class OpenGLFramebuffer : public Framebuffer
	{
	public:
		OpenGLFramebuffer(const Specification& spec);
		virtual ~OpenGLFramebuffer();

		virtual const Specification& GetSpecification() const override { return m_Specification; }
		virtual unsigned int GetHandle() const override { return m_Handle; }

		virtual void Overwrite() override;
		virtual void Resize(unsigned int width, unsigned int height) override;

		virtual void Bind() const override;
		virtual void Unbind() const override;

		virtual Ref<Texture2D> GetColorAttachment() const override;

	private:
		unsigned int m_Handle = 0;
		Ref<Texture2D> m_ColorAttachment = nullptr;
		Ref<Texture2D> m_DepthAttachment = nullptr;
		Ref<Texture2DMultiSampled> m_MSAAColorAttachment = nullptr;
		Ref<Texture2DMultiSampled> m_MSAADepthAttachment = nullptr;

		Ref<OpenGLFramebuffer> m_IntermediateFB = nullptr;

		Specification m_Specification;
	};
}

