#pragma once
#include "Crank/Renderer/Buffers.h"

namespace Crank
{
	// -------------------------------------------------------------------------------------------------
	// --------------------------------------------VBO--------------------------------------------------
	class OpenGLVertexBuffer : public VertexBuffer
	{
	public:
		OpenGLVertexBuffer(size_t byteSize);
		OpenGLVertexBuffer(size_t byteSize, float* data);
		virtual ~OpenGLVertexBuffer();

		virtual void Bind() const override;
		virtual void Unbind() const override;

		virtual void SetData(size_t byteSize, float* data, size_t offset) override;

		inline virtual void SetLayout(const BufferLayout& layout) override { m_Layout = layout; m_Layout.CalculateOffsetsAndStride(); }
		inline virtual const BufferLayout& GetLayout() const override { return m_Layout; }

	private:
		unsigned int m_Handle = 0;
		size_t m_VertexCount = 0;

		BufferLayout m_Layout;
	};


	// -------------------------------------------------------------------------------------------------
	// --------------------------------------------EBO--------------------------------------------------
	class OpenGLElementBuffer : public ElementBuffer
	{
	public:
		OpenGLElementBuffer(unsigned int count);
		OpenGLElementBuffer(unsigned int count, unsigned int* data);
		virtual ~OpenGLElementBuffer();

		virtual void Bind() const override;
		virtual void Unbind() const override;

		virtual void SetData(unsigned int count, unsigned int* data, size_t offset) override;

		inline virtual unsigned int GetCount() const override { return m_Count; }

	private:
		unsigned int m_Handle = 0;

		unsigned int m_Count = 0;
	};
}
