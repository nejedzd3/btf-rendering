#include "crpch.h"
#include "OpenGLRendererAPI.h"
#include "Crank/Core/Application.h"

#include <glad/glad.h>

namespace Crank
{
	void OpenGLRendererAPI::Init()
	{
		glEnable(GL_BLEND);
		glEnable(GL_DEPTH_TEST);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		if (Application::Get().GetWindow().IsMSAA())
			glEnable(GL_MULTISAMPLE);
	}

	void OpenGLRendererAPI::SetClearColor(const glm::vec4& color)
	{
		glClearColor(color.r, color.g, color.b, color.a);
	}

	void OpenGLRendererAPI::Clear()
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}

	void OpenGLRendererAPI::SetDepthFunction(DepthFunction depthFunction)
	{
		glDepthFunc(CrankDepthFuncToOpenGL(depthFunction));
	}

	void OpenGLRendererAPI::DrawElements(const Ref<VertexArray>& vertexArray)
	{
		vertexArray->Bind();
		glDrawElements(GL_TRIANGLES, vertexArray->GetIndexCount(), GL_UNSIGNED_INT, nullptr);
	}

	void OpenGLRendererAPI::DrawArrays(const Ref<VertexArray>& vertexArray)
	{
		vertexArray->Bind();
		glDrawArrays(GL_TRIANGLES, 0, vertexArray->GetVertexCount());
	}

	unsigned int OpenGLRendererAPI::CrankDepthFuncToOpenGL(DepthFunction dFunc)
	{
		switch (dFunc)
		{
		case DepthFunction::Less: return GL_LESS;
		case DepthFunction::LEqual: return GL_LEQUAL;
		case DepthFunction::None:
		default:
			CR_CORE_ERROR("OpenGLRendererAPI::CrankDepthFuncToOpenGL(): Wrong depth function");
			return GL_NONE;
		}
	}
}
