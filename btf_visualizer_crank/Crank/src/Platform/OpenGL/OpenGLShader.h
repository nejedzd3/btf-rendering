#pragma once

#include "Crank/Renderer/Shader.h"
#include <glad/glad.h>

enum class LoadingDestination { Invalid, VS, FS };

namespace Crank
{
	class OpenGLShader : public Shader
	{
	public:
		OpenGLShader() = default;
		OpenGLShader(const std::string& fileName);
		~OpenGLShader();

		virtual void Bind() const override;
		virtual void Unbind() const override;

		virtual int GetLocation(const std::string& input) const override;
		
		virtual void PassUniform(const std::string& name, const glm::mat4& value) override;
		virtual void PassUniform(const std::string& name, const glm::mat3& value) override;
		virtual void PassUniform(const std::string& name, const glm::uvec3& value) override;
		virtual void PassUniform(const std::string& name, const glm::uvec2& value) override;
		virtual void PassUniform(const std::string& name, const glm::vec4& value) override;
		virtual void PassUniform(const std::string& name, const glm::vec3& value) override;
		virtual void PassUniform(const std::string& name, const glm::vec2& value) override;
		virtual void PassUniform(const std::string& name, float value) override;
		virtual void PassUniform(const std::string& name, int value) override;

		virtual void PassUniform(const std::string& name, const UniformVariant& value) override;

		virtual const std::unordered_map<std::string, Shader::DataDescription>& GetAttributes() const override { return m_Attributes; }
		virtual const std::unordered_map<std::string, Shader::DataDescription>& GetUniforms() const override { return m_Uniforms; }
		virtual const std::unordered_map<std::string, Shader::DataDescription>& GetTextures() const override { return m_Textures; }

	private:
		unsigned int CreateVertexShader(const std::string& source);
		unsigned int CreateFragmentShader(const std::string& source);
		
		bool ParseShaderFile(const std::string& fileName);
		void ResolveInclude(const std::string& path);

		std::stringstream& GetShaderStream(LoadingDestination ld);
		std::string GetShaderName(LoadingDestination ld);
		LoadingDestination GetLoadingDestination(const std::string& name);

		void CacheLocations();

		Shader::DataType OpenGLTypeToCrankDataType(GLenum type);
	
	private:
		unsigned int m_ShaderProgram = 0;

		std::unordered_map<std::string, Shader::DataDescription> m_Attributes;
		std::unordered_map<std::string, Shader::DataDescription> m_Uniforms;
		std::unordered_map<std::string, Shader::DataDescription> m_Textures;

		std::unordered_map<std::string, std::stringstream> m_ShaderSources;
		std::set<std::string> m_Includes;

		const std::string m_OpenGLVersion = "#version 460 core";
	};

}

