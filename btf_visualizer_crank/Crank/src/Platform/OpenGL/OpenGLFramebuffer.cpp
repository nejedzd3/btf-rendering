#include "crpch.h"
#include "OpenGLFramebuffer.h"

#include <glad/glad.h>

namespace Crank
{
	OpenGLFramebuffer::OpenGLFramebuffer(const Specification& spec)
		: m_Specification(spec)
	{
		Overwrite();
	}

	OpenGLFramebuffer::~OpenGLFramebuffer()
	{
		glDeleteFramebuffers(1, &m_Handle);
		m_ColorAttachment = nullptr;
		m_MSAAColorAttachment = nullptr;
		m_DepthAttachment = nullptr;
		m_MSAADepthAttachment = nullptr;
		m_IntermediateFB = nullptr;
	}

	void OpenGLFramebuffer::Resize(unsigned int width, unsigned int height)
	{
		m_Specification.Width = width;
		m_Specification.Height = height;
		Overwrite();
	}	
	
	void OpenGLFramebuffer::Overwrite()
	{
		glGetError();
		if (m_Handle)
		{
			glDeleteFramebuffers(1, &m_Handle);
			m_ColorAttachment = nullptr;
			m_MSAAColorAttachment = nullptr;
			m_DepthAttachment = nullptr;
			m_MSAADepthAttachment = nullptr;
			m_IntermediateFB = nullptr;
		}

		glCreateFramebuffers(1, &m_Handle);
		glBindFramebuffer(GL_FRAMEBUFFER, m_Handle);

		if (m_Specification.UsesMSAA())
		{
			// color 
			m_MSAAColorAttachment = Texture2DMultiSampled::Create({ Texture::Format::RGBA, 8, m_Specification.Width, m_Specification.Height, 0, m_Specification.MSAASamples });
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D_MULTISAMPLE, m_MSAAColorAttachment->GetHandle(), 0);

			// depth
			m_MSAADepthAttachment = Texture2DMultiSampled::Create({ Texture::Format::DEPTH24_STENCIL8, 8, m_Specification.Width, m_Specification.Height, 0, m_Specification.MSAASamples });
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D_MULTISAMPLE, m_MSAADepthAttachment->GetHandle(), 0);
			
			// prep intermediate fbo
			Specification intSpec = m_Specification;
			intSpec.MSAASamples = 0;
			m_IntermediateFB = CreateRef<OpenGLFramebuffer>(intSpec);

		} 
		else
		{
			// color
			m_ColorAttachment = Texture2D::Create({ Texture::Format::RGBA, 8, m_Specification.Width, m_Specification.Height, 0, 0 });
			m_ColorAttachment->SetParameter(Texture::Parameter::MinificationFiltering, GL_LINEAR);
			m_ColorAttachment->SetParameter(Texture::Parameter::MagnificationFiltering, GL_LINEAR);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_ColorAttachment->GetHandle(), 0);

			// depth
			m_DepthAttachment = Texture2D::Create({ Texture::Format::DEPTH24_STENCIL8, 8, m_Specification.Width, m_Specification.Height, 0, 0 });
			m_DepthAttachment->SetParameter(Texture::Parameter::MinificationFiltering, GL_LINEAR);
			m_DepthAttachment->SetParameter(Texture::Parameter::MagnificationFiltering, GL_LINEAR);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, m_DepthAttachment->GetHandle(), 0);
		}


		auto err = glGetError();
		if (err != GL_NO_ERROR)
		{
			CR_CORE_ERROR("ERROR OVERWRITE");
		}

		auto status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		if (status != GL_FRAMEBUFFER_COMPLETE)
			CR_CORE_ERROR("OpenGLFramebuffer::Overwrite(): Failed to create new Framebuffer.");

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	void OpenGLFramebuffer::Bind() const
	{
		glBindFramebuffer(GL_FRAMEBUFFER, m_Handle);

		// Binding implies that framebuffer is being used as a viewport
		glViewport(0, 0, m_Specification.Width, m_Specification.Height);
	}
	
	void OpenGLFramebuffer::Unbind() const
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	Ref<Texture2D> OpenGLFramebuffer::GetColorAttachment() const
	{
		if (m_Specification.UsesMSAA())
		{
			glGetError();
			unsigned int width = m_Specification.Width, height = m_Specification.Height;

			glBindFramebuffer(GL_READ_FRAMEBUFFER, m_Handle);
			glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_IntermediateFB->GetHandle());

			// resolves multisampled color attachment
			glBlitFramebuffer(0, 0, width, height, 0, 0, width, height, GL_COLOR_BUFFER_BIT, GL_NEAREST);

			glBindFramebuffer(GL_FRAMEBUFFER, 0);

			auto err = glGetError();
			if (err != GL_NO_ERROR)
			{
				CR_CORE_ERROR("ERROR GETTER");
			}
			return m_IntermediateFB->GetColorAttachment();
		}
		else
		{
			return m_ColorAttachment;
		}
	}
}
