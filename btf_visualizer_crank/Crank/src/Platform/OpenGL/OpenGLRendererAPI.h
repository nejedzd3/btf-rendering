#pragma once
#include "Crank/Renderer/RendererAPI.h"

namespace Crank
{
	class OpenGLRendererAPI : public RendererAPI
	{
	public:
		virtual void Init() override;

		virtual void SetClearColor(const glm::vec4& color) override;
		virtual void Clear() override;
		virtual void SetDepthFunction(DepthFunction depthFunction) override;

		virtual void DrawElements(const Ref<VertexArray>& vertexArray) override;
		virtual void DrawArrays(const Ref<VertexArray>& vertexArray) override;
	
	private:
		unsigned int CrankDepthFuncToOpenGL(DepthFunction dFunc);
	};
}
