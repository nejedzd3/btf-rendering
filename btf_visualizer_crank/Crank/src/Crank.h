#pragma once

/**
 * For including the Crank engine functionality.
 */

#include "Crank/Core/Application.h"
#include "Crank/Core/Layer.h"
#include "Crank/Core/Log.h"
#include "Crank/Core/Time.h"
#include "Crank/Core/Core.h"

#include "Crank/Input/Input.h"
#include "Crank/Input/InputCodesConvertor.h"
#include "Crank/Input/KeyCodes.h"
#include "Crank/Input/MouseCodes.h"

#include "Crank/Renderer/Buffers.h"
#include "Crank/Renderer/VertexArray.h"
#include "Crank/Renderer/Renderer.h"
#include "Crank/Renderer/Shader.h"
#include "Crank/Renderer/Texture.h"
#include "Crank/Renderer/MeshLoader.h"
#include "Crank/Renderer/Mesh.h"
#include "Crank/Renderer/Camera.h"
#include "Crank/Renderer/CameraController.h"
#include "Crank/Renderer/Light.h"
#include "Crank/Renderer/Framebuffer.h"
#include "Crank/Renderer/SkyBox.h"

#include "Crank/ImGui/ImGuiLayer.h"
#include "Crank/ImGui/ImGuiRenderer.h"

// Utility
#include "Crank/Utility/MathHelpers.h"

// BTF Extension
#include "Crank/BTF/TextureAtlas.h"
