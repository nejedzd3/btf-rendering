#pragma once
#include "KeyCodes.h"
#include "MouseCodes.h"


enum ImGuiKey;

namespace Crank
{
	class InputCodeConvertor
	{
	public:
		InputCodeConvertor() = default;
		~InputCodeConvertor() = default;

		static ImGuiKey CrankKeyToImGuiKey(KeyCode CrankKey);
		static int CrankKeyToGLFWKey(KeyCode CrankKey);
		static KeyCode GLFWKeyToCrankKey(int GLFWKey);

	private:
	};
}
