#include "crpch.h"
#include "InputCodesConvertor.h"

#include <imgui.h>

namespace Crank
{
	ImGuiKey InputCodeConvertor::CrankKeyToImGuiKey(KeyCode CrankKey)
	{
            switch (CrankKey)
            {
            case KeyInput::TAB: return ImGuiKey_Tab;
            case KeyInput::LEFT: return ImGuiKey_LeftArrow;
            case KeyInput::RIGHT: return ImGuiKey_RightArrow;
            case KeyInput::UP: return ImGuiKey_UpArrow;
            case KeyInput::DOWN: return ImGuiKey_DownArrow;
            case KeyInput::PAGE_UP: return ImGuiKey_PageUp;
            case KeyInput::PAGE_DOWN: return ImGuiKey_PageDown;
            case KeyInput::HOME: return ImGuiKey_Home;
            case KeyInput::END: return ImGuiKey_End;
            case KeyInput::INSERT: return ImGuiKey_Insert;
            case KeyInput::DEL: return ImGuiKey_Delete;
            case KeyInput::BACKSPACE: return ImGuiKey_Backspace;
            case KeyInput::SPACE: return ImGuiKey_Space;
            case KeyInput::ENTER: return ImGuiKey_Enter;
            case KeyInput::ESCAPE: return ImGuiKey_Escape;
            case KeyInput::APOSTROPHE: return ImGuiKey_Apostrophe;
            case KeyInput::COMMA: return ImGuiKey_Comma;
            case KeyInput::MINUS: return ImGuiKey_Minus;
            case KeyInput::PERIOD: return ImGuiKey_Period;
            case KeyInput::SLASH: return ImGuiKey_Slash;
            case KeyInput::SEMICOLON: return ImGuiKey_Semicolon;
            case KeyInput::EQUAL: return ImGuiKey_Equal;
            case KeyInput::LEFT_BRACKET: return ImGuiKey_LeftBracket;
            case KeyInput::BACKSLASH: return ImGuiKey_Backslash;
            case KeyInput::RIGHT_BRACKET: return ImGuiKey_RightBracket;
            case KeyInput::GRAVE_ACCENT: return ImGuiKey_GraveAccent;
            case KeyInput::CAPS_LOCK: return ImGuiKey_CapsLock;
            case KeyInput::SCROLL_LOCK: return ImGuiKey_ScrollLock;
            case KeyInput::NUM_LOCK: return ImGuiKey_NumLock;
            case KeyInput::PRINT_SCREEN: return ImGuiKey_PrintScreen;
            case KeyInput::PAUSE: return ImGuiKey_Pause;
            case KeyInput::NUMPAD_0: return ImGuiKey_Keypad0;
            case KeyInput::NUMPAD_1: return ImGuiKey_Keypad1;
            case KeyInput::NUMPAD_2: return ImGuiKey_Keypad2;
            case KeyInput::NUMPAD_3: return ImGuiKey_Keypad3;
            case KeyInput::NUMPAD_4: return ImGuiKey_Keypad4;
            case KeyInput::NUMPAD_5: return ImGuiKey_Keypad5;
            case KeyInput::NUMPAD_6: return ImGuiKey_Keypad6;
            case KeyInput::NUMPAD_7: return ImGuiKey_Keypad7;
            case KeyInput::NUMPAD_8: return ImGuiKey_Keypad8;
            case KeyInput::NUMPAD_9: return ImGuiKey_Keypad9;
            case KeyInput::NUMPAD_DECIMAL: return ImGuiKey_KeypadDecimal;
            case KeyInput::NUMPAD_DIVIDE: return ImGuiKey_KeypadDivide;
            case KeyInput::NUMPAD_MULTIPLY: return ImGuiKey_KeypadMultiply;
            case KeyInput::NUMPAD_SUBTRACT: return ImGuiKey_KeypadSubtract;
            case KeyInput::NUMPAD_ADD: return ImGuiKey_KeypadAdd;
            case KeyInput::NUMPAD_ENTER: return ImGuiKey_KeypadEnter;
            case KeyInput::NUMPAD_EQUAL: return ImGuiKey_KeypadEqual;
            case KeyInput::LEFT_SHIFT: return ImGuiKey_LeftShift;
            case KeyInput::LEFT_CONTROL: return ImGuiKey_LeftCtrl;
            case KeyInput::LEFT_ALT: return ImGuiKey_LeftAlt;
            case KeyInput::LEFT_SUPER: return ImGuiKey_LeftSuper;
            case KeyInput::RIGHT_SHIFT: return ImGuiKey_RightShift;
            case KeyInput::RIGHT_CONTROL: return ImGuiKey_RightCtrl;
            case KeyInput::RIGHT_ALT: return ImGuiKey_RightAlt;
            case KeyInput::RIGHT_SUPER: return ImGuiKey_RightSuper;
            case KeyInput::MENU: return ImGuiKey_Menu;
            case KeyInput::NUM_0: return ImGuiKey_0;
            case KeyInput::NUM_1: return ImGuiKey_1;
            case KeyInput::NUM_2: return ImGuiKey_2;
            case KeyInput::NUM_3: return ImGuiKey_3;
            case KeyInput::NUM_4: return ImGuiKey_4;
            case KeyInput::NUM_5: return ImGuiKey_5;
            case KeyInput::NUM_6: return ImGuiKey_6;
            case KeyInput::NUM_7: return ImGuiKey_7;
            case KeyInput::NUM_8: return ImGuiKey_8;
            case KeyInput::NUM_9: return ImGuiKey_9;
            case KeyInput::A: return ImGuiKey_A;
            case KeyInput::B: return ImGuiKey_B;
            case KeyInput::C: return ImGuiKey_C;
            case KeyInput::D: return ImGuiKey_D;
            case KeyInput::E: return ImGuiKey_E;
            case KeyInput::F: return ImGuiKey_F;
            case KeyInput::G: return ImGuiKey_G;
            case KeyInput::H: return ImGuiKey_H;
            case KeyInput::I: return ImGuiKey_I;
            case KeyInput::J: return ImGuiKey_J;
            case KeyInput::K: return ImGuiKey_K;
            case KeyInput::L: return ImGuiKey_L;
            case KeyInput::M: return ImGuiKey_M;
            case KeyInput::N: return ImGuiKey_N;
            case KeyInput::O: return ImGuiKey_O;
            case KeyInput::P: return ImGuiKey_P;
            case KeyInput::Q: return ImGuiKey_Q;
            case KeyInput::R: return ImGuiKey_R;
            case KeyInput::S: return ImGuiKey_S;
            case KeyInput::T: return ImGuiKey_T;
            case KeyInput::U: return ImGuiKey_U;
            case KeyInput::V: return ImGuiKey_V;
            case KeyInput::W: return ImGuiKey_W;
            case KeyInput::X: return ImGuiKey_X;
            case KeyInput::Y: return ImGuiKey_Y;
            case KeyInput::Z: return ImGuiKey_Z;
            case KeyInput::F1: return ImGuiKey_F1;
            case KeyInput::F2: return ImGuiKey_F2;
            case KeyInput::F3: return ImGuiKey_F3;
            case KeyInput::F4: return ImGuiKey_F4;
            case KeyInput::F5: return ImGuiKey_F5;
            case KeyInput::F6: return ImGuiKey_F6;
            case KeyInput::F7: return ImGuiKey_F7;
            case KeyInput::F8: return ImGuiKey_F8;
            case KeyInput::F9: return ImGuiKey_F9;
            case KeyInput::F10: return ImGuiKey_F10;
            case KeyInput::F11: return ImGuiKey_F11;
            case KeyInput::F12: return ImGuiKey_F12;
            default: return ImGuiKey_None;
            }
	}

	int InputCodeConvertor::CrankKeyToGLFWKey(KeyCode CrankKey)
	{
        return (int)CrankKey;
	}

	KeyCode InputCodeConvertor::GLFWKeyToCrankKey(int GLFWKey)
	{
        return (KeyCode)GLFWKey;
	}

}

