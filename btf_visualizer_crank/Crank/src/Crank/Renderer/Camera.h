#pragma once

#include <glm/mat4x4.hpp>

#include "Crank/Renderer/Transform.h"

namespace Crank
{
	// -------------------------------------------------------------------------------------------------
	// --------------------------------------Base Camera Class------------------------------------------
	class Camera
	{
	public:
		Camera()
			: m_ProjectionMatrix(glm::mat4(1.0f)) {}

		Camera(const glm::mat4& projectionMatrix)
			: m_ProjectionMatrix(projectionMatrix) {}

		virtual ~Camera() {}

		inline const glm::mat4& GetProjectionMatrix() const { return m_ProjectionMatrix; }

	protected:
		glm::mat4 m_ProjectionMatrix;
	};

	// -------------------------------------------------------------------------------------------------
	// -------------------------------------Perspective Camera------------------------------------------
	class PerspectiveCamera : public Camera
	{
	public:
		PerspectiveCamera() {}
		PerspectiveCamera(float fov, float aspectRatio, float nearPlane, float farPlane);
		~PerspectiveCamera() {}

		void SetProjection(float fov, float aspectRatio, float nearPlane, float farPlane);
		void UpdateProjection();

		inline float GetFOV() const { return m_FOV; }
		inline float GetAspectRatio() const { return m_AspectRatio; }
		inline float GetNearPlane() const { return m_NearPlane; }
		inline float GetFarPlane() const { return m_FarPlane; }
		inline float GetZoom() const { return m_Zoom; }


		inline void SetFOV(float FOV) { m_FOV = FOV; UpdateProjection(); }
		inline void SetAspectRatio(float aspectRatio) { m_AspectRatio = aspectRatio; UpdateProjection(); }
		inline void SetNearPlane(float nearPlane) { m_NearPlane = nearPlane; UpdateProjection(); }
		inline void SetFarPlane(float farPlane) { m_FarPlane = farPlane; UpdateProjection(); }
		inline void SetZoom(float zoom) { m_Zoom = zoom; UpdateProjection(); }

		inline const glm::mat4& GetViewMatrix() { return m_Transform.GetInverseModelMatrix(); }
		inline Transform& GetTransform() { return m_Transform; }

	private:
		Transform m_Transform;

		float m_FOV = 45.0f,
			m_AspectRatio = 16.0f / 9.0f,
			m_NearPlane = 0.1f,
			m_FarPlane = 1000.0f,
			m_Zoom = 1.0f;

	private:
		friend class FreeCameraController;
	};

	// -------------------------------------------------------------------------------------------------
	// -------------------------------------Orthographic Camera-----------------------------------------
	class OrthographicCamera : public Camera
	{
	public:
		OrthographicCamera(float left, float right, float top, float bottom);
		~OrthographicCamera() {}

		void SetProjection(float left, float right, float top, float bottom);
		inline void SetRotation(float degrees) { m_Rotation = degrees; CalculateViewMatrix(); }
		inline void SetPosition(const glm::vec3 position) { m_Position = position; CalculateViewMatrix(); }

		inline const glm::mat4& GetViewMatrix() const { return m_ViewMatrix; }
		inline const glm::mat4& GetProjectionViewMatrix() const { return m_ProjectionViewMatrix; }
		inline const glm::vec3& GetOrigin() const { return m_Position; }
		inline float GetRotation() const { return m_Rotation; }

	private:
		void CalculateViewMatrix();

	private:
		glm::mat4 m_ViewMatrix;
		glm::mat4 m_ProjectionViewMatrix;

		float m_Rotation = 0.0f;
		glm::vec3 m_Position = glm::vec3(0.0f);
	};
}
