#include "crpch.h"
#include "Buffers.h"

#include "Crank/Renderer/RendererAPI.h"
#include "Platform/OpenGL/OpenGLBuffers.h"

namespace Crank
{
	// -------------------------------------------------------------------------------------------------
	// --------------------------------------------VBO--------------------------------------------------
	Ref<VertexBuffer> VertexBuffer::Create(size_t byteSize)
	{
		switch (RendererAPI::GetAPI())
		{
		case RendererAPI::API::None:
			CR_CORE_WARN("VertexBuffer::Create(): No Rendering API is selected.");
			return nullptr;

		case RendererAPI::API::OpenGL:
			return CreateRef<OpenGLVertexBuffer>(byteSize);
		}

		CR_CORE_ERROR("VertexBuffer::Create(): Unknown Rendering API.");
		return nullptr;
	}

	Ref<VertexBuffer> VertexBuffer::Create(size_t byteSize, float* data)
	{
		switch (RendererAPI::GetAPI())
		{
		case RendererAPI::API::None:
			CR_CORE_WARN("VertexBuffer::Create(): No Rendering API is selected.");
			return nullptr;

		case RendererAPI::API::OpenGL:
			return CreateRef<OpenGLVertexBuffer>(byteSize, data);
		}

		CR_CORE_ERROR("VertexBuffer::Create(): Unknown Rendering API.");
		return nullptr;
	}

	// -------------------------------------------------------------------------------------------------
	// --------------------------------------------EBO--------------------------------------------------
	Ref<ElementBuffer> ElementBuffer::Create(unsigned int count)
	{
		switch (RendererAPI::GetAPI())
		{
		case RendererAPI::API::None:
			CR_CORE_WARN("VertexBuffer::Create(): No Rendering API is selected.");
			return nullptr;

		case RendererAPI::API::OpenGL:
			return CreateRef<OpenGLElementBuffer>(count);
		}

		CR_CORE_ERROR("VertexBuffer::Create(): Unknown Rendering API.");
		return nullptr;
	}

	Ref<ElementBuffer> ElementBuffer::Create(unsigned int count, unsigned int* data)
	{
		switch (RendererAPI::GetAPI())
		{
		case RendererAPI::API::None:
			CR_CORE_WARN("VertexBuffer::Create(): No Rendering API is selected.");
			return nullptr;

		case RendererAPI::API::OpenGL:
			return CreateRef<OpenGLElementBuffer>(count, data);
		}

		CR_CORE_ERROR("VertexBuffer::Create(): Unknown Rendering API.");
		return nullptr;
	}

	void BufferLayout::CalculateOffsetsAndStride()
	{
		size_t offset = 0;
		m_Stride = 0;

		for (auto& el : m_Elements)
		{
			el.Offset = offset;

			if (m_Type == Type::TightlyPacked)
			{
				offset += m_VertexCount * BufferHelper::ByteSizeOfDataType(el.Type);
			}

			else if (m_Type == Type::Interleaved)
			{
				offset += BufferHelper::ByteSizeOfDataType(el.Type);
				m_Stride += (unsigned int) BufferHelper::ByteSizeOfDataType(el.Type);
			}
			
			else
			{
				CR_CORE_WARN("BufferLayout::CalculateOffsetsAndStride(): Buffer layout has no specified type.");
			}
		}
	}
}
