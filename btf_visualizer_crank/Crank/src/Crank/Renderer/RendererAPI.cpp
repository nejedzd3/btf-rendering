#pragma once

#include "crpch.h"
#include "RendererAPI.h"

namespace Crank
{
#ifdef CR_RENDERER_API_OPENGL
	RendererAPI::API RendererAPI::s_API = RendererAPI::API::OpenGL;
#else
	RendererAPI::API RendererAPI::s_API = RendererAPI::API::None;
#endif
}
