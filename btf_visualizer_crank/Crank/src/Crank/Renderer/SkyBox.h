#pragma once

#include "Crank/Renderer/Texture.h"
#include "Crank/Renderer/VertexArray.h"

namespace Crank
{
	class SkyBox
	{
	public:
		SkyBox() {}
		SkyBox(const Ref<Shader>& shader, const std::vector<std::string>& texturePaths);
		~SkyBox();

		void LoadTexture(const std::vector<std::string>& texturePaths);
		inline const Ref<VertexArray>& GetVertexArray() const { return m_Cube; }
		inline const Ref<CubeMap>& GetTexture() const { return m_CubeMapTexture; }

	private:
		Ref<CubeMap> m_CubeMapTexture = nullptr;
		Ref<VertexArray> m_Cube = nullptr;
	};


}
