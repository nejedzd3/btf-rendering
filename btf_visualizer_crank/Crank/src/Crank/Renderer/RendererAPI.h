#pragma once

#include <glm/glm.hpp>

#include "VertexArray.h"

namespace Crank
{
	class RendererAPI
	{
	public:
		enum class API
		{
			None = 0, OpenGL
		};
		enum class DepthFunction
		{
			None = 0, LEqual, Less
		};

	public:
		virtual void Init() = 0;

		virtual void SetClearColor(const glm::vec4& color) = 0;
		virtual void Clear() = 0;
		virtual void SetDepthFunction(DepthFunction depthFunction) = 0;

		virtual void DrawElements(const Ref<VertexArray>& vertexArray) = 0;
		virtual void DrawArrays(const Ref<VertexArray>& vertexArray) = 0;

		inline static API GetAPI() { return s_API; }

	private:
		static API s_API;
	};
}
