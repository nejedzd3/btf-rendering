#pragma once

#include "Crank/Renderer/Shader.h"
#include "Crank/ImGui/ImGuiRenderer.h"

namespace Crank
{
	class Light
	{
	public:
		Light(const Ref<Shader>& shader);
		~Light() {}

		Light& SetData(const std::string& name, const Shader::UniformVariant& value, const ImGuiDataDetail& imguiDetail = ImGuiDataDetail());
		
		const Shader::UniformVariant& GetData(const std::string& name);
		std::vector<std::string> GetDataNames() const;
		inline const Ref<Shader>& GetShader() const { return m_Shader; }

		void PassData();

		void OnImGuiRender();

	private:
		Ref<Shader> m_Shader = nullptr;
		std::string m_InShaderName = "";

		std::unordered_map<std::string, Shader::UniformVariant> m_Data;
		std::unordered_map<std::string, ImGuiDataDetail> m_ImGuiRenderDetails;
	
	private:
		bool IsLightUniform(const std::string& name);
		void GetInShaderName(const std::string& uniformName);
		std::string MakeDataNameFull(const std::string& name);

	};
}

