#include "crpch.h"
#include "Shader.h"

#include "Crank/Renderer/RendererAPI.h"
#include "Platform/OpenGL/OpenGLShader.h"

namespace Crank
{
	const Shader::UniformVariant Shader::EmptyVariant = std::monostate();

	Ref<Shader> Shader::Create(const std::string& fileName)
	{
		switch (RendererAPI::GetAPI())
		{
		case RendererAPI::API::None:
			CR_CORE_WARN("Shader::Create(): No Rendering API is selected.");
			return nullptr;

		case RendererAPI::API::OpenGL:
			return CreateRef<OpenGLShader>(fileName);
		}

		CR_CORE_ERROR("Shader::Create(): Unknown Rendering API.");
		return nullptr;
	}
}
