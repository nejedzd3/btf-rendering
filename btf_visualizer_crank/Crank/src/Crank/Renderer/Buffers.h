#pragma once

#include "Crank/Core/Log.h"

namespace Crank
{

	enum class BufferDataType
	{
		None = 0, Float1, Float2, Float3, Float4
	};

	// -------------------------------------------------------------------------------------------------
	// -------------------------------------------Helper------------------------------------------------
	class BufferHelper
	{
	public:
		inline static size_t ByteSizeOfDataType(BufferDataType type)
		{
			int typeCount = CountOfDataType(type);

			switch (type)
			{
			case BufferDataType::Float1:
			case BufferDataType::Float2:
			case BufferDataType::Float3:
			case BufferDataType::Float4:
				return typeCount * sizeof(float);
			
			case BufferDataType::None:
				return 0;

			default:
				CR_CORE_ERROR("BufferHelper::ByteSizeOfDataType(): Unknown BufferDataType.");
				return 0;
			}
		}

		inline static int CountOfDataType(BufferDataType type)
		{
			switch (type)
			{
			case BufferDataType::Float1:
				return 1;

			case BufferDataType::Float2:
				return 2;

			case BufferDataType::Float3:
				return 3;

			case BufferDataType::Float4:
				return 4;

			case BufferDataType::None:
				return 0;

			default:
				CR_CORE_ERROR("BufferHelper::ByteSizeOfDataType(): Unknown BufferDataType.");
				return 0;
			}
		}

#ifdef CR_RENDERER_API_OPENGL
		// Defined in OpenGLBuffers.cpp
		static unsigned int BufferDataTypeToOpenGLType(BufferDataType type);
#endif

	private:
		BufferHelper();
	};


	struct BufferElement
	{
		std::string		Name;		// name of the attribute in shader
		BufferDataType	Type;		// data type in buffer
		int				TypeCount;	// number of elements in one <Type>
		size_t			Offset;		
		bool			Normalized; 

		BufferElement()
			: Name(""), Offset(0), Normalized(false),
			  Type(BufferDataType::None), 
			  TypeCount(BufferHelper::CountOfDataType(BufferDataType::None))
		{}

		BufferElement(std::string name, BufferDataType type, bool normalized = false)
			: Name(name), Offset(0), Normalized(normalized), Type(type),
			  TypeCount(BufferHelper::CountOfDataType(type))
		{}
	};

	class BufferLayout
	{
	public:
		enum class Type {
			None = 0, Interleaved, TightlyPacked
		};
	public:
		BufferLayout()
			: m_VertexCount(0), m_Type(Type::None), m_Stride(0) {}

		BufferLayout(Type type, unsigned int vertexCount)
			: m_VertexCount(vertexCount), m_Type(type), m_Stride(0) {}

		inline BufferLayout& AddElement(const BufferElement& element) { m_Elements.push_back(element); return *this; }

		inline const std::vector<BufferElement>& GetElements() const { return m_Elements; }
		inline unsigned int GetStride() const { return m_Stride; }
		inline unsigned int GetVertexCount() const { return m_VertexCount; }

		void CalculateOffsetsAndStride(); // is called when layout gets assigned to VBO

		std::vector<BufferElement>::iterator begin() { return m_Elements.begin(); }
		std::vector<BufferElement>::iterator end() { return m_Elements.end(); }

		std::vector<BufferElement>::const_iterator begin() const { return m_Elements.begin(); }
		std::vector<BufferElement>::const_iterator end() const { return m_Elements.end(); }

	private:
		std::vector<BufferElement> m_Elements;
		unsigned int m_VertexCount;

		Type m_Type;
		unsigned int m_Stride;
	};


	// -------------------------------------------------------------------------------------------------
	// --------------------------------------------VBO--------------------------------------------------
	class VertexBuffer
	{
	public:
		virtual ~VertexBuffer() {}

		virtual void Bind() const = 0;
		virtual void Unbind() const = 0;

		virtual void SetData(size_t byteSize, float* data, size_t offset) = 0;

		virtual void SetLayout(const BufferLayout& layout) = 0;
		virtual const BufferLayout& GetLayout() const = 0;

		static Ref<VertexBuffer> Create(size_t byteSize);
		static Ref<VertexBuffer> Create(size_t byteSize, float* data);
	};


	// -------------------------------------------------------------------------------------------------
	// --------------------------------------------EBO--------------------------------------------------
	class ElementBuffer
	{
	public:
		virtual ~ElementBuffer() {}

		virtual void Bind() const = 0;
		virtual void Unbind() const = 0;

		virtual void SetData(unsigned int count, unsigned int* data, size_t offset) = 0;

		virtual unsigned int GetCount() const = 0;

		static Ref<ElementBuffer> Create(unsigned int count);
		static Ref<ElementBuffer> Create(unsigned int count, unsigned int* data);
	};



}
