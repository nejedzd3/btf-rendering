#include "crpch.h"
#include "Mesh.h"

namespace Crank
{
	void Mesh::PassMaterialUniforms() const 
	{
		if (m_Shader == m_Material->GetShader())
			m_Material->PassData();
		else
			CR_CORE_ERROR("Mesh::PassMaterialUniforms(): Material shader does NOT match mesh's shader!");
	}
}