#include "crpch.h"
#include "Material.h"

namespace Crank
{
	Material::Material(const Ref<Shader>& shader)
	{
		m_Shader = shader;
		for (auto&[name, description] : m_Shader->GetUniforms())
		{
			if (IsMaterialUniform(name))
			{
				m_Data[name] = Shader::EmptyVariant;
				m_ImGuiRenderDetails[name] = ImGuiDataDetail();
				m_ImGuiRenderDetails[name].Name = name;
				GetMaterialInShaderName(name);
			}
		}
		for (auto& [name, description] : m_Shader->GetTextures())
		{
			if (IsMaterialUniform(name))
				m_Textures[name] = nullptr;
		}
	}

	Material& Material::SetData(const std::string& name, const Shader::UniformVariant& value, const ImGuiDataDetail& imguiDetail)
	{
		std::string fullname = MakeDataNameFull(name);
		auto found = m_Data.find(fullname);
		if (found != m_Data.end())
		{
			m_Data[fullname] = value;

			// Set same name for ImGui
			ImGuiDataDetail tmp = imguiDetail;
			tmp.Name = name;

			m_ImGuiRenderDetails[fullname] = tmp;
		}
		else
			CR_CORE_ERROR("Material::SetData(): \"{0}\" not found in material!", fullname);
		return *this;
	}

	Material& Material::SetTexture(const std::string& name, const Ref<Texture>& texture)
	{
		std::string fullname = MakeDataNameFull(name);
		auto found = m_Textures.find(fullname);
		if (found != m_Textures.end())
			m_Textures[fullname] = texture;
		else
			CR_CORE_ERROR("Material::SetTexture(): \"{0}\" not found in material!", fullname);
		return *this;
	}

	const Shader::UniformVariant& Material::GetData(const std::string& name)
	{
		std::string fullname = MakeDataNameFull(name);
		auto found = m_Data.find(fullname);
		if (found == m_Data.end())
		{
			CR_CORE_ERROR("Material::GetData(): \"{0}\" not found in material!", fullname);
			return Shader::EmptyVariant;
		}
		return found->second;
	}

	Ref<Texture> Material::GetTexture(const std::string& name)
	{
		std::string fullname = MakeDataNameFull(name);
		auto found = m_Textures.find(fullname);
		if (found == m_Textures.end()) 
		{
			CR_CORE_ERROR("Material::GetTexture(): \"{0}\" not found in material!", fullname);
			return nullptr;
		}
		return found->second;
	}

	std::vector<std::string> Material::GetDataNames() const
	{
		std::vector<std::string> names;
		for (auto& it : m_Data)
		{
			names.push_back(it.first);
		}
		return names;
	}

	std::vector<std::string> Material::GetTextureNames() const
	{
		std::vector<std::string> names;
		for (auto& it : m_Textures)
		{
			names.push_back(it.first);
		}
		return names;
	}

	void Material::PassData()
	{
		for (auto& [name, value] : m_Data)
		{
			if (value == Shader::EmptyVariant)
			{
				CR_CORE_ERROR("Material::PassData(): Data for uniform \"{0}\" not found", name);
				continue;
			}
			m_Shader->PassUniform(name, value);
		}
		unsigned int slot = m_TextureSlotRange.x;
		for (auto& [name, texture] : m_Textures)
		{
			if (!texture)
			{
				CR_CORE_ERROR("Material::PassData(): Data for texture \"{0}\" not found", name);
				continue;
			}

			texture->Bind(slot);
			m_Shader->PassUniform(name,(int)slot);

			slot++;
		}
		m_TextureSlotRange.y = --slot;
	}

	void Material::OnImGuiRender()
	{
		ImGuiRenderer::Submit(m_InShaderMaterialName);
		for (auto& [name, value] : m_Data)
		{
			if (value == Shader::EmptyVariant)
				continue;

			if (auto intValuePtr = std::get_if<int>(&value))
			{
				ImGuiRenderer::Submit(intValuePtr, m_ImGuiRenderDetails.at(name));
			}
			else if (auto floatValuePtr = std::get_if<float>(&value))
			{
				ImGuiRenderer::Submit(floatValuePtr, m_ImGuiRenderDetails.at(name));
			}
			else if (auto boolValuePtr = std::get_if<bool>(&value))
			{
				ImGuiRenderer::Submit(boolValuePtr, m_ImGuiRenderDetails.at(name));
			}

			else if (auto uvec2ValuePtr = std::get_if<glm::uvec2>(&value))
			{
				ImGuiRenderer::Submit(*uvec2ValuePtr, m_ImGuiRenderDetails.at(name));
			}
			else if (auto uvec3ValuePtr = std::get_if<glm::uvec3>(&value))
			{
				ImGuiRenderer::Submit(*uvec3ValuePtr, m_ImGuiRenderDetails.at(name));
			}
			else if (auto vec2ValuePtr = std::get_if<glm::vec2>(&value))
			{
				ImGuiRenderer::Submit(*vec2ValuePtr, m_ImGuiRenderDetails.at(name));
			}
			else if (auto vec3ValuePtr = std::get_if<glm::vec3>(&value))
			{
				ImGuiRenderer::Submit(*vec3ValuePtr, m_ImGuiRenderDetails.at(name));
			}
			else if (auto vec4ValuePtr = std::get_if<glm::vec4>(&value))
			{
				ImGuiRenderer::Submit(*vec4ValuePtr, m_ImGuiRenderDetails.at(name));
			}


			else if (auto mat2ValuePtr = std::get_if<glm::mat2>(&value))
			{
				ImGuiRenderer::Submit(*mat2ValuePtr, m_ImGuiRenderDetails.at(name));
			}
			else if (auto mat3ValuePtr = std::get_if<glm::mat3>(&value))
			{
				ImGuiRenderer::Submit(*mat3ValuePtr, m_ImGuiRenderDetails.at(name));
			}
			else if (auto mat4ValuePtr = std::get_if<glm::mat4>(&value))
			{
				ImGuiRenderer::Submit(*mat4ValuePtr, m_ImGuiRenderDetails.at(name));
			}

		}
	}

	bool Material::IsMaterialUniform(const std::string& name)
	{
		return Utility::StartsWith(name, "um_");
	}

	void Material::GetMaterialInShaderName(const std::string& uniformName)
	{
		std::string materialName = "";
		for (char c : uniformName)
		{
			if (c == '.')
				break;
			materialName += c;
		}
		if (m_InShaderMaterialName != materialName && !m_InShaderMaterialName.empty())
			CR_CORE_WARN("Material::GetMaterialInShaderName(): Inconsistency in material's name in shader. Old: \"{0}\", New: \"{1}\"", m_InShaderMaterialName, materialName);
		
		m_InShaderMaterialName = materialName;
	}

	std::string Material::MakeDataNameFull(const std::string& name)
	{
		if (Utility::StartsWith(name, m_InShaderMaterialName))
			return name;
		else
			return m_InShaderMaterialName + "." + name;
	}
}
