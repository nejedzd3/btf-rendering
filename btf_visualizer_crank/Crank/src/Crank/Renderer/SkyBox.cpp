#include "SkyBox.h"
#include "crpch.h"
#include "SkyBox.h"

#include "Crank/Renderer/Buffers.h"
//#include "Crank/Renderer/Texture.h"
//#include "Crank/Renderer/VertexArray.h"

namespace Crank
{
    SkyBox::SkyBox(const Ref<Shader>& shader, const std::vector<std::string>& texturePaths)
	{
        // from https://learnopengl.com/code_viewer.php?code=advanced/cubemaps_skybox_data
        const unsigned int numOfVertices = 36;
        float vertices[numOfVertices * 3] = {
            -1.0f,  1.0f, -1.0f,
            -1.0f, -1.0f, -1.0f,
             1.0f, -1.0f, -1.0f,
             1.0f, -1.0f, -1.0f,
             1.0f,  1.0f, -1.0f,
            -1.0f,  1.0f, -1.0f,

            -1.0f, -1.0f,  1.0f,
            -1.0f, -1.0f, -1.0f,
            -1.0f,  1.0f, -1.0f,
            -1.0f,  1.0f, -1.0f,
            -1.0f,  1.0f,  1.0f,
            -1.0f, -1.0f,  1.0f,

             1.0f, -1.0f, -1.0f,
             1.0f, -1.0f,  1.0f,
             1.0f,  1.0f,  1.0f,
             1.0f,  1.0f,  1.0f,
             1.0f,  1.0f, -1.0f,
             1.0f, -1.0f, -1.0f,

            -1.0f, -1.0f,  1.0f,
            -1.0f,  1.0f,  1.0f,
             1.0f,  1.0f,  1.0f,
             1.0f,  1.0f,  1.0f,
             1.0f, -1.0f,  1.0f,
            -1.0f, -1.0f,  1.0f,

            -1.0f,  1.0f, -1.0f,
             1.0f,  1.0f, -1.0f,
             1.0f,  1.0f,  1.0f,
             1.0f,  1.0f,  1.0f,
            -1.0f,  1.0f,  1.0f,
            -1.0f,  1.0f, -1.0f,

            -1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f,  1.0f,
             1.0f, -1.0f, -1.0f,
             1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f,  1.0f,
             1.0f, -1.0f,  1.0f
        };
	    
        m_Cube = VertexArray::Create(shader);
        auto vbo = VertexBuffer::Create(sizeof(vertices), vertices);
        vbo->SetLayout(BufferLayout(BufferLayout::Type::TightlyPacked, numOfVertices)
                       .AddElement({ "a_Position", BufferDataType::Float3 })
        );
        m_Cube->AddVertexBuffer(vbo);
        
        LoadTexture(texturePaths);
    }

	SkyBox::~SkyBox()
	{
	}

    void SkyBox::LoadTexture(const std::vector<std::string>& texturePaths)
    {
        if (texturePaths.size() < 1)
            return;
        m_CubeMapTexture = CubeMap::Create(texturePaths);
    }
}
