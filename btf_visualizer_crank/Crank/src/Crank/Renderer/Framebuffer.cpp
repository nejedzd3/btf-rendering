#include "crpch.h"
#include "Framebuffer.h"
#include "Platform/OpenGL/OpenGLFramebuffer.h"
#include "Crank/Renderer/RendererAPI.h"

namespace Crank
{
	Ref<Framebuffer> Framebuffer::Create(const Specification& spec)
	{
		switch (RendererAPI::GetAPI())
		{
		case RendererAPI::API::None:
			CR_CORE_WARN("Framebuffer::Create(): No Rendering API is selected.");
			return nullptr;

		case RendererAPI::API::OpenGL:
			return CreateRef<OpenGLFramebuffer>(spec);
		}

		CR_CORE_ERROR("Framebuffer::Create(): Unknown Rendering API.");
		return nullptr;
	}
}
