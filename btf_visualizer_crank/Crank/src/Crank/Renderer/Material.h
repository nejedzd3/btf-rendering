#pragma once
#include <glm/glm.hpp>
#include "Crank/Renderer/Texture.h"
#include "Crank/Renderer/Shader.h"
#include <Crank/ImGui/ImGuiRenderer.h>

namespace Crank
{
	class Material
	{
	public:
		Material(const Ref<Shader>& shader);
		~Material() {}

		Material& SetData(const std::string& name, const Shader::UniformVariant& value, const ImGuiDataDetail& imguiDetail = ImGuiDataDetail());
		Material& SetTexture(const std::string& name, const Ref<Texture>& texture);

		inline void SetTextureSlotRange(const glm::uvec2& range) { m_TextureSlotRange = range; }
		inline void UpdateTextureSlotRangeStart(unsigned int newStart) 
		{ 
			m_TextureSlotRange.x = newStart; 
			m_TextureSlotRange.y = newStart + (unsigned int) m_Textures.size();
		}

		const Shader::UniformVariant& GetData(const std::string& name);
		Ref<Texture> GetTexture(const std::string& name);
		std::vector<std::string> GetDataNames() const;
		std::vector<std::string> GetTextureNames() const;
		inline const Ref<Shader>& GetShader() const { return m_Shader; }

		void PassData();
		
		void OnImGuiRender();
	
	private:
		Ref<Shader> m_Shader = nullptr;
		std::string m_InShaderMaterialName = "";

		std::unordered_map<std::string, Shader::UniformVariant> m_Data;
		std::unordered_map<std::string, Ref<Texture>> m_Textures;

		std::unordered_map<std::string, ImGuiDataDetail> m_ImGuiRenderDetails;

		glm::uvec2 m_TextureSlotRange = { 0, 0 };

	private:
		bool IsMaterialUniform(const std::string& name);
		void GetMaterialInShaderName(const std::string& uniformName);
		std::string MakeDataNameFull(const std::string& name);
	};
}

