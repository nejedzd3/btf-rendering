#pragma once

#include "RendererAPI.h"
#include "VertexArray.h"

#include "Camera.h"
#include "Crank/Renderer/Mesh.h"
# include "Crank/Renderer/SkyBox.h"

namespace Crank
{
	class Renderer
	{
	public:
		static void Init();

		static void BeginScene(const Ref<PerspectiveCamera>& camera);
		static void EndScene();

		static void Submit(const Ref<Mesh>& mesh);
		static void Submit(const Ref<VertexArray>& vertexArray);
		static void Submit(const Ref<SkyBox>& skybox);

		static inline Ref<RendererAPI>& GetAPI() { return s_RendererAPI; }

	private:
		static Ref<RendererAPI> s_RendererAPI;

	private:
		struct SceneData
		{
			Ref<PerspectiveCamera> m_Camera;
			glm::mat4 m_ProjectionViewMatrix;
		};

		static Ref<SceneData> m_SceneData;
	};
}
