#include "crpch.h"
#include "CameraController.h"
#include "Crank/Input/Input.h"
#include "Crank/Events/MouseEvents.h"

#include <glm/gtx/quaternion.hpp>
#include <Crank/Core/Application.h>

// temporary import
#include <GLFW/glfw3.h>

namespace Crank
{
	// -------------------------------------------------------------------------------------------------
	// ------------------------------------Free Camera Controller---------------------------------------

	FreeCameraController::FreeCameraController()
		: m_Camera(CreateRef<PerspectiveCamera>()), m_CameraTransform(m_Camera->GetTransform())
	{}

	FreeCameraController::FreeCameraController(float fov, float aspectRatio, float nearPlane, float farPlane, const glm::vec3& position, const glm::quat& orientation)
		: m_Camera(CreateRef<PerspectiveCamera>(fov, aspectRatio, nearPlane, farPlane)), m_CameraTransform(m_Camera->GetTransform())
	{
		m_CameraTransform.SetPosition(position);
		m_CameraTransform.SetOrientation(orientation);
	}

	void FreeCameraController::OnUpdate(DeltaTime dt)
	{
		if (Input::IsKeyPressed(KeyInput::D))
		{
			m_CameraTransform += m_CameraTransform.GetRightVector() * m_MovementSensitivity * dt.GetSeconds();
		}
		if (Input::IsKeyPressed(KeyInput::A))
		{
			m_CameraTransform -= m_CameraTransform.GetRightVector() * m_MovementSensitivity * dt.GetSeconds();
		}
		if (Input::IsKeyPressed(KeyInput::S))
		{
			m_CameraTransform -= m_CameraTransform.GetForwardVector() * m_MovementSensitivity * dt.GetSeconds();
		}
		if (Input::IsKeyPressed(KeyInput::W))
		{
			m_CameraTransform += m_CameraTransform.GetForwardVector() * m_MovementSensitivity * dt.GetSeconds();
		}
		if (Input::IsKeyPressed(KeyInput::LEFT_CONTROL))
		{
			m_CameraTransform -= glm::vec3(0.0f, 1.0f, 0.0f) * m_MovementSensitivity * dt.GetSeconds();
		}
		if (Input::IsKeyPressed(KeyInput::SPACE))
		{
			m_CameraTransform += glm::vec3(0.0f, 1.0f, 0.0f) * m_MovementSensitivity * dt.GetSeconds();
		}
		
		if (Input::IsMouseButtonPressed(MouseInput::BUTTON_RIGHT))
		{
			glm::vec2 deltaMousePos = { Input::GetMouseX() - m_InitialMousePosition.x, Input::GetMouseY() - m_InitialMousePosition.y };
			m_YawPitch += m_RotationSensitivity * dt.GetSeconds() * -deltaMousePos;

			m_CameraTransform.SetOrientation(glm::quat({ m_YawPitch.y, m_YawPitch.x, 0.0f }));
			
			// move cursor to initial position
			glfwSetCursorPos(static_cast<GLFWwindow*>(Application::Get().GetWindow().GetNativeWindow()),
							 m_InitialMousePosition.x, m_InitialMousePosition.y);
		}

	}

	void FreeCameraController::OnEvent(Event& e)
	{
		EventDispatcher dispatcher(e);

		dispatcher.Dispatch<WindowResizedEvent>(CR_BIND_EVENT_FUNCTION(FreeCameraController::OnResize));

		dispatcher.Dispatch<MouseClickedEvent>([this](MouseClickedEvent& e) {
			if (e.GetButton() == MouseInput::BUTTON_RIGHT)
			{
				m_InitialMousePosition = { Input::GetMouseX(), Input::GetMouseY()};
				glfwSetInputMode(static_cast<GLFWwindow*>(Application::Get().GetWindow().GetNativeWindow()), 
								 GLFW_CURSOR, GLFW_CURSOR_DISABLED); // GLFW_CURSOR_HIDDEN does not work
			}

			return false;
		});

		dispatcher.Dispatch<MouseReleasedEvent>([this](MouseReleasedEvent& e) {
			if (e.GetButton() == MouseInput::BUTTON_RIGHT)
			{
				glfwSetInputMode(static_cast<GLFWwindow*>(Application::Get().GetWindow().GetNativeWindow()),
					GLFW_CURSOR, GLFW_CURSOR_NORMAL);
			}

			return false;
		});

		dispatcher.Dispatch<MouseScrolledEvent>([this](MouseScrolledEvent& e) {
			float zoom = m_Camera->GetZoom();
			zoom += -e.GetYOffset() * m_ZoomSensitivity;
			zoom = glm::clamp(zoom, m_MaxInZoom, m_MaxOutZoom);

			m_Camera->SetZoom(zoom);
			return true;
		});
	}

	bool FreeCameraController::OnResize(WindowResizedEvent& e)
	{
		float newAspectRatio = (float)e.GetNewWidth() / (float)e.GetNewHeight();
		m_Camera->SetAspectRatio(newAspectRatio);

		return false;
	}

	void FreeCameraController::Resize(unsigned int width, unsigned int height)
	{
		float newAspectRatio = (float)width / (float)height;
		m_Camera->SetAspectRatio(newAspectRatio);
	}
	// -------------------------------------------------------------------------------------------------
	// ------------------------------------Orbital Camera Controller---------------------------------------

	OrbitalCameraController::OrbitalCameraController(const glm::vec2& yawPitch, const glm::vec3& targetPos)
		: m_Camera(CreateRef<PerspectiveCamera>()), m_CameraTransform(m_Camera->GetTransform()), m_YawPitch(yawPitch), m_TargetPos(targetPos)
	{
		CalculateChange();
	}

	OrbitalCameraController::OrbitalCameraController(float fov, float aspectRatio, float nearPlane, float farPlane, const glm::vec2& yawPitch, const glm::vec3& targetPos)
		: m_Camera(CreateRef<PerspectiveCamera>(fov, aspectRatio, nearPlane, farPlane)), m_CameraTransform(m_Camera->GetTransform()), m_YawPitch(yawPitch), m_TargetPos(targetPos)
	{
		CalculateChange();
	}

	void OrbitalCameraController::OnUpdate(DeltaTime dt)
	{
		if (Input::IsKeyPressed(KeyInput::D))
		{
			m_YawPitch.x += 0.1f * m_MovementSensitivity * dt.GetSeconds();
			CalculateChange();
		}
		if (Input::IsKeyPressed(KeyInput::A))
		{
			m_YawPitch.x -= 0.1f * m_MovementSensitivity * dt.GetSeconds();
			CalculateChange();
		}
		if (Input::IsKeyPressed(KeyInput::S))
		{
			m_YawPitch.y += 0.1f * m_MovementSensitivity * dt.GetSeconds();
			CalculateChange();
		}
		if (Input::IsKeyPressed(KeyInput::W))
		{
			m_YawPitch.y -= 0.1f * m_MovementSensitivity * dt.GetSeconds();
			CalculateChange();
		}

		if (Input::IsMouseButtonPressed(MouseInput::BUTTON_RIGHT))
		{
			glm::vec2 deltaMousePos = { Input::GetMouseX() - m_InitialMousePosition.x, Input::GetMouseY() - m_InitialMousePosition.y };
			m_YawPitch += m_RotationSensitivity * dt.GetSeconds() * -deltaMousePos;
			CalculateChange();
			
			// move cursor to initial position
			glfwSetCursorPos(static_cast<GLFWwindow*>(Application::Get().GetWindow().GetNativeWindow()),
				m_InitialMousePosition.x, m_InitialMousePosition.y);
		}
	}

	void OrbitalCameraController::OnEvent(Event& e)
	{
		EventDispatcher dispatcher(e);

		dispatcher.Dispatch<WindowResizedEvent>(CR_BIND_EVENT_FUNCTION(OrbitalCameraController::OnResize));

		dispatcher.Dispatch<MouseClickedEvent>([this](MouseClickedEvent& e) {
			if (e.GetButton() == MouseInput::BUTTON_RIGHT)
			{
				m_InitialMousePosition = { Input::GetMouseX(), Input::GetMouseY() };
				glfwSetInputMode(static_cast<GLFWwindow*>(Application::Get().GetWindow().GetNativeWindow()),
					GLFW_CURSOR, GLFW_CURSOR_DISABLED); // GLFW_CURSOR_HIDDEN does not work
			}

			return false;
			});

		dispatcher.Dispatch<MouseReleasedEvent>([this](MouseReleasedEvent& e) {
			if (e.GetButton() == MouseInput::BUTTON_RIGHT)
			{
				glfwSetInputMode(static_cast<GLFWwindow*>(Application::Get().GetWindow().GetNativeWindow()),
					GLFW_CURSOR, GLFW_CURSOR_NORMAL);
			}

			return false;
			});

		dispatcher.Dispatch<MouseScrolledEvent>([this](MouseScrolledEvent& e) {
			float zoom = m_Camera->GetZoom();
			zoom += -e.GetYOffset() * m_ZoomSensitivity;
			zoom = glm::clamp(zoom, m_MaxInZoom, m_MaxOutZoom);

			m_Camera->SetZoom(zoom);
			return true;
			});
	}

	bool OrbitalCameraController::OnResize(WindowResizedEvent& e)
	{
		float newAspectRatio = (float)e.GetNewWidth() / (float)e.GetNewHeight();
		m_Camera->SetAspectRatio(newAspectRatio);

		return false;
	}

	void OrbitalCameraController::Resize(unsigned int width, unsigned int height)
	{
		float newAspectRatio = (float)width / (float)height;
		m_Camera->SetAspectRatio(newAspectRatio);
	}
	void OrbitalCameraController::CalculateChange()
	{
		m_YawPitch.y = glm::clamp(m_YawPitch.y, glm::pi<float>() / -2.0f, 0.0f);
		if (m_YawPitch.x > 2.0f * glm::pi<float>() || m_YawPitch.x < -2.0f * glm::pi<float>())
			m_YawPitch.x = 0.0f;

		m_CameraTransform.SetOrientation(glm::quat({ m_YawPitch.y, m_YawPitch.x, 0.0f }));
		m_CameraTransform.SetPosition(m_TargetPos + glm::vec3(m_CameraTransform.GetOrientation() * glm::vec3(0.0f, 0.0f, m_Radius)));
	}
}
