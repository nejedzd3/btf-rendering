#include "crpch.h"
#include "Camera.h"

#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/glm.hpp>

namespace Crank
{
	// -------------------------------------------------------------------------------------------------
	// -------------------------------------Perspective Camera------------------------------------------

	PerspectiveCamera::PerspectiveCamera(float fov, float aspectRatio, float nearPlane, float farPlane)
		: m_FOV(fov), m_AspectRatio(aspectRatio), m_NearPlane(nearPlane), m_FarPlane(farPlane)
	{
		m_ProjectionMatrix = glm::perspective(glm::radians(fov * m_Zoom), aspectRatio, nearPlane, farPlane);
	}

	void PerspectiveCamera::SetProjection(float fov, float aspectRatio, float nearPlane, float farPlane)
	{
		m_ProjectionMatrix = glm::perspective(glm::radians(fov * m_Zoom), aspectRatio, nearPlane, farPlane);
		m_FOV = fov; m_AspectRatio = aspectRatio;
		m_NearPlane = nearPlane; m_FarPlane = farPlane;
	}

	void PerspectiveCamera::UpdateProjection()
	{
		m_ProjectionMatrix = glm::perspective(glm::radians(m_FOV * m_Zoom), m_AspectRatio, m_NearPlane, m_FarPlane);
	}


	// -------------------------------------------------------------------------------------------------
	// -------------------------------------Orthographic Camera-----------------------------------------

	OrthographicCamera::OrthographicCamera(float left, float right, float bottom, float top)
	{
		SetProjection(left, right, bottom, top);
		CalculateViewMatrix();

		m_ProjectionViewMatrix = m_ProjectionMatrix * m_ViewMatrix;
	}

	void OrthographicCamera::SetProjection(float left, float right, float bottom, float top)
	{
		m_ProjectionMatrix = glm::ortho(left, right, bottom, top, -1.0f, 1.0f);
		m_ProjectionViewMatrix = m_ProjectionMatrix * m_ViewMatrix;
	}

	void OrthographicCamera::CalculateViewMatrix()
	{
		glm::mat4 transform = glm::translate(glm::mat4(1.0f), m_Position) *
			glm::rotate(glm::mat4(1.0f), glm::radians(m_Rotation), { 0, 0, 1 });

		m_ViewMatrix = glm::inverse(transform);
		m_ProjectionViewMatrix = m_ProjectionMatrix * m_ViewMatrix;
	}
}
