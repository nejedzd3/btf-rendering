#pragma once
#include "crpch.h"
#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>

namespace Crank
{
	class Transform
	{
	public:
		Transform() 
		{ 
			RecalculateCached(); 
		}

		~Transform() {}

		inline glm::vec3 GetForwardVector() const
		{
			return glm::vec3((m_ModelMatrix * glm::vec4(0.0f, 0.0f, -1.0f, 0.0f)));
		}

		inline glm::vec3 GetUpVector() const
		{
			return glm::vec3((m_ModelMatrix * glm::vec4(0.0f, 1.0f, 0.0f, 0.0f)));
		}

		inline glm::vec3 GetRightVector() const
		{
			return glm::vec3((m_ModelMatrix * glm::vec4(1.0f, 0.0f, 0.0f, 0.0f)));
		}

		inline const glm::mat4& GetModelMatrix()
		{
			if (!m_Changed)
				return m_ModelMatrix;

			RecalculateCached();
			return m_ModelMatrix;
		}

		inline const glm::mat4& GetInverseModelMatrix()
		{
			if (!m_Changed)
				return m_InverseModelMatrix;

			RecalculateCached();
			return m_InverseModelMatrix;
		}

		inline glm::vec3& GetPosition() { return m_Position; }
		inline glm::vec3& GetScale() { return m_Scale; }
		inline glm::quat& GetOrientation() { return m_Orientation; }

		inline const glm::vec3& GetPosition() const { return m_Position; }
		inline const glm::vec3& GetScale() const { return m_Scale; }
		inline const glm::quat& GetOrientation() const { return m_Orientation; }
		inline glm::mat4 GetRotation() const { return glm::toMat4(m_Orientation); }

		inline void SetPosition(const glm::vec3& position) { m_Position = position; m_Changed = true; }
		inline void SetScale(const glm::vec3& scale) { m_Scale = scale; m_Changed = true; }
		inline void SetOrientation(const glm::quat& orientation) { m_Orientation = glm::normalize(orientation); m_Changed = true; }

		Transform& operator+=(const glm::vec3& other)
		{
			m_Position += other;
			m_Changed = true;
			return *this;
		}

		Transform& operator-=(const glm::vec3& other)
		{
			m_Position -= other;
			m_Changed = true;
			return *this;
		}

	private:
		inline void RecalculateCached()
		{
			m_ModelMatrix = glm::translate(glm::mat4(1.0f), m_Position) * glm::toMat4(m_Orientation) * glm::scale(glm::mat4(1.0f), m_Scale);
			m_InverseModelMatrix = glm::inverse(m_ModelMatrix);
			m_Changed = false;
		}

	private:
		glm::vec3 m_Position = { 0.0f, 0.0f, 0.0f };
		glm::vec3 m_Scale = { 1.0f, 1.0f, 1.0f };
		glm::quat m_Orientation = glm::quat(glm::vec3(0.0f, 0.0f, 0.0f));

		glm::mat4 m_ModelMatrix, m_InverseModelMatrix;

		bool m_Changed = false;
	};
}
