#pragma once
#include "Crank/Renderer/VertexArray.h"
#include "Crank/Renderer/Shader.h"
#include "Crank/Renderer/Transform.h"
#include "Crank/Renderer/Material.h"

namespace Crank {
	class Mesh
	{
	public:
		Mesh(const Ref<Shader>& shader, const Ref<VertexArray>& vao, const Transform& transform)
			: m_VertexArray(vao), m_Shader(shader), m_Transform(transform)
		{}

		~Mesh() {}

		void PassMaterialUniforms() const;

		inline const Ref<Shader>& GetShader() const { return m_Shader; }
		inline const Ref<VertexArray>& GetVertexArray() const { return m_VertexArray; }
		inline const Ref<Material>& GetMaterial() const { return m_Material; }
		inline Transform& GetTransform() { return m_Transform; }

		inline void SetMaterial(const Ref<Material>& material) { m_Material = material; }

	private:
		Ref<VertexArray> m_VertexArray;
		Ref<Shader> m_Shader;
		Ref<Material> m_Material;

		Transform m_Transform;

	private:
		friend class MeshLoader;
	};
}
