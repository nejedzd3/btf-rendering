#include "crpch.h"
#include "VertexArray.h"
#include "Crank/Renderer/RendererAPI.h"

#include "Platform/OpenGL/OpenGLVertexArray.h"

namespace Crank
{
	Ref<VertexArray> VertexArray::Create(const Ref<Shader>& shader)
	{
		switch (RendererAPI::GetAPI())
		{
		case RendererAPI::API::None:
			CR_CORE_WARN("VertexArray::Create(): No Rendering API is selected.");
			return nullptr;

		case RendererAPI::API::OpenGL:
			return CreateRef<OpenGLVertexArray>(shader);
		}

		CR_CORE_ERROR("VertexArray::Create(): Unknown Rendering API.");
		return nullptr;
	}
}
