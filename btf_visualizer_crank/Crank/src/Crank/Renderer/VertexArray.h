#pragma once
#include "Crank/Renderer/Buffers.h"
#include "Crank/Renderer/Shader.h"


namespace Crank
{
	class VertexArray
	{
	public:
		virtual ~VertexArray() {}

		virtual void Bind() const = 0;
		virtual void Unbind() const = 0;

		virtual void AddVertexBuffer(const Ref<VertexBuffer>& vertexBuffer) = 0;
		virtual void AddElementBuffer(const Ref<ElementBuffer>& elementBuffer) = 0;

		virtual const std::vector<Ref<VertexBuffer>>& GetVertexBuffers() = 0;
		virtual const Ref<ElementBuffer>& GetElementBuffer() = 0;
		virtual unsigned int GetIndexCount() const = 0;
		virtual unsigned int GetVertexCount() const = 0;
		virtual const Ref<Shader>& GetShader() const = 0;

		static Ref<VertexArray> Create(const Ref<Shader>& shader);
	};
}
