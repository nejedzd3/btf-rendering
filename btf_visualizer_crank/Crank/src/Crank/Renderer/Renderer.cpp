#include "crpch.h"
#include "Renderer.h"
#include "Platform/OpenGL/OpenGLShader.h"
#include "Platform/OpenGL/OpenGLRendererAPI.h"

namespace Crank
{
#ifdef CR_RENDERER_API_OPENGL
	Ref<RendererAPI> Renderer::s_RendererAPI = CreateRef<OpenGLRendererAPI>();
#else
	Ref<RendererAPI> Renderer::s_RendererAPI = nullptr;
	CR_CORE_ERROR("NO RENDERING API SELECTED");
#endif

	Ref<Renderer::SceneData> Renderer::m_SceneData = CreateRef<Renderer::SceneData>();

	void Renderer::Init()
	{
		s_RendererAPI->Init();
	}

	void Renderer::BeginScene(const Ref<PerspectiveCamera>& camera)
	{
		m_SceneData->m_ProjectionViewMatrix = camera->GetProjectionMatrix() * camera->GetViewMatrix();
		m_SceneData->m_Camera = camera;
	}

	void Renderer::EndScene()
	{
	}

	void Renderer::Submit(const Ref<Mesh>& mesh)
	{
		const Ref<Shader>& shader = mesh->GetShader();
		shader->Bind();

		// should happen in mesh (mesh->SetUniforms())
		shader->PassUniform("u_PVM", m_SceneData->m_ProjectionViewMatrix * mesh->GetTransform().GetModelMatrix());
		shader->PassUniform("u_ModelMatrix", mesh->GetTransform().GetModelMatrix());

		mesh->PassMaterialUniforms();

		s_RendererAPI->DrawElements(mesh->GetVertexArray());
	}

	void Renderer::Submit(const Ref<VertexArray>& vertexArray)
	{
		const Ref<Shader>& shader = vertexArray->GetShader();
		shader->Bind();
		s_RendererAPI->DrawElements(vertexArray);
	}

	void Renderer::Submit(const Ref<SkyBox>& skybox)
	{
		const Ref<VertexArray> vao = skybox->GetVertexArray();
		const Ref<Shader>& shader = vao->GetShader();
		
		// in VS the z coordinate is set to w, thus achieving maximal depth
		s_RendererAPI->SetDepthFunction(RendererAPI::DepthFunction::LEqual);

		shader->Bind();

		shader->PassUniform("u_ProjectionView", 
			m_SceneData->m_Camera->GetProjectionMatrix() * 
			glm::mat4(glm::mat3(m_SceneData->m_Camera->GetViewMatrix())) // remove translation from view matrix
		);
		skybox->GetTexture()->Bind(0);

		s_RendererAPI->DrawArrays(vao);

		s_RendererAPI->SetDepthFunction(RendererAPI::DepthFunction::Less);
	}
}
