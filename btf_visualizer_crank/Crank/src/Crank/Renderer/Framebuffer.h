#pragma once

namespace Crank
{
	class Texture2D;

	class Framebuffer
	{
	public:
		struct Specification
		{
			unsigned int Width, Height;
			unsigned int MSAASamples;

			bool SwapChainTarget = false;

			Specification(unsigned int width, unsigned int height, unsigned int msaaSamples = 0)
				: Width(width), Height(height), MSAASamples(msaaSamples)
			{}

			inline bool UsesMSAA() const { return MSAASamples > 0; }
		};

	public:
		virtual const Specification& GetSpecification() const = 0;
		virtual void Overwrite() = 0;
		virtual void Resize(unsigned int width, unsigned int height) = 0;

		virtual void Bind() const = 0;
		virtual void Unbind() const = 0;

		virtual unsigned int GetHandle() const = 0;

		virtual Ref<Texture2D> GetColorAttachment() const = 0;

		static Ref<Framebuffer> Create(const Specification& spec);

	};
}
