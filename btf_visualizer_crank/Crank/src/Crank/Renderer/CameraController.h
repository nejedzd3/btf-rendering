#pragma once

#include "Camera.h"
#include "Crank/Core/Time.h"
#include "Crank/Events/Event.h"
#include "Crank/Events/ApplicationEvents.h"

namespace Crank
{
	// -------------------------------------------------------------------------------------------------
	// ------------------------------------Free Camera Controller---------------------------------------
	class FreeCameraController
	{
	public:
		FreeCameraController();
		FreeCameraController(float fov, float aspectRatio, float nearPlane, float farPlane, 
							 const glm::vec3& position, const glm::quat& orientation);
		~FreeCameraController() {}

		void OnUpdate(DeltaTime dt);
		void OnEvent(Event& e);

		bool OnResize(WindowResizedEvent& e);
		void Resize(unsigned int width, unsigned int height);

		inline const glm::vec2& GetYawPitch() const { return m_YawPitch; }

		inline const Ref<PerspectiveCamera>& GetCamera() const { return m_Camera; }
		inline Ref<PerspectiveCamera>& GetCamera() { return m_Camera; }

	private:
		Ref<PerspectiveCamera> m_Camera;
		Transform& m_CameraTransform;

		glm::vec2 m_YawPitch = { 0.0f, 0.0f };
		glm::vec2 m_InitialMousePosition = { 0.0f, 0.0f };

		float m_MovementSensitivity = 1.0f,
			  m_RotationSensitivity = 0.3f,
			  m_ZoomSensitivity = 0.1f,
			  m_MaxInZoom = 0.1f,
			  m_MaxOutZoom = 2.0f;
	};

	// -------------------------------------------------------------------------------------------------
	// ------------------------------------Orbital Camera Controller---------------------------------------
	class OrbitalCameraController
	{
	public:
		OrbitalCameraController(const glm::vec2& yawPitch = glm::vec2(0.0f), const glm::vec3& targetPos = glm::vec3(0.0f));
		OrbitalCameraController(float fov, float aspectRatio, float nearPlane, float farPlane,
			const glm::vec2& yawPitch, const glm::vec3& targetPos);
		~OrbitalCameraController() {}

		void OnUpdate(DeltaTime dt);
		void OnEvent(Event& e);

		bool OnResize(WindowResizedEvent& e);
		void Resize(unsigned int width, unsigned int height);

		inline const glm::vec2& GetYawPitch() const { return m_YawPitch; }

		inline const Ref<PerspectiveCamera>& GetCamera() const { return m_Camera; }
		inline Ref<PerspectiveCamera>& GetCamera() { return m_Camera; }

	private:
		Ref<PerspectiveCamera> m_Camera;
		Transform& m_CameraTransform;

		glm::vec2 m_InitialMousePosition = { 0.0f, 0.0f };

		glm::vec2 m_YawPitch = { 0.0f, 0.0f };
		glm::vec3 m_TargetPos = { 0.0f, 0.0f, 0.0f };
		float m_Radius = 1.0f;

		float m_MovementSensitivity = 5.0f,
			m_RotationSensitivity = 0.5f,
			m_ZoomSensitivity = 0.1f,
			m_MaxInZoom = 0.1f,
			m_MaxOutZoom = 2.0f;

	private:
		void CalculateChange();
	};
}
