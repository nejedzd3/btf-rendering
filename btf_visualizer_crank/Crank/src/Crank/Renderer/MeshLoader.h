#pragma once
#include "Crank/Renderer/Mesh.h"

namespace Crank
{
	class MeshLoader
	{
	public:
		MeshLoader() {}
		~MeshLoader() {}

		static Ref<Mesh> LoadMesh(const std::string& fileName, const Ref<Shader>& shader);

	private:


	private:
		friend class Mesh;
	};

}
