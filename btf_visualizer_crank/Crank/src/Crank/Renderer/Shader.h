#pragma once

#include <glm/glm.hpp>

namespace Crank
{
	class Shader
	{
	public:
		using UniformVariant = std::variant<bool, int, float, glm::vec2, glm::vec3, glm::vec4, glm::uvec2, 
											glm::uvec3, glm::mat2, glm::mat3, glm::mat4, std::monostate>;
		
		static const UniformVariant EmptyVariant;

		enum class DataType
		{
			None = 0, Bool, Float, Int, Vec2, Vec3, Vec4, Mat2, Mat3, Mat4, Sampler1D, Sampler2D, Sampler3D, SamplerCube
		};

		struct DataDescription
		{
			int Location;
			DataType Type;

			DataDescription() : Location(-1), Type(DataType::None) {}
			DataDescription(int location, DataType type) : Location(location), Type(type) {}
		};

	public:
		virtual ~Shader() {}

		virtual void Bind() const = 0;
		virtual void Unbind() const = 0;

		virtual int GetLocation(const std::string& input) const = 0;

		virtual void PassUniform(const std::string& name, const glm::mat4& value) = 0;
		virtual void PassUniform(const std::string& name, const glm::mat3& value) = 0;
		virtual void PassUniform(const std::string& name, const glm::uvec3& value) = 0;
		virtual void PassUniform(const std::string& name, const glm::uvec2& value) = 0;
		virtual void PassUniform(const std::string& name, const glm::vec4& value) = 0;
		virtual void PassUniform(const std::string& name, const glm::vec3& value) = 0;
		virtual void PassUniform(const std::string& name, const glm::vec2& value) = 0;
		virtual void PassUniform(const std::string& name, float value) = 0;
		virtual void PassUniform(const std::string& name, int value) = 0;
		virtual void PassUniform(const std::string& name, const UniformVariant& value) = 0;

		virtual const std::unordered_map<std::string, Shader::DataDescription>& GetUniforms() const = 0;
		virtual const std::unordered_map<std::string, Shader::DataDescription>& GetAttributes() const = 0;
		virtual const std::unordered_map<std::string, Shader::DataDescription>& GetTextures() const = 0;

		static Ref<Shader> Create(const std::string& fileName);
	};
}
