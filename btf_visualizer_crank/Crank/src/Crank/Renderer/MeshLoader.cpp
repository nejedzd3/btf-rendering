#include "crpch.h"
#include "MeshLoader.h"

#include "Crank/Renderer/Buffers.h"

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

namespace Crank
{
	Ref<Mesh> MeshLoader::LoadMesh(const std::string& fileName, const Ref<Shader>& shader)
	{
		
		const aiScene* scene = NULL;

		scene = aiImportFile(fileName.c_str(), aiProcessPreset_TargetRealtime_MaxQuality);

		if (!scene)
		{
			CR_CORE_ERROR("MeshLoader::LoadMesh(): Failed to load mesh from file \"{0}\"", aiGetErrorString());
			return nullptr;
		}

		if (scene->mNumMeshes < 1)
		{
			CR_CORE_ERROR("MeshLoader::LoadMesh(): Mesh not found in file \"{0}\"", aiGetErrorString());
			return nullptr;
		}

		auto *mesh = scene->mMeshes[0];

		unsigned int numVertices = mesh->mNumVertices;

		Ref<VertexBuffer> vbo = VertexBuffer::Create((size_t)numVertices * 8 * sizeof(float)); // accounting for position, normal, texcoord
		vbo->SetData((size_t)numVertices * 3 * sizeof(float), (float*)mesh->mVertices, 0);
		vbo->SetData((size_t)numVertices * 3 * sizeof(float), (float*)mesh->mNormals, numVertices * 3 * sizeof(float));

		std::vector<float> textureCoordinates;

		// WARNING: - assimp's mesh->mTextureCoords is an array of arrays that contain desired texture coords
		//			- this array is for type vec3 and has z-th coordinate set to 0 (for UVs)
		for (unsigned int i = 0; i < numVertices; ++i) 
		{
			textureCoordinates.push_back(mesh->mTextureCoords[0][i].x);
			textureCoordinates.push_back(mesh->mTextureCoords[0][i].y);
		}

		vbo->SetData((size_t)numVertices * 2 * sizeof(float), &textureCoordinates[0], numVertices * 6 * sizeof(float));

		vbo->SetLayout(BufferLayout(BufferLayout::Type::TightlyPacked, numVertices)
						.AddElement({"a_Position", BufferDataType::Float3})
						.AddElement({"a_Normal", BufferDataType::Float3})
						.AddElement({"a_TexCoord", BufferDataType::Float2})
		);

		Ref<ElementBuffer> ebo = ElementBuffer::Create(mesh->mNumFaces * 3);

		unsigned int *indices = new unsigned int[mesh->mNumFaces * 3];

		for (unsigned int i = 0, j = 0; i < mesh->mNumFaces; i++, j += 3)
		{
			indices[j] = mesh->mFaces[i].mIndices[0];
			indices[j + 1] = mesh->mFaces[i].mIndices[1];
			indices[j + 2] = mesh->mFaces[i].mIndices[2];
		}

		ebo->SetData(mesh->mNumFaces * 3, indices, 0);

		delete[] indices;

		Ref<VertexArray> vao = VertexArray::Create(shader);
		vao->AddElementBuffer(ebo);
		vao->AddVertexBuffer(vbo);

		aiReleaseImport(scene);
		
		return CreateRef<Mesh>(shader, vao, Transform());
	}
}