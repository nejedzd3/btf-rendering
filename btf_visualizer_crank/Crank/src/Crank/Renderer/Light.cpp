#include "crpch.h"
#include "Light.h"

namespace Crank
{
	Light::Light(const Ref<Shader>& shader)
	{
		m_Shader = shader;
		for (auto& [name, description] : m_Shader->GetUniforms())
		{
			if (IsLightUniform(name))
			{
				m_Data[name] = Shader::EmptyVariant;
				m_ImGuiRenderDetails[name] = ImGuiDataDetail();
				m_ImGuiRenderDetails[name].Name = name;
				GetInShaderName(name);
			}
		}
	}

	Light& Light::SetData(const std::string& name, const Shader::UniformVariant& value, const ImGuiDataDetail& imguiDetail)
	{
		std::string fullname = MakeDataNameFull(name);
		auto found = m_Data.find(fullname);
		if (found != m_Data.end())
		{
			m_Data[fullname] = value;

			// Set same name for ImGui
			ImGuiDataDetail tmp = imguiDetail;
			tmp.Name = name;

			m_ImGuiRenderDetails[fullname] = tmp;
		}
		else
			CR_CORE_ERROR("Light::SetData(): \"{0}\" not found in light!", fullname);
		return *this;
	}

	const Shader::UniformVariant& Light::GetData(const std::string& name)
	{
		std::string fullname = MakeDataNameFull(name);
		auto found = m_Data.find(fullname);
		if (found == m_Data.end())
		{
			CR_CORE_ERROR("Light::GetData(): \"{0}\" not found in light!", fullname);
			return Shader::EmptyVariant;
		}
		return found->second;
	}

	std::vector<std::string> Light::GetDataNames() const
	{
		std::vector<std::string> names;
		for (auto& it : m_Data)
		{
			names.push_back(it.first);
		}
		return names;
	}

	void Light::PassData()
	{
		for (auto& [name, value] : m_Data)
		{
			if (value == Shader::EmptyVariant)
			{
				CR_CORE_ERROR("Light::PassData(): Data for uniform \"{0}\" not assigned", name);
				continue;
			}
			m_Shader->PassUniform(name, value);
		}
	}

	void Light::OnImGuiRender()
	{
		ImGuiRenderer::Submit(m_InShaderName);
		for (auto& [name, value] : m_Data)
		{
			if (value == Shader::EmptyVariant)
				continue;

			if (auto intValuePtr = std::get_if<int>(&value))
			{
				ImGuiRenderer::Submit(intValuePtr, m_ImGuiRenderDetails.at(name));
			}
			else if (auto floatValuePtr = std::get_if<float>(&value))
			{
				ImGuiRenderer::Submit(floatValuePtr, m_ImGuiRenderDetails.at(name));
			}
			else if (auto boolValuePtr = std::get_if<bool>(&value))
			{
				ImGuiRenderer::Submit(boolValuePtr, m_ImGuiRenderDetails.at(name));
			}

			else if (auto uvec2ValuePtr = std::get_if<glm::uvec2>(&value))
			{
				ImGuiRenderer::Submit(*uvec2ValuePtr, m_ImGuiRenderDetails.at(name));
			}
			else if (auto uvec3ValuePtr = std::get_if<glm::uvec3>(&value))
			{
				ImGuiRenderer::Submit(*uvec3ValuePtr, m_ImGuiRenderDetails.at(name));
			}
			else if (auto vec2ValuePtr = std::get_if<glm::vec2>(&value))
			{
				ImGuiRenderer::Submit(*vec2ValuePtr, m_ImGuiRenderDetails.at(name));
			}
			else if (auto vec3ValuePtr = std::get_if<glm::vec3>(&value))
			{
				ImGuiRenderer::Submit(*vec3ValuePtr, m_ImGuiRenderDetails.at(name));
			}
			else if (auto vec4ValuePtr = std::get_if<glm::vec4>(&value))
			{
				ImGuiRenderer::Submit(*vec4ValuePtr, m_ImGuiRenderDetails.at(name));
			}


			else if (auto mat2ValuePtr = std::get_if<glm::mat2>(&value))
			{
				ImGuiRenderer::Submit(*mat2ValuePtr, m_ImGuiRenderDetails.at(name));
			}
			else if (auto mat3ValuePtr = std::get_if<glm::mat3>(&value))
			{
				ImGuiRenderer::Submit(*mat3ValuePtr, m_ImGuiRenderDetails.at(name));
			}
			else if (auto mat4ValuePtr = std::get_if<glm::mat4>(&value))
			{
				ImGuiRenderer::Submit(*mat4ValuePtr, m_ImGuiRenderDetails.at(name));
			}

		}
	}

	bool Light::IsLightUniform(const std::string& name)
	{
		return Utility::StartsWith(name, "ul_");
	}

	void Light::GetInShaderName(const std::string& uniformName)
	{
		std::string lightName = "";
		for (char c : uniformName)
		{
			if (c == '.')
				break;
			lightName += c;
		}
		if (m_InShaderName != lightName && !m_InShaderName.empty())
			CR_CORE_WARN("Light::GetInShaderName(): Inconsistency in material's name in shader. Old: \"{0}\", New: \"{1}\"", m_InShaderName, lightName);

		m_InShaderName = lightName;
	}

	std::string Light::MakeDataNameFull(const std::string& name)
	{
		if (Utility::StartsWith(name, m_InShaderName))
			return name;
		else
			return m_InShaderName + "." + name;
	}
}
