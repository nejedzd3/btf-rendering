#pragma once

#include <glm/glm.hpp>

namespace Crank
{
	class Texture
	{
	public:
		enum class Parameter
		{
			None = 0, MinificationFiltering, MagnificationFiltering, WrappingS, WrappingT, WrappingR
		};

		enum class Format { None = 0, RGB, RGBA, DEPTH24_STENCIL8 };

		struct Specification
		{
			Format Format;
			unsigned int BitsPerChannel;
			unsigned int Width, Height;
			unsigned int MipMapLevels;
			unsigned int MSAASamples;

			Specification() : Format(Format::None), BitsPerChannel(0), Width(0), Height(0), MipMapLevels(0), MSAASamples(0) {}

			Specification(Texture::Format format, unsigned int bitsPerChannel, unsigned int width,
				unsigned int height, unsigned int mipMapLevels, unsigned int msaaSamples = 0)
				: Format(format), BitsPerChannel(bitsPerChannel), Width(width),
				  Height(height), MipMapLevels(mipMapLevels), MSAASamples(msaaSamples)
			{}

			unsigned int MaxMipMapLevels() const { return (unsigned int) floor(log2(glm::max(Width, Height))); }
			void SetMaxMipMapLevels() { MipMapLevels = MaxMipMapLevels(); }
		};

	public:
		virtual ~Texture() {}

		virtual unsigned int GetWidth() const = 0;
		virtual unsigned int GetHeight() const = 0;
		virtual unsigned int GetHandle() const = 0;

		virtual void SetParameter(Parameter parameter, int value) = 0;
		virtual void SetData(unsigned char* data) = 0;

		virtual void Bind(unsigned int slot = 0) const = 0;
	};


	class Texture2D : public Texture
	{
	public:
		static Ref<Texture2D> Create(const std::string& path, bool useMipMaps = true);
		static Ref<Texture2D> Create(const Specification& spec);

		virtual Specification& GetSpec() = 0;
		virtual const Specification& GetSpec() const = 0;
	};

	class Texture2DMultiSampled : public Texture
	{
	public:
		static Ref<Texture2DMultiSampled> Create(const Specification& spec);

		virtual Specification& GetSpec() = 0;
		virtual const Specification& GetSpec() const = 0;
	};

	class CubeMap : public Texture
	{
	public:
		static Ref<CubeMap> Create(const std::vector<std::string>& paths);
	};


}
