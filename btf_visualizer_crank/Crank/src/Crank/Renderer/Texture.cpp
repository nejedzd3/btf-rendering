#include "crpch.h"
#include "Texture.h"

#include "Crank/Renderer/Renderer.h"
#include "Platform/OpenGL/OpenGLTexture.h"

namespace Crank
{
	Ref<Texture2D> Texture2D::Create(const std::string& path, bool useMipMaps)
	{
		switch (RendererAPI::GetAPI())
		{
		case RendererAPI::API::None:
			CR_CORE_WARN("Texture2D::Create(): No Rendering API is selected.");
			return nullptr;

		case RendererAPI::API::OpenGL:
			return CreateRef<OpenGLTexture2D>(path, useMipMaps);
		}

		CR_CORE_ERROR("Texture2D::Create(): Unknown Rendering API.");
		return nullptr;
	}

	Ref<Texture2D> Texture2D::Create(const Specification& spec)
	{
		switch (RendererAPI::GetAPI())
		{
		case RendererAPI::API::None:
			CR_CORE_WARN("Texture2D::Create(): No Rendering API is selected.");
			return nullptr;

		case RendererAPI::API::OpenGL:
			return CreateRef<OpenGLTexture2D>(spec);
		}

		CR_CORE_ERROR("Texture2D::Create(): Unknown Rendering API.");
		return nullptr;
	}

	Ref<Texture2DMultiSampled> Texture2DMultiSampled::Create(const Specification& spec)
	{
		switch (RendererAPI::GetAPI())
		{
		case RendererAPI::API::None:
			CR_CORE_WARN("Texture2D::Create(): No Rendering API is selected.");
			return nullptr;

		case RendererAPI::API::OpenGL:
			return CreateRef<OpenGLTexture2DMultiSampled>(spec);
		}

		CR_CORE_ERROR("Texture2D::Create(): Unknown Rendering API.");
		return nullptr;
	}


	Ref<CubeMap> CubeMap::Create(const std::vector<std::string>& paths)
	{
		switch (RendererAPI::GetAPI())
		{
		case RendererAPI::API::None:
			CR_CORE_WARN("Texture2D::Create(): No Rendering API is selected.");
			return nullptr;

		case RendererAPI::API::OpenGL:
			return CreateRef<OpenGLCubeMap>(paths);
		}

		CR_CORE_ERROR("Texture2D::Create(): Unknown Rendering API.");
		return nullptr;
	}
}
