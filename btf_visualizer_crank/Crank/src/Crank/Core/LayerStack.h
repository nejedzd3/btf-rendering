#pragma once

#include "Crank/Core/Layer.h"

namespace Crank {

	using LayerVector = std::vector<Layer*>;

	class LayerStack
	{
	public:
		LayerStack();
		~LayerStack();

		/**
		 * Pushes layer into the layer stack. Takes ownership of the Layer object.
		 * 
		 * \param layer
		 */
		void PushLayer(Layer* layer);

		/**
		 * Pushes overlay into the layer stack. Takes ownership of the Layer object.
		 *
		 * \param layer
		 */
		void PushOverlay(Layer* overlay);

		/**
		 * Removes given layer from the layer stack. Frees the layer object.
		 * 
		 * \param layer
		 */
		void PopLayer(Layer* layer);

		/**
		 * Removes given overlay from the layer stack. Frees the overlay object.
		 *
		 * \param layer
		 */
		void PopOverlay(Layer* overlay);

		LayerVector::iterator begin() { return m_Layers.begin(); }
		LayerVector::iterator end()	{ return m_Layers.end(); }

	private:
		LayerVector m_Layers;
		int m_LayerInsertIndex = 0;
	};
}

