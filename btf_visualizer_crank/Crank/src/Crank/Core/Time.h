#pragma once
#include "crpch.h"

namespace Crank
{
	using TimePoint = std::chrono::time_point<std::chrono::system_clock>;

	class DeltaTime
	{
	public:
		DeltaTime(float time = 0.0f)
			: m_Time(time)
		{}

		operator float() const { return m_Time; }
		operator float() { return m_Time; }

		inline float GetSeconds() const { return m_Time; }
		inline float GetMilliseconds() const {return m_Time * 1000;}

	private:
		float m_Time;
	};

	class Timer
	{
	public:
		Timer()
		{
			m_StartTime = std::chrono::system_clock::now();
		}

		~Timer()
		{
			m_EndTime = std::chrono::system_clock::now();
		}

		inline void Start() { m_StartTime = std::chrono::system_clock::now(); m_Ended = false; }

		inline float EndMilliSeconds()
		{
			m_Ended = true;
			m_EndTime = std::chrono::system_clock::now();
			return std::chrono::duration<float>(m_EndTime - m_StartTime).count();
		}

		inline float EndSeconds()
		{
			m_Ended = true;
			m_EndTime = std::chrono::system_clock::now();
			return std::chrono::duration<float>(m_EndTime - m_StartTime).count();
		}

		inline bool IsEnded() const { return m_Ended; }

	private:
		TimePoint m_StartTime;
		TimePoint m_EndTime;
		
		bool m_Ended = false;
	};

	class Time
	{
	public:
		Time()
			: m_LastTime(std::chrono::system_clock::now())
		{}

		Time(std::chrono::time_point<std::chrono::system_clock> time)
			: m_LastTime(time) 
		{}

		inline DeltaTime UpdateDeltaTime() 
		{
			TimePoint time = std::chrono::system_clock::now();
			m_DeltaTime = std::chrono::duration<float>(time - m_LastTime).count();
			m_LastTime = time;
			return m_DeltaTime;
		}
		
		inline float GetLastUpdateTime() const { return m_DeltaTime.GetSeconds(); }

		inline void StartTimer() { m_Timer.Start(); }
		inline float EndTimerSeconds() { m_Timer.EndSeconds(); }
		inline float EndMilliTimerSeconds() { m_Timer.EndMilliSeconds(); }
		
	private:
		TimePoint m_LastTime;
		DeltaTime m_DeltaTime;
		Timer m_Timer;
	};

}
