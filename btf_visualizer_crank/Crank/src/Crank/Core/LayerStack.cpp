#include "crpch.h"
#include "LayerStack.h"

namespace Crank {

    LayerStack::LayerStack()
    {
    }

    LayerStack::~LayerStack()
    {
        for (Layer* l : m_Layers) {
            l->OnDetach();
            delete l;
        }
    }

    void LayerStack::PushLayer(Layer* layer)
    {
        m_Layers.emplace(m_Layers.begin() + m_LayerInsertIndex, layer);
        m_LayerInsertIndex++;
    }

    void LayerStack::PushOverlay(Layer* overlay)
    {
        m_Layers.emplace_back(overlay);
    }

    void LayerStack::PopLayer(Layer* layer)
    {
        auto findResult = std::find(m_Layers.begin(), m_Layers.begin() + m_LayerInsertIndex, layer);
        if (findResult != m_Layers.end())
        {
            m_Layers.erase(findResult);
            layer->OnDetach();
            m_LayerInsertIndex--;
            delete layer;
        }
        else 
        {
            CR_CORE_WARN("LayerStack: Failed to pop layer ", layer->GetName(), ", layer not found");
        }
    }

    void LayerStack::PopOverlay(Layer* overlay)
    {
        auto findResult = std::find(m_Layers.begin() + m_LayerInsertIndex, m_Layers.end(), overlay);
        if (findResult != m_Layers.end())
        {
            m_Layers.erase(findResult);
            overlay->OnDetach();
            delete overlay;
        }
        else
        {
            CR_CORE_WARN("LayerStack: Failed to pop overlay ", overlay->GetName(), ", overlay not found");
        }
    }
}