#pragma once

#ifdef CR_PLATFORM_WINDOWS

extern Crank::Application* Crank::CreateApplication();

int main(int argc, char** argv)
{
	Crank::Log::Init();

	auto app = Crank::CreateApplication();
	app->Run();
	delete app;
}

#endif
