#pragma once

#include "Crank/Core/Core.h"
#include "Crank/Events/Event.h"
#include "Crank/Core/Time.h"

namespace Crank {

	class Layer
	{
	public:
		Layer(const std::string& name = "Default Layer");
		virtual ~Layer() {}

		virtual void OnUpdate(DeltaTime ts) {}
		virtual void OnEvent(Event& e) {}

		virtual void OnAttach() {}
		virtual void OnDetach() {}

		virtual void OnImGuiRender() {}

		inline const std::string& GetName() { return m_Name; }
		inline void SetActive(bool active) { m_Active = active; }

	protected:
		std::string m_Name;
		bool m_Active;
	};
}
