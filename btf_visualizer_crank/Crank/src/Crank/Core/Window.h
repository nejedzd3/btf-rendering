#pragma once

#include "crpch.h"

#include "Crank/Core/Core.h"
#include "Crank/Events/Event.h"
#include "Crank/Events/ApplicationEvents.h"

namespace Crank
{


	class Window
	{
	public:
		struct Specification
		{
			unsigned int	Width = 1280;
			unsigned int	Height = 720;
			const char*		Title = "Default Window";
			unsigned int	MSAASamples = 0; // off by default
			bool			UseVSync = false;

			Specification() {}

			Specification(unsigned int width, 
							 unsigned int height,
							 const char* title,
							 unsigned int msaaSamples,
							 bool useVSync			
				)
				: Width(width), Height(height), Title(title), 
				  MSAASamples(msaaSamples), UseVSync(useVSync)
			{}
		};
	public:
		virtual ~Window() {}
		
		virtual void OnUpdate() = 0;
		virtual bool OnResize(WindowResizedEvent& e) = 0;

		virtual unsigned int GetWidth() const = 0;
		virtual unsigned int GetHeight() const = 0;
		virtual float GetAspectRatio() const = 0;
		virtual unsigned int GetMSAASamples() const = 0;

		virtual void SetEventCallback(const std::function<void(Event&)>& callback) = 0;
		virtual void SetVSync(bool enabled) = 0;
		virtual bool IsVSync() const = 0;
		virtual bool IsMSAA() const = 0;

		virtual void* GetNativeWindow() = 0;

		static Window* Create(const Specification& properties);
	};
}

