#pragma once

#ifdef CR_PLATFORM_WINDOWS
	#define CR_WINDOWS_BUILD
#else
	#error Crank only supports Windows
#endif

#ifdef CR_DEBUG
	#define CR_ASSERT(x, ...) { if(!(x)) { CR_ERROR("Assertion Failed: {0}", __VA_ARGS__); __debugbreak(); } }
	#define CR_CORE_ASSERT(x, ...) { if(!(x)) { CR_CORE_ERROR("Assertion Failed: {0}", __VA_ARGS__); __debugbreak(); } }
#else
	#define CR_ASSERT(x, ...)
	#define CR_CORE_ASSERT(x, ...)
#endif

#define BIT(x) 1 << x

#define CR_BIND_EVENT_FUNCTION(x) std::bind(&x, this, std::placeholders::_1)

namespace Crank
{

	template<typename T>
	using Ref = std::shared_ptr<T>;

	template<typename T, typename ... Args>
	constexpr Ref<T> CreateRef(Args&& ... args)
	{
		return std::make_shared<T>(std::forward<Args>(args)...);
	}
}
