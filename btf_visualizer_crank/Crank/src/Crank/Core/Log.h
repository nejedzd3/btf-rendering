#pragma once

#include "Crank/Core/Core.h"

// Ignores all warnings from inside external headers
#pragma warning(push, 0)
#include <spdlog/spdlog.h>
#include <spdlog/fmt/ostr.h>
#pragma warning(pop)

namespace Crank
{
	class Log
	{
	public:
		Log();
		~Log();
		static void Init();

		inline static Ref<spdlog::logger>& GetCoreLogger() { return s_CoreLogger; }
		inline static Ref<spdlog::logger>& GetClientLogger() { return s_ClientLogger; }
	private:
		static Ref<spdlog::logger> s_ClientLogger;
		static Ref<spdlog::logger> s_CoreLogger;
	};
}

#ifndef CR_DIST
	// Core log macros
	#define CR_CORE_ERROR(...)   ::Crank::Log::GetCoreLogger()->error(__VA_ARGS__)
	#define CR_CORE_WARN(...)    ::Crank::Log::GetCoreLogger()->warn(__VA_ARGS__)
	#define CR_CORE_INFO(...)    ::Crank::Log::GetCoreLogger()->info(__VA_ARGS__)
	#define CR_CORE_TRACE(...)   ::Crank::Log::GetCoreLogger()->trace(__VA_ARGS__)
	#define CR_CORE_FATAL(...)   ::Crank::Log::GetCoreLogger()->fatal(__VA_ARGS__)

	// Client log macros
	#define CR_ERROR(...)        ::Crank::Log::GetClientLogger()->error(__VA_ARGS__)
	#define CR_WARN(...)         ::Crank::Log::GetClientLogger()->warn(__VA_ARGS__)
	#define CR_INFO(...)         ::Crank::Log::GetClientLogger()->info(__VA_ARGS__)
	#define CR_TRACE(...)        ::Crank::Log::GetClientLogger()->trace(__VA_ARGS__)
	#define CR_FATAL(...)        ::Crank::Log::GetClientLogger()->fatal(__VA_ARGS__)
#endif


