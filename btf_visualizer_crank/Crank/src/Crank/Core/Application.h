#pragma once

#include "Crank/Core/Core.h"

#include "Crank/Events/ApplicationEvents.h"
#include "Crank/Events/KeyboardEvents.h"
#include "Crank/Core/LayerStack.h"
#include "Crank/Core/Time.h"
#include "Crank/Core/Window.h"
#include "Crank/Renderer/Renderer.h"
#include "Platform/OpenGL/OpenGLShader.h"
#include "Platform/OpenGL/OpenGLBuffers.h"
#include "Platform/OpenGL/OpenGLVertexArray.h"

#include "Crank/ImGui/ImGuiLayer.h"

namespace Crank
{
	class Application
	{
	public:
		Application();
		virtual ~Application();

		void Run();
		void Close();

		void OnEvent(Event& e);

		void PushLayer(Layer* layer);
		void PushOverlay(Layer* overlay);

		float GetFps() const;
		float GetFrameTime() const;

		static inline Application& Get() { return *s_Instance; }
		inline Window& GetWindow() { return *m_Window; }

	protected:
		virtual bool OnWindowClosed(WindowClosedEvent& e);
		virtual bool OnKeyPressed(KeyPressedEvent& e);
		virtual bool OnResize(WindowResizedEvent& e);

	private:
		std::unique_ptr<Window> m_Window;
		ImGuiLayer* m_ImGuiLayer;
		LayerStack m_LayerStack;

		Time m_Time;

		bool m_Running = true;
	
	private:
		static Application* s_Instance;
	};

	// To be defined in CLIENT
	Application* CreateApplication();
}

