#include "crpch.h"
#include "Application.h"
#include "Crank/Input/Input.h"


namespace Crank
{
	Application* Application::s_Instance = nullptr;

	Application::Application()
	{
		CR_CORE_ASSERT(!s_Instance, "Application already exists!");
		s_Instance = this;

		m_Window = std::unique_ptr<Window>(Window::Create({1280, 720, "Crank Sandbox", 4, false}));
		m_Window->SetEventCallback(CR_BIND_EVENT_FUNCTION(Application::OnEvent));

		Renderer::Init();

		m_ImGuiLayer = new ImGuiLayer();
		PushOverlay(m_ImGuiLayer);
	}

	Application::~Application()
	{}

	void Application::Run()
	{
		while (m_Running)
		{
			DeltaTime dt = m_Time.UpdateDeltaTime();
			for (Layer* layer : m_LayerStack) 
			{
				layer->OnUpdate(dt);
			}

			m_ImGuiLayer->Begin();
			for (Layer* layer : m_LayerStack)
				layer->OnImGuiRender();
			m_ImGuiLayer->End();

			m_Window->OnUpdate();
		}
	}

	void Application::Close()
	{
		m_Running = false;
	}

	void Application::OnEvent(Event& e)
	{
		EventDispatcher dispatcher(e);
		dispatcher.Dispatch<WindowClosedEvent>(CR_BIND_EVENT_FUNCTION(Application::OnWindowClosed));
		dispatcher.Dispatch<KeyPressedEvent>(CR_BIND_EVENT_FUNCTION(Application::OnKeyPressed));
		dispatcher.Dispatch<WindowResizedEvent>(CR_BIND_EVENT_FUNCTION(Application::OnResize));

		for (auto it = m_LayerStack.end(); it != m_LayerStack.begin();)
		{
			(*--it)->OnEvent(e);
			if (e.IsHandled())
				break;
		}

	}
	void Application::PushLayer(Layer* layer)
	{
		if (!layer) {
			CR_CORE_ERROR("Application::PushLayer failed to push layer {0}", layer->GetName());
			return;
		}
		m_LayerStack.PushLayer(layer);
		layer->OnAttach();
	}

	void Application::PushOverlay(Layer* overlay)
	{
		if (!overlay) {
			CR_CORE_ERROR("Application::PushLayer failed to push overlay {0}", overlay->GetName());
			return;
		}
		m_LayerStack.PushOverlay(overlay);
		overlay->OnAttach();
	}

	float Application::GetFps() const
	{
		return 1.0f / m_Time.GetLastUpdateTime();
	}

	float Application::GetFrameTime() const
	{
		return m_Time.GetLastUpdateTime();
	}

	bool Application::OnWindowClosed(WindowClosedEvent& e)
	{
		m_Running = false;
		return true;
	}

	bool Application::OnKeyPressed(KeyPressedEvent& e) 
	{
		return false;
	}
	bool Application::OnResize(WindowResizedEvent& e)
	{
		return m_Window->OnResize(e);
	}
}
