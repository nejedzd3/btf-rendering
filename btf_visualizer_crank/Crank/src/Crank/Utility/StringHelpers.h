#pragma once
#include "crpch.h"

namespace Crank
{
	namespace Utility
	{
		static bool StartsWith(const std::string& str, const std::string& pattern)
		{
			if (str.length() < pattern.length())
				return false;

			return str.substr(0, pattern.length()) == pattern;
		}

		static std::string GetNthToken(const std::string& str, size_t n, const char delimiter = ' ')
		{
			if (str.length() < n)
				return "";

			std::string result;
			std::stringstream ss(str);

			for (size_t i = 0; i < n; ++i)
			{
				if (!ss.good())
					return "";

				std::getline(ss, result, delimiter);
			}

			return result;
		}
	}


}
