#pragma once
#include "crpch.h"
#include <glm/glm.hpp>

namespace Crank
{
	namespace Utility
	{
		static glm::vec3 CartesianToSphereCoords(const glm::vec3& cartCoords)
		{
			float r = glm::sqrt(cartCoords.x * cartCoords.x + cartCoords.y * cartCoords.y + cartCoords.z * cartCoords.z);
			float elevation = glm::acos(cartCoords.z / r);
			float azimuth = elevation == 0.0f ? 0.0f : 
				glm::pi<float>() + glm::sign(cartCoords.y) * glm::acos(cartCoords.x / glm::sqrt(cartCoords.x * cartCoords.x + cartCoords.y * cartCoords.y));
			return { r, azimuth, elevation };
		}
	}

}
