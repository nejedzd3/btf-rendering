#include "crpch.h"
#include "ImGuiRenderer.h"
#include <imgui.h>

namespace Crank
{
	void ImGuiRenderer::Submit(const std::string& text)
	{
		ImGui::Text(text.c_str());
	}

	void ImGuiRenderer::Submit(int* intPtr, const ImGuiDataDetail& det)
	{
		ImGui::DragInt(det.Name.c_str(), intPtr, det.Increment, (int)det.Min, (int)det.Max);
	}

	void ImGuiRenderer::Submit(float* floatPtr, const ImGuiDataDetail& det)
	{
		ImGui::DragFloat(det.Name.c_str(), floatPtr, det.Increment, det.Min, det.Max);
	}

	void ImGuiRenderer::Submit(bool* boolPtr, const ImGuiDataDetail& det)
	{
		ImGui::Checkbox(det.Name.c_str(), boolPtr);
	}

	void ImGuiRenderer::Submit(const glm::uvec2& vec, const ImGuiDataDetail& det)
	{
		ImGui::DragScalarN(det.Name.c_str(), ImGuiDataType_U32, (void*)&(vec[0]), 2, det.Increment, (const void*)&det.Min, (const void*)&det.Max);
	}

	void ImGuiRenderer::Submit(const glm::uvec3& vec, const ImGuiDataDetail& det)
	{
		ImGui::DragScalarN(det.Name.c_str(), ImGuiDataType_U32, (void*)&(vec[0]), 3, det.Increment, (const void*)&det.Min, (const void*)&det.Max);
	}

	void ImGuiRenderer::Submit(const glm::vec2& vec, const ImGuiDataDetail& det)
	{
		ImGui::DragFloat2(det.Name.c_str(), (float*)&(vec[0]), det.Increment, det.Min, det.Max);
	}

	void ImGuiRenderer::Submit(const glm::vec3& vec, const ImGuiDataDetail& det)
	{
		if (det.Color)
			ImGui::ColorEdit3(det.Name.c_str(), (float*)&vec[0]);
		else
			ImGui::DragFloat3(det.Name.c_str(), (float*)&vec[0], det.Increment, det.Min, det.Max);
	}

	void ImGuiRenderer::Submit(const glm::vec4& vec, const ImGuiDataDetail& det)
	{
		ImGui::DragFloat4(det.Name.c_str(), (float*)&vec[0], det.Increment, det.Min, det.Max);
	}

	void ImGuiRenderer::Submit(const glm::mat2& mat, const ImGuiDataDetail& det)
	{
		ImGui::Text(det.Name.c_str());
		ImGui::DragFloat2("", (float*)&(mat[0]), det.Increment, det.Min, det.Max);
		ImGui::DragFloat2("", (float*)&(mat[2]), det.Increment, det.Min, det.Max);
	}

	void ImGuiRenderer::Submit(const glm::mat3& mat, const ImGuiDataDetail& det)
	{
		ImGui::Text(det.Name.c_str());
		ImGui::DragFloat3("", (float*)&mat[0], det.Increment, det.Min, det.Max);
		ImGui::DragFloat3("", (float*)&mat[3], det.Increment, det.Min, det.Max);
		ImGui::DragFloat3("", (float*)&mat[6], det.Increment, det.Min, det.Max);
	}

	void ImGuiRenderer::Submit(const glm::mat4& mat, const ImGuiDataDetail& det)
	{
		ImGui::Text(det.Name.c_str());
		ImGui::DragFloat4("", (float*)&mat[0], det.Increment, det.Min, det.Max);
		ImGui::DragFloat4("", (float*)&mat[4], det.Increment, det.Min, det.Max);
		ImGui::DragFloat4("", (float*)&mat[8], det.Increment, det.Min, det.Max);
		ImGui::DragFloat4("", (float*)&mat[12], det.Increment, det.Min, det.Max);
	}

	void ImGuiRenderer::Separator()
	{
		ImGui::Separator();
	}

	void ImGuiRenderer::SameLine()
	{
		ImGui::SameLine();
	}

	bool ImGuiRenderer::Button(const std::string& name)
	{
		return ImGui::Button(name.c_str());
	}

	void ImGuiRenderer::SetFontAsDefault(const std::string& path, float size_px)
	{
		ImGuiIO& io = ImGui::GetIO();
		io.Fonts->AddFontFromFileTTF(path.c_str(), size_px);

		io.FontDefault = io.Fonts->Fonts.back();
	}
}