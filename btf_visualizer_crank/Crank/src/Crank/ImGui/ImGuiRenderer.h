#pragma once
#include <glm/glm.hpp>

namespace Crank
{
	struct ImGuiDataDetail
	{
		std::string Name;
		float Min, Max;
		float Increment;
		bool Color;

		ImGuiDataDetail() : Name(""), Color(false), Min(-10.0f), Max(10.0f), Increment(1.0f) {}

		ImGuiDataDetail(const std::string& name) : Name(name), Color(false), Min(-10.0f), Max(10.0f), Increment(1.0f) {}
		
		ImGuiDataDetail(bool color, float min, float max, float increment, const std::string& name = "")
			: Name(name), Color(color), Min(min), Max(max), Increment(increment) {}
		
		ImGuiDataDetail(const ImGuiDataDetail& rhs)
		{
			Name = rhs.Name; Min = rhs.Min; Max = rhs.Max; Increment = rhs.Increment; Color = rhs.Color;
		}
	};

	class ImGuiRenderer
	{
	public:
		static void Submit(const std::string& text);
		static void Submit(int* intPtr, const ImGuiDataDetail& det);
		static void Submit(float* floatPtr, const ImGuiDataDetail& det);
		static void Submit(bool* boolPtr, const ImGuiDataDetail& det);
		static void Submit(const glm::uvec2& vec, const ImGuiDataDetail& det);
		static void Submit(const glm::uvec3& vec, const ImGuiDataDetail& det);
		static void Submit(const glm::vec2& vec, const ImGuiDataDetail& det);
		static void Submit(const glm::vec3& vec, const ImGuiDataDetail& det);
		static void Submit(const glm::vec4& vec, const ImGuiDataDetail& det);
		static void Submit(const glm::mat2& mat, const ImGuiDataDetail& det);
		static void Submit(const glm::mat3& mat, const ImGuiDataDetail& det);
		static void Submit(const glm::mat4& mat, const ImGuiDataDetail& det);

		static void Separator();
		static void SameLine();
		static bool Button(const std::string& name);

		static void SetFontAsDefault(const std::string& path, float size_px);
	};
}

