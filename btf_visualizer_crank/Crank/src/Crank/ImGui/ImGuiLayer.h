#pragma once

#include "Crank/Core/Layer.h"
#include "imgui.h"

#include "Crank/Events/ApplicationEvents.h"
#include "Crank/Events/KeyboardEvents.h"
#include "Crank/Events/MouseEvents.h"

namespace Crank
{
	class ImGuiLayer : public Layer 
	{
	public:
		ImGuiLayer();
		~ImGuiLayer();

		virtual void OnUpdate(DeltaTime ts) override;

		virtual void OnAttach() override;
		virtual void OnDetach() override;

		virtual void OnImGuiRender() override;

		void Begin();
		void End();

	private:
		float m_Time = 0.0f;

	};


}
