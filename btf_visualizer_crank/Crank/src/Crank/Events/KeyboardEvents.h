#pragma once

#include "crpch.h"
#include "Event.h"
#include "Crank/Input/KeyCodes.h"

namespace Crank
{

	class KeyPressedEvent : public Event
	{
	public:
		KeyPressedEvent(KeyCode keyCode)
			: m_KeyCode(keyCode) {}


		inline KeyCode GetKeyCode() const { return m_KeyCode; }

		EVENT_CLASS_TYPE(KeyPress)
		EVENT_CLASS_CATEGORY(EventCategory::Keyboard | EventCategory::UserInput)

		std::string ToString() const override 
		{
			std::stringstream ss;
			ss << "KeyPressedEvent: Pressed Key's code - " << m_KeyCode;
			return ss.str();
		}

	private:
		KeyCode m_KeyCode;
	};

	class KeyReleasedEvent : public Event
	{
	public:
		KeyReleasedEvent(KeyCode keyCode)
			: m_KeyCode(keyCode) {}

		inline KeyCode GetKeyCode() const	{ return m_KeyCode; }
		
		EVENT_CLASS_TYPE(KeyRelease)
		EVENT_CLASS_CATEGORY(EventCategory::Keyboard | EventCategory::UserInput)

		std::string ToString() const override 
		{
			std::stringstream ss;
			ss << "KeyReleasedEvent: Released Key's code - " << m_KeyCode;
			return ss.str();
		}

	private:
		KeyCode m_KeyCode;
	};

	class KeyTypedEvent : public Event 
	{
	public:
		KeyTypedEvent(KeyCode keyCode)
			: m_KeyCode(keyCode) {}

		inline KeyCode GetKeyCode() const { return m_KeyCode; }

		EVENT_CLASS_TYPE(KeyTyped)
		EVENT_CLASS_CATEGORY(EventCategory::Keyboard | EventCategory::UserInput)

		std::string ToString() const override
		{
			std::stringstream ss;
			ss << "KeyTypedEvent: Typed Key's code - " << m_KeyCode;
			return ss.str();
		}
	private:
		KeyCode m_KeyCode;
	};
}