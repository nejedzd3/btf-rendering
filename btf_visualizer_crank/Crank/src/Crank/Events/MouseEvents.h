#pragma once

#include "crpch.h"
#include "Event.h"
#include "Crank/Input/MouseCodes.h"

namespace Crank
{
	class MouseMovedEvent : public Event
	{
	public:
		MouseMovedEvent(float x, float y)	
			: m_X(x), m_Y(y) {}

		inline float GetX() const { return m_X; }
		inline float GetY() const { return m_Y; }

		EVENT_CLASS_TYPE(MouseMove)
		EVENT_CLASS_CATEGORY(EventCategory::Mouse | EventCategory::UserInput)

		std::string	ToString() const override 
		{ 
			std::stringstream ss;
			ss << "MouseMovedEvent: Moved to (" << m_X << ", " << m_Y << ")";
			return ss.str();
		}
	private:
		float m_X, m_Y;
	};

	class MouseClickedEvent : public Event
	{
	public:
		MouseClickedEvent(MouseCode button) 
			: m_Button(button) {}

		inline MouseCode GetButton() const { return m_Button; }

		EVENT_CLASS_TYPE(MouseClick)
		EVENT_CLASS_CATEGORY(EventCategory::Mouse | EventCategory::UserInput | EventCategory::MouseButton)

		std::string	ToString() const override 
		{
			std::stringstream ss;
			ss << "MouseClickedEvent: Clicked button - " << m_Button;
			return ss.str();
		}
	private:
		MouseCode m_Button;
	};

	class MouseReleasedEvent : public Event
	{
	public:
		MouseReleasedEvent(MouseCode button)
			: m_Button(button) {}

		inline MouseCode GetButton() const { return m_Button; }

		EVENT_CLASS_TYPE(MouseRelease)
		EVENT_CLASS_CATEGORY(EventCategory::Mouse | EventCategory::UserInput | EventCategory::MouseButton)

		std::string	ToString() const override 
		{
			std::stringstream ss;
			ss << "MouseReleasedEvent: Released button - " << m_Button;
			return ss.str();
		}

	private:
		MouseCode m_Button;
	};

	class MouseScrolledEvent : public Event 
	{
	public:
		MouseScrolledEvent(float xOffset, float yOffset) 
			: m_XOffset(xOffset), m_YOffset(yOffset) {}

		inline float GetXOffset() const { return m_XOffset; }
		inline float GetYOffset() const { return m_YOffset; }

		EVENT_CLASS_TYPE(MouseScroll)
		EVENT_CLASS_CATEGORY(EventCategory::Mouse | EventCategory::UserInput)

		std::string	ToString() const override { 
			std::stringstream ss;
			ss << "MouseScrolledEvent: Mouse scroll xOffset = " << m_XOffset << ", yOffset = " << m_YOffset;
			return ss.str();
		}

	private:
		float m_XOffset, m_YOffset;
	};
}
