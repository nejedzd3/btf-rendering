#pragma once

#include "Crank/Core/Core.h"

namespace Crank
{
	
	enum class EventType
	{
		None = 0,
		KeyPress, KeyRelease, KeyTyped,
		MouseMove, MouseClick, MouseRelease, MouseScroll,
		WindowResize, WindowClose, WindowMoved, WindowFocus, WindowLostFocus,
		AppTick, AppUpdate, AppRender
	};

	enum EventCategory
	{
		None			= 0,
		Mouse			= BIT(0),
		MouseButton		= BIT(1),
		Keyboard		= BIT(2),
		App				= BIT(3),
		UserInput		= BIT(4)
	};

#define EVENT_CLASS_TYPE(type) static EventType GetStaticType() { return EventType::##type; }\
								virtual EventType GetEventType() const override { return GetStaticType(); }\
								virtual const char* GetName() const override { return #type; }

#define EVENT_CLASS_CATEGORY(category) virtual int GetEventCategoryFlags() const override { return category; }



	class Event
	{
		friend class EventDispatcher;
	public:
		virtual const char* GetName() const = 0;
		virtual EventType	GetEventType() const = 0;
		virtual int			GetEventCategoryFlags() const = 0;

		virtual std::string	ToString() const { return GetName(); }

		inline bool IsInCategory(EventCategory category)
		{
			return GetEventCategoryFlags() & category;
		}

		inline bool IsHandled()
		{
			return m_Handled;
		}

	protected:
		bool m_Handled = false;
	};

	class EventDispatcher 
	{
		template<typename T>
		using EventFunction = std::function<bool(T&)>;
	public:
		EventDispatcher(Event& event) 
			: m_Event(event) {}

		template<typename T>
		bool Dispatch(EventFunction<T> func) 
		{
			if (m_Event.GetEventType() == T::GetStaticType())
			{
				m_Event.m_Handled = func(*(T*) &m_Event);
				return true;
			}
			return false;
		}

	private:
		Event& m_Event;
	};

	inline std::ostream& operator<<(std::ostream& os, const Event& e) 
	{
		return os << e.ToString();
	}
}
