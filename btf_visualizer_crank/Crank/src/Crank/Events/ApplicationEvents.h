#pragma once

#include "crpch.h"
#include "Event.h"

namespace Crank {

	class WindowResizedEvent : public Event 
	{
	public:
		WindowResizedEvent(unsigned int newWidth, unsigned int newHeight)
			: m_NewHeight(newHeight), m_NewWidth(newWidth) {}

		inline unsigned int GetNewWidth() const  { return m_NewWidth; }
		inline unsigned int GetNewHeight() const { return m_NewHeight; }

		EVENT_CLASS_TYPE(WindowResize)
		EVENT_CLASS_CATEGORY(EventCategory::App)
		
		std::string ToString() const override 
		{
			std::stringstream ss;
			ss << "WindowResizedEvent: New resolution - " << m_NewWidth << "x" << m_NewHeight;
			return ss.str();
		}
	private:
		unsigned int m_NewHeight, m_NewWidth;
	};

	class WindowClosedEvent : public Event
	{
	public:
		WindowClosedEvent() {}

		EVENT_CLASS_TYPE(WindowClose)
		EVENT_CLASS_CATEGORY(EventCategory::App)
	};

	class AppTickEvent : public Event 
	{
	public:
		AppTickEvent() {}

		EVENT_CLASS_TYPE(AppTick)
		EVENT_CLASS_CATEGORY(EventCategory::App)
	};

	class AppUpdateEvent : public Event
	{
	public:
		AppUpdateEvent() {}

		EVENT_CLASS_TYPE(AppUpdate)
		EVENT_CLASS_CATEGORY(EventCategory::App)
	};

	class AppRenderEvent : public Event
	{
	public:
		AppRenderEvent() {}

		EVENT_CLASS_TYPE(AppRender)
		EVENT_CLASS_CATEGORY(EventCategory::App)
	};
}