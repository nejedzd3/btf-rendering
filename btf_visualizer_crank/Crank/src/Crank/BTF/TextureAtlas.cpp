#include "crpch.h"
#include "TextureAtlas.h"

namespace Crank
{
	TextureAtlas::TextureAtlas(const std::string& path, const std::string& separateMetadataPath)
	{
		std::string metadataPath;
		if (!separateMetadataPath.empty())
			metadataPath = separateMetadataPath;
		else
		{
			std::string suffix = "";
			size_t suffixEnd = 0;

			for (size_t i = path.length() - 1; i >= 0; --i)
			{
				if (path[i] == '.')
				{
					suffixEnd = i;
					break;
				}
				suffix += path[i];
			}

			if (suffixEnd == 0 || suffix.empty())
			{
				CR_CORE_ERROR("TextureAtlas::TextureAtlas: Failed to parse metadata path.");
				return;
			}
			metadataPath = path.substr(0, suffixEnd) + ".metadata";
		}

		if (!LoadMetadata(metadataPath))
		{
			CR_CORE_ERROR("TextureAtlas::TextureAtlas: Failed to load metadata.");
			return;
		}

		m_AtlasTexture = Texture2D::Create(path, false);
	}
	bool TextureAtlas::LoadMetadata(const std::string& path)
	{
		std::ifstream ifs(path);

		ifs >> m_Spec.TextureCount.x >> m_Spec.TextureCount.y;
		ifs >> m_Spec.SingleTextureResolution.x >> m_Spec.SingleTextureResolution.y;
		ifs >> m_Spec.FirstSphericalCoordinate.x >> m_Spec.FirstSphericalCoordinate.y;
		ifs >> m_Spec.IsUTIA;

		if (ifs.fail())
		{
			m_Spec = Specification();
			return false;
		}

		return true;
	}
}
