#pragma once
#include "Crank/Renderer/Texture.h"

namespace Crank
{
	class TextureAtlas
	{
	public:
		struct Specification
		{
			glm::uvec2 TextureCount;
			glm::uvec2 SingleTextureResolution;
			glm::vec2  FirstSphericalCoordinate;
			bool	   IsUTIA;

			Specification()
				: TextureCount({0, 0}), SingleTextureResolution({0, 0}), FirstSphericalCoordinate({0.0f, 0.0f}), IsUTIA(false)
			{}

			Specification(const glm::uvec2& textureCount, const glm::uvec2 singleTextureResolution, const glm::vec2& firstSphericalCoordinate, bool isUTIA)
				: TextureCount(textureCount), SingleTextureResolution(singleTextureResolution), 
				  FirstSphericalCoordinate(firstSphericalCoordinate), IsUTIA(isUTIA)
			{}
		};

	public:
		TextureAtlas(const std::string& path, const std::string& separateMetadataPath = "");
		~TextureAtlas() {}

		Ref<Texture2D> GetAtlasTexture() { return m_AtlasTexture; }

		const Specification& GetSpec() const { return m_Spec; }
		const Specification& GetSpec() { return m_Spec; }

	private:
		Ref<Texture2D> m_AtlasTexture = nullptr;
		Specification m_Spec;

	private:
		bool LoadMetadata(const std::string& path);
	};
}

