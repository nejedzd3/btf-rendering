#pragma once

#include <iostream>
#include <memory>
#include <algorithm>
#include <functional>
#include <typeinfo>
#include <variant>
#include <chrono>

#include <vector>
#include <unordered_map>
#include <set>

#include <assert.h>
#include <stdio.h>
#include <cstring>

#include <string>
#include <sstream>
#include <fstream>


#include "Crank/Core/Log.h"
#include "Crank/Core/Core.h"
#include "Crank/Utility/StringHelpers.h"


#ifdef CR_PLATFORM_WINDOWS
	#include <Windows.h>
#endif
