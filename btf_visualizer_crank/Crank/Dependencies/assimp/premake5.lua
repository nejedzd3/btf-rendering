externalproject "assimp"
	location "./code"
	kind "StaticLib"
	language "C++"
	cppdialect "C++17"
	staticruntime "On"

	defines
	{
		"ASSIMP_BUILD_NO_EXPORT",
		"ASSIMP_BUILD_NO_M3D_IMPORTER"
	}