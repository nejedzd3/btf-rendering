# BTF Rendering

This is a repository for bachelor thesis "Real time rendering 3D grafiky za použití pokročilých texturálních modelů". In this repository are all files and programs used for the practical part of this thesis. That includes:


* Link to gitlab repository for Blender plugin DBTF Creator
* Raw BTF materials
* Python script with functions to synthezise texture atlases for BTFs
* Fork of game engine Crank which was partly modified to render BTFs
* Application BTF Visualizer which is used to render BTFs
