from array import array
import cv2 as cv
import numpy as np
from pathlib import Path


def spherical_to_cartesian(
    spherical_coords: np.ndarray,
    radius=1.0,
    center: np.ndarray = np.array([0.0, 0.0, 0.0]),
):
    cartesian_coords = np.array(
        [
            np.sin(spherical_coords[0]) * np.cos(spherical_coords[1]),
            np.sin(spherical_coords[1]),
            np.cos(spherical_coords[0]) * np.cos(spherical_coords[1]),
        ]
    )
    if center.shape != cartesian_coords.shape:
        raise Exception("Center shape not valid. Use 3 element NDArray.")
    return radius * cartesian_coords + center


def cartesian_to_spherical(cartesian_coords: np.ndarray):
    r = np.sqrt(np.sum(cartesian_coords**2))

    azimuth = np.sign(cartesian_coords[1]) * np.arccos(
        cartesian_coords[0]
        / np.sqrt(cartesian_coords[0] ** 2 + cartesian_coords[1] ** 2)
    )

    elevation = np.arccos(cartesian_coords[2] / r)

    return np.array([azimuth, elevation, r])


def normalize(x: np.ndarray):
    return x / np.linalg.norm(x)


def parse_btf_texture_name(name: str):
    split_name_wo_suffix = name.split("-")

    # 'Cx.x-x.x', kde x <=> cislce
    if len(split_name_wo_suffix) == 2:
        azimuth = float(split_name_wo_suffix[0][1:])
        elevation = float(split_name_wo_suffix[1])
        if 0.0 < azimuth < 2 * np.pi and 0.0 < elevation < np.pi / 2:
            return (None, None, azimuth, elevation)

    # 'Lx.x-x.x_Cx.x-x.x', kde x <=> cislce
    elif len(split_name_wo_suffix) == 3:
        thr_pc_split = name.split("_")
        if len(thr_pc_split) != 2:
            return None

        light_info_split = thr_pc_split[0][1:].split("-")
        camera_info_split = thr_pc_split[1][1:].split("-")
        if len(light_info_split) != 2 or len(camera_info_split) != 2:
            return None

        light_azimuth = float(light_info_split[0])
        light_elevation = float(light_info_split[1])

        camera_azimuth = float(camera_info_split[0])
        camera_elevation = float(camera_info_split[1])

        if (
            0.0 < light_azimuth < 2 * np.pi
            and 0.0 < camera_azimuth < 2 * np.pi
            and 0.0 < light_elevation < np.pi / 2
            and 0.0 < camera_elevation < np.pi / 2
        ):
            return (light_azimuth, light_elevation, camera_azimuth, camera_elevation)

    return None


def create_btf_texture_atlas(loaded_textures, output_folder: Path, output_name: str):

    loaded_textures.sort(key=lambda x: x[0])
    texture_res = (loaded_textures[0][2].shape[0], loaded_textures[0][2].shape[1])

    texture_matrix = []
    last_azi = loaded_textures[0][0]
    buff_array = []

    for tup in loaded_textures:
        # azimuth of previous image != azimuth of current image
        if last_azi != tup[0]:
            buff_array.sort(key=lambda y: y[0], reverse=True)  # sort by elevation
            texture_matrix.append([x[1] for x in buff_array])
            buff_array = []

        buff_array.append((tup[1], tup[2]))  # (elevation, image_data)

        last_azi = tup[0]

    # insert last row
    buff_array.sort(key=lambda y: y[0], reverse=True)  # sort by elevation
    texture_matrix.append([x[1] for x in buff_array])

    atlas_res = (
        len(texture_matrix) * texture_res[0],
        len(texture_matrix[0]) * texture_res[1],
    )  # (X, Y)
    atlas_texture = np.zeros((atlas_res[1], atlas_res[0], 3), dtype=np.uint8)

    x_offset = 0
    y_offset = 0
    for im_arr in texture_matrix:
        for image in im_arr:
            atlas_texture[
                0
                + y_offset * texture_res[1] : image.shape[1]
                + y_offset * texture_res[1],
                0
                + x_offset * texture_res[0] : image.shape[0]
                + x_offset * texture_res[0],
            ] = image
            y_offset += 1

        y_offset = 0
        x_offset += 1

    cv.imwrite(str(output_folder / f"{output_name}.png"), atlas_texture)

    metadata_file = output_folder / f"{output_name}.metadata"

    with open(str(metadata_file.resolve()), "w") as f:
        # x y       - num of texture in atlas
        # x y       - resolution of single texture
        # azi elev  - starting azimuth and elevation
        # utia      - is utia btf (1) or not (0)
        f.write(
            f"{atlas_res[0] // texture_res[0]} {atlas_res[1] // texture_res[1]}\n{texture_res[0]} {texture_res[1]}\n{loaded_textures[0][0]} {loaded_textures[0][1]}\n0"
        )


def create_utia_btf_texture_atlas(
    loaded_textures, output_folder: Path, output_name: str
):
    loaded_textures.sort(key=lambda x: x[0])
    loaded_textures.sort(key=lambda x: x[1])

    texture_res = (loaded_textures[0][2].shape[0], loaded_textures[0][2].shape[1])

    texture_matrix = np.empty((9, 9), dtype=list)
    i, j = 0, 8
    for tup in loaded_textures:
        texture_matrix[i, j] = tup[2]
        i += 1
        if i >= 9:
            j -= 1
            i = 0

    atlas_res = (
        len(texture_matrix) * texture_res[0],
        len(texture_matrix[0]) * texture_res[1],
    )  # (X, Y)
    atlas_texture = np.zeros((atlas_res[1], atlas_res[0], 3), dtype=np.uint8)

    x_offset = 0
    y_offset = 0
    for im_arr in texture_matrix:
        for image in im_arr:
            atlas_texture[
                0
                + y_offset * texture_res[1] : image.shape[1]
                + y_offset * texture_res[1],
                0
                + x_offset * texture_res[0] : image.shape[0]
                + x_offset * texture_res[0],
            ] = image
            y_offset += 1

        y_offset = 0
        x_offset += 1

    cv.imwrite(str(output_folder / f"{output_name}.png"), atlas_texture)

    metadata_file = output_folder / f"{output_name}.metadata"

    with open(str(metadata_file.resolve()), "w") as f:
        # x y       - num of texture in atlas
        # x y       - resolution of single texture
        # azi elev  - starting azimuth and elevation
        # utia      - is utia btf (1) or not (0)
        f.write(
            f"{atlas_res[0] // texture_res[0]} {atlas_res[1] // texture_res[1]}\n{texture_res[0]} {texture_res[1]}\n{loaded_textures[0][0]} {loaded_textures[0][1]}\n1"
        )


def create_testing_texture_atlas(
    output_folder: Path, dimensions, single_tex_res, starting_spherical_coords
):
    if not output_folder.exists():
        print(f'Error: Light folder "{output_folder.name}" does not exist.')
        return

    texture_matrix = []

    for x in range(0, dimensions[0]):
        row = []
        for y in range(0, dimensions[1]):
            img = np.ones((single_tex_res[0], single_tex_res[1], 3), np.uint8) * 255

            # Hardcoded to work on square images
            offset = (27, -40)
            text = f"({x}, {y})"
            text_center = (
                single_tex_res[0] // 2 - offset[0] * len(text),
                single_tex_res[1] // 2 - offset[1],
            )

            cv.putText(
                img,
                text,
                text_center,
                cv.FONT_HERSHEY_SIMPLEX,
                3,
                (0, 0, 0),
                3,
                cv.LINE_AA,
            )
            border_col = [150, 150, 150]
            img[:5, :] = border_col
            img[-5:, :] = border_col
            img[:, :5] = border_col
            img[:, -5:] = border_col
            row.append(img)
        row.reverse()
        texture_matrix.append(row)

    atlas_res = (
        len(texture_matrix) * single_tex_res[0],
        len(texture_matrix[0]) * single_tex_res[1],
    )  # (X, Y)
    atlas_texture = np.zeros((atlas_res[1], atlas_res[0], 3), dtype=np.uint8)

    x_offset = 0
    y_offset = 0
    for im_arr in texture_matrix:
        for image in im_arr:
            atlas_texture[
                0
                + y_offset * single_tex_res[1] : image.shape[1]
                + y_offset * single_tex_res[1],
                0
                + x_offset * single_tex_res[0] : image.shape[0]
                + x_offset * single_tex_res[0],
            ] = image
            y_offset += 1

        y_offset = 0
        x_offset += 1

    cv.imwrite(str(output_folder / "TestAtlas.png"), atlas_texture)

    metadata_file = output_folder / "TestAtlas.metadata"

    with open(str(metadata_file.resolve()), "w") as f:
        # x y       - num of texture in atlas
        # x y       - resolution of single texture
        # azi elev  - starting azimuth and elevation
        # is_utia   - no
        f.write(
            f"{dimensions[0]} {dimensions[1]}\n{single_tex_res[0]} {single_tex_res[1]}\n{starting_spherical_coords[0]} {starting_spherical_coords[1]}\n0"
        )


def process_raw_btf_texture_set(folder: Path):
    if not folder.exists() or not folder.is_dir():
        print(f'Error: Wrong Path - "{folder}" does not exist or is not a directory')
        return None
    output_folder = folder.with_name(f"{folder.name}_atlas")
    output_folder.mkdir(exist_ok=True)
    print(f'Processing "{folder.name}"...')

    # Assuming folder layout "texture_name/light/view.png"
    for light_folder in folder.iterdir():
        loaded_textures = []
        for path in light_folder.iterdir():
            if path.is_dir():
                print(
                    f'Error: Light folder "{light_folder.name}" contains directories.'
                )
                return

            image_data = cv.imread(str(path.resolve()))
            parsed_name_data = parse_btf_texture_name(path.stem)
            if parsed_name_data is None:
                print(
                    f'Error: Light folder "{folder.name}" contains unparsable texture name.'
                )
                return

            azi = parsed_name_data[2]
            elev = parsed_name_data[3]

            loaded_textures.append((azi, elev, image_data))

        create_btf_texture_atlas(loaded_textures, output_folder, light_folder.name)

    print(f"Finished. Output saved to {output_folder}")


def process_utia_btf_texture_set(folder: Path):
    if not folder.exists() or not folder.is_dir():
        print(f'Error: Wrong Path - "{folder}" does not exist or is not a directory')
        return None
    output_folder = folder.with_name(f"{folder.name}_atlas")
    output_folder.mkdir(exist_ok=True)
    print(f'Processing "{folder.name}"...')

    # Assuming folder layout "texture_name/view/view_light.png"
    light_files = []
    for view_folder in folder.iterdir():
        for light_png in view_folder.iterdir():
            light_files.append(light_png)
            break

    loaded_textures = []
    for path in light_files:
        image_data = cv.imread(str(path.resolve()))

        file_name = path.stem
        parts = file_name.split("_")

        l_azi = np.radians(int(parts[1][2:]))
        l_elev = np.radians(int(parts[0][2:]))

        azi = np.radians(int(parts[3][2:]))
        elev = np.radians(int(parts[2][2:]))

        loaded_textures.append((azi, elev, image_data))

    create_utia_btf_texture_atlas(
        loaded_textures, output_folder, f"L{l_azi:.3f}-{l_elev:.3f}"
    )

    print(f"Finished. Output saved to {output_folder}")


def create_utia_testing_atlas(output_folder: Path):
    if not output_folder.exists() or not output_folder.is_dir():
        print(
            f'Error: Wrong Path - "{output_folder}" does not exist or is not a directory'
        )
        return None
    atlas_dim = (9, 9)
    single_tex_res = (512, 512)
    first_sph_coord = (0.0, 0.0)

    texture_matrix = []
    offset = [1, 6, 12, 18, 20, 24]
    elev_idx = 0
    elev_remaining = offset[elev_idx]

    for x in range(0, atlas_dim[0]):
        row = []
        for y in range(0, atlas_dim[1]):
            img = np.ones((single_tex_res[0], single_tex_res[1], 3), np.uint8) * 255

            if elev_remaining < 1:
                elev_idx += 1
                elev_remaining = offset[elev_idx]

            elev = elev_idx * 15
            azi = (offset[elev_idx] - elev_remaining) * (360 // offset[elev_idx])

            # Hardcoded to work on square images
            text_offset = (26, -40)
            text = f"({azi}, {elev})"
            text_center = (
                single_tex_res[0] // 2 - text_offset[0] * len(text),
                single_tex_res[1] // 2 - text_offset[1],
            )

            cv.putText(
                img,
                text,
                text_center,
                cv.FONT_HERSHEY_SIMPLEX,
                3,
                (0, 0, 0),
                3,
                cv.LINE_AA,
            )
            border_col = [150, 150, 150]
            img[:5, :] = border_col
            img[-5:, :] = border_col
            img[:, :5] = border_col
            img[:, -5:] = border_col
            row.append(img)
            elev_remaining -= 1

        texture_matrix.insert(0, row)

    atlas_res = (
        len(texture_matrix) * single_tex_res[0],
        len(texture_matrix[0]) * single_tex_res[1],
    )  # (X, Y)
    atlas_texture = np.zeros((atlas_res[1], atlas_res[0], 3), dtype=np.uint8)

    x_offset = 0
    y_offset = 0
    for im_arr in texture_matrix:
        for image in im_arr:
            atlas_texture[
                0
                + x_offset * single_tex_res[0] : image.shape[0]
                + x_offset * single_tex_res[0],
                0
                + y_offset * single_tex_res[1] : image.shape[1]
                + y_offset * single_tex_res[1],
            ] = image
            y_offset += 1

        y_offset = 0
        x_offset += 1

    cv.imwrite(str(output_folder / f"utia_test_atlas.png"), atlas_texture)

    metadata_file = output_folder / f"utia_test_atlas.metadata"

    with open(str(metadata_file.resolve()), "w") as f:
        # x y       - num of texture in atlas
        # x y       - resolution of single texture
        # azi elev  - starting azimuth and elevation
        # utia      - is utia btf (1) or not (0)
        f.write(
            f"{atlas_res[0] // single_tex_res[0]} {atlas_res[1] // single_tex_res[1]}\n{single_tex_res[0]} {single_tex_res[1]}\n{first_sph_coord[0]} {first_sph_coord[1]}\n1"
        )
